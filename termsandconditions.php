<?php 

session_start();
ob_start();
include "scripts/config.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/external-pages.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "topNav.php"; ?><!-- faq -->
    <div id="faq" class="faq_page">
        <div class="container">
            <!-- header -->
            <h3 class="section_header">
                <hr class="left visible-desktop">
                <span>Terms and Conditions</span>
                <hr class="right visible-desktop">
            </h3>

            <!-- list -->
            <div class="row">
                <div class="span12">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            Introduction
                        </div>
                        <div class="answer">
                            These terms of use govern your use of our website; by using our website, you agree to these terms of use in full. If you disagree with these terms of use or any part of these terms of use, you must not use our website. <br> <br>

							You must be at least 16 years of age to use our website and have some affiliation to the [University of Maryland] (i.e. student, faculty, staff, administration, or alumni). By using our website and by agreeing to these terms of use, you warrant and represent that you are at least 16 years of age and have some affiliation to the [University of Maryland]. <br> <br>
	
							Like most interactive web sites Procity's website uses cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. By using our website and agreeing to these terms of use, you consent to our use of cookies.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">2</div>
                        <div class="question">
                            Privacy Statement
                        </div>
                        <div class="answer">
                            We are committed to protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible service to our customers. Congress has created specific offences for unauthorised actions against computer systems and data. We will investigate any such actions with a view to prosecuting and/or taking civil proceedings to recover damages against those responsible
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">3</div>
                        <div class="question">
                            Licence to use website
                        </div>
                        <div class="answer">
							Unless otherwise stated, we or our licensors own the intellectual property rights in the website and material on the website. Subject to the licence below, all these intellectual property rights are reserved. <br><br> 

							You may view, download for caching purposes only, and print pages from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms of use. <br> <br> 

							You must not:<br> <br> 

							(a)	republish material from this website (including republication on another website);<br>

							(b)	sell, rent or sub-license material from the website;<br>

							(c)	show any material from the website in public without written consent;<br> 

							(d)	reproduce, duplicate, copy or otherwise exploit material on our website for a commercial purpose;<br> 

							(e)	edit or otherwise modify any material on the website.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">4</div>
                        <div class="question">
                            Acceptable use
                        </div>
                        <div class="answer">
                            You must not use our website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity. <br> <br>

							You must not use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software. <br> <br>

							You must not conduct any systematic or automated data collection activities (including, without limitation, scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">5</div>
                        <div class="question">
                            Restricted access
                        </div>
                        <div class="answer">
                            Access to certain areas of our website is restricted. We reserve the right to restrict access to other areas of our website, or indeed our whole website, at our discretion. <br> <br>

							A claimer of an item of our website must follow through with all item requests 1 item at a time. The claimer will be restricted from claiming more than 1 item within the website in order to ensure the efficiency of the website and encourage timely exchanges. <br><br>

							A donator of an item is limited to 5 active donations at any given time. The donator will be restricted from donating any more than 5 donations, in order to ensure the efficiency of the website.<br><br>

							You must not use any other person's user ID and password to access our website [, unless you have that person's documented permission to do so].<br><br>

							We may disable your user ID and password at any time in our sole discretion with or without notice or explanation.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">6</div>
                        <div class="question">
                            User content
                        </div>
                        <div class="answer">
                            In these terms of use, "your content" means material (including, without limitation, text, images and audio-visual materials) that you submit to our website, for whatever purpose. <br> <br>

							You grant to us a worldwide, irrevocable, non-exclusive, royalty-free licence to use, reproduce, adapt, publish, translate and distribute your content in any existing or future media. You also grant to us the right to sub-license these rights and the right to bring an action for infringement of these rights. <br> <br>

							You warrant and represent that your content will comply with these terms of use. <br> <br>

							Your content must not be illegal or unlawful, must not infringe any third party's legal rights and must not be capable of giving rise to legal action whether against you or us or a third party (in each case under any applicable law).  <br> <br>

							You must not submit any content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint. <br> <br>

							We reserve the right to edit or remove any material submitted to our website, or stored on our servers, or hosted or published upon our website.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">7</div>
                        <div class="question">
                            Procity Specific Information
                        </div>
                        <div class="answer">
                            All Procity users are encouraged to only meet in preferred locations, as listed in the users Profile page. In the event that a preferred location is located within a Residence Hall, Procity users are encouraged to exchange items on the same floor of and near the University of Maryland 24-Hour Service Desk. <br> <br>

							Procity is not liable for any item damage, usage, exposure, functionality, life, performance, or appearance of any items posted to www.myprocity.com and exchanged betweem Procity users. 
							Procity will only encourage members to take precaution when claiming items and to be honest and accurate when donating items. <br> <br>

							All items must be packaged inside a box, bin, or bag when donating to our Donation Pick-Up Service, unless the item is a furniture item, or exceeds the capacity of a box, bin, or bag. In the event that the Procity user does not abide by this rule, Procity acquires the right to not accept the donation, nor give the predicted ProPoint value for the item. <br> <br>

							After a Unique Donation has been evaluated by Procity, the donator of the Unique Donation will be notified of the ProPoint value via the user's email address wherein s/he can cancel the donation if desired. <br> <br>

							ProPoints are calculated and determined uniquely for each item using an algorithm branded by Procity. The details of the ProPoint system and algorithm are as follows: Item's average value (this is determined by locating the item on eBay's Buy Now feature and averaging the first 5 results for the item in its used condition state and excluding outliers) + Item's average value x 60% (Like new/New condition), or x 40% (Used/Signs of wear and tear condition), or x 20% (Useable/Shows major signs of use condition). 
							The Donation Pick-Up days, as outlined on the home page of the website, are always optional for the Procity donator. Only through this process will the donator be able to receive ProPoints for an unclaimed item. In order for the item to be eligible for this service, the item must have been donated to the Procity network more than 24 hours prior to the Donation Pick-Up time, and could not have been claimed by another Procity user. <br> <br>

							Any donator to Procity's network is eligible to donate an item to Procity's partnered charity on the select Donation Pick-Up days to receive ProPoint value from a member of the Procity organization. <br> <br>

							During Donation Pick-Up days, a member of the Procity organization will be evaluating the current condition of the items in order to process the correct ProPoint value. ProPoint value for items present on Donation Pick-Up days will be processed immediately on sight of the donation. The ultimate condition of the item is evaluated and determined by the Procity staff member. The ultimate decision to donate the item is controlled by the Procity user.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">8</div>
                        <div class="question">
                            Links from this website 
                        </div>
                        <div class="answer">
                            We do not monitor or review the content of other party’s websites which are linked to from this website. Opinions expressed or material appearing on such websites are not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site & to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">9</div>
                        <div class="question">
                            Limited warranties
                        </div>
                        <div class="answer">
                            We do not warrant the completeness or accuracy of the information published on this website; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date. <br> <br>

							To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to this website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill).
	
                        </div>
                    </div>
					                    <div class="faq">
                        <div class="number">10</div>
                        <div class="question">
                            Limitations and exclusions of liability
                        </div>
                        <div class="answer">
                           Nothing in these terms of use will: (a) limit or exclude our or your liability for death or personal injury resulting from negligence; (b) limit or exclude our or your liability for fraud or fraudulent misrepresentation; (c) limit any of our or your liabilities in any way that is not permitted under applicable law; or (d) exclude any of our or your liabilities that may not be excluded under applicable law.  <br> <br>

							The limitations and exclusions of liability set out in this Section and elsewhere in these terms of use: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under these terms of use or in relation to the subject matter of these terms of use, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty. <br> <br>

							To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature. <br> <br>

							We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control. <br> <br>

							We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings and virtual currencies, business, contracts, commercial opportunities or goodwill. <br> <br>

							We will not be liable to you in respect of any loss or corruption of any data, database or software. <br> <br>

							We will not be liable to you in respect of any special, indirect or consequential loss or damage to your items upon exchange.

                        </div>
                    </div>

					 <div class="faq">
                        <div class="number">11</div>
                        <div class="question">
                            Indemnity
                        </div>
                        <div class="answer">
                            You hereby indemnify us and undertake to keep us indemnified against any losses, damages, costs, liabilities and expenses (including, without limitation, legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute on the advice of our legal advisers) incurred or suffered by us arising out of any breach by you of any provision of these terms of use, or arising out of any claim that you have breached any provision of these terms of use.
                        </div>
                    </div>

					<div class="faq">
                        <div class="number">12</div>
                        <div class="question">
                            Breaches of these terms of use
                        </div>
                        <div class="answer">
							Without prejudice to our other rights under these terms of use, if you breach these terms of use in any way, we may take such action as we deem appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.
                        </div>
                    </div>
					<div class="faq">
                        <div class="number">13</div>
                        <div class="question">
                            Variation
                        </div>
                        <div class="answer">
							We may revise these terms of use from time to time. Revised terms of use will apply to the use of our website from the date of publication of the revised terms of use on our website.
                        </div>
                    </div>
					<div class="faq">
                        <div class="number">14</div>
                        <div class="question">
                            Assignment
                        </div>
                        <div class="answer">
							We may transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms of use without notifying you or obtaining your consent. 
                        </div>
                    </div>
					<div class="faq">
                        <div class="number">15</div>
                        <div class="question">
                            Severability
                        </div>
                        <div class="answer">
							If a provision of these terms of use is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect. If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. 
                        </div>
                    </div>
					<div class="faq">
                        <div class="number">16</div>
                        <div class="question">
                            Exclusion of third party rights
                        </div>
                        <div class="answer">
							These terms of use are for the benefit of you and us, and are not intended to benefit any third party or be enforceable by any third party. The exercise of our and your rights in relation to these terms of use is not subject to the consent of any third party. 
                        </div>
                    </div>

					<div class="faq">
                        <div class="number">17</div>
                        <div class="question">
                            Entire agreement
                        </div>
                        <div class="answer">
							Subject to the first paragraph of Section [8], these terms of use constitute the entire agreement between you and us in relation to your use of our website and supersede all previous agreements in respect of your use of our website.
                        </div>
                    </div>

					<div class="faq">
                        <div class="number">18</div>
                        <div class="question">
                            Law and jurisdiction
                        </div>
                        <div class="answer">
							These terms of use will be governed by and construed in accordance with English  law, and any disputes relating to these terms of use will be subject to the [non-]exclusive  jurisdiction of the courts of England and Wales.
                        </div>
                    </div>

					<div class="faq">
                        <div class="number">19</div>
                        <div class="question">
                            Credit
                        </div>
                        <div class="answer">
							This document was made in part using a SEQ Legal template.
                        </div>
                    </div>


                </div>
				<!--<script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 

					
				</script> 
				
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> -->
				
            </div>
        </div>
    </div>

   

    <!-- starts footer -->
    <?php include "footer.php";?>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
</body>
</html>
