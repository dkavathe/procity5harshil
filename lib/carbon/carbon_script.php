<?php

/*
 * This is a script that wraps the Carbon datediff library.
 * 
 * Carbon is class-based and it uses namespaces, which means
 * it can't be directly accessed from a script (because the
 * use keyword is not valid in PHP scripts, only in PHP
 * classes).
 * 
 * This class is a wrapper for Carbon, in that it is allowed
 * to use the "use" keyword, and does so to load the library.
 * It contains the one function we use Carbon for (though
 * it can certainly be extended to use more functions), which
 * is the fuzzy_date_diff function.
 * 
 * Fuzzy date diff is what you see on Facebook, Twitter, and
 * such that looks like "two weeks ago" or "in one second"
 * as oppposed to some long timestamp like 2000-12-13 13:20:19.
 * The function takes in a start and end timestamp, calculates
 * the difference between them, and returns the result
 * as a human-readable diff time format.
 */

require_once('src/Carbon/Carbon.php');
use Carbon\Carbon;

class CarbonScript {
    function fuzzy_date_diff($start, $end,$mode) {
        $start = new DateTime($start);
        $end = new DateTime($end);

        $diff = $start->diff($end); 

        $days = $diff->format('%d'); 
        $hours = $diff->format('%h'); 
        $mins = $diff->format('%i'); 
        $secs = $diff->format('%s');

        return Carbon::now()->addDays($days)->addHours($hours)->addMinutes($mins)->addSeconds($secs)->diffForHumans($mode);
    }
}