<?php
/**
 * Created by PhpStorm.
 * User: Micheal
 * Date: 2/7/14
 * Time: 2:52 AM
 */
session_start();

class PaypalTest
{

    public $api_user = "testapiuser_api1.apiuser.net";
    public $api_pass = "1391907867";
    public $api_sig = "AFcWxV21C7fd0v3bYYYRCpSSRl31A8UDnd7UYJHPV0Xixwlr29tAslrH"; //"A-ATjwILvTD6k.gYXQxjhoN6FIHhAAYRQZpxWiRawZ2KrGquUUAOMo3o";
    public $app_id = "APP-80W284485P519543T";
    public $apiUrl = 'https://svcs.sandbox.paypal.com/AdaptivePayments/';
    public $paypalUrl = "https://www.paypal.com/webscr?cmd=_ap-payment&paykey=";
    public $headers;

    private $orgName;
    private $orgInfo;
    private $donationAmount;
    private $percentageAmount;
    private $paykey;




    //get orderInfo
    public function getOrderInfo()
    {
        $this->orgName = $_POST['organization'];
        //connect to mysql
        include 'config.php';

        //get org paypal info
        $sql = "SELECT name, email, paypal FROM orginfo WHERE id ='$this->orgName'";
        $result = mysql_query($sql);

        $this->orgInfo = mysql_fetch_assoc($result);
        $this->donationAmount = $_POST['amount'];
        $_SESSION['points'] = $_POST['points'];
        $this->percentageAmount = (5 / 100) * $this->donationAmount;

       $this->splitPay();
    }

    public function __construct(){
        $this->headers = array(
            "X-PAYPAL-SECURITY-USERID: ".$this->api_user,
            "X-PAYPAL-SECURITY-PASSWORD: ".$this->api_pass,
            "X-PAYPAL-SECURITY-SIGNATURE: ".$this->api_sig,
            "X-PAYPAL-DEVICE-IPADDRESS: 76.125.238.169",
            "X-PAYPAL-REQUEST-DATA-FORMAT: JSON",
            "X-PAYPAL-RESPONSE-DATA-FORMAT: JSON",
            "X-PAYPAL-APPLICATION-ID: ".$this->app_id,
            ""
        );
    }

    public function getPaymentOptions($paykey){

    }
    public function setPaymentOptions(){

    }
    public function _paypalSend($data,$call){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl.$call);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        $response = curl_exec($ch);
        json_decode($response, TRUE);
        return $response;

    }
    public function splitPay(){

        // create the pay request
        $createPacket = array(
            "actionType" =>"PAY",
            "currencyCode" => "USD",
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount"=> $this->donationAmount, //organizations Take
                        "email"=> $this->orgInfo['paypal']
                    ),
                    array(
                        "amount"=> $this->percentageAmount, //Procity's Take
                        "email"=> "msquared86@gmail.com"
                    ),
                ),
            ),
            "returnUrl" => "https://www.myprocity.com/profile.php",
            "cancelUrl" => "https://www.myprocity.com/buypro.php",
            "requestEnvelope" => array(
                "errorLanguage" => "en_US",
                "detailLevel" => "ReturnAll",
            ),
        );

        $response = $this->_paypalSend($createPacket,"Pay");

        //var_dump(json_decode($response));
        $responseArray = json_decode($response, TRUE);

        if($responseArray['responseEnvelope']['ack'])
        {
            $this->paykey = $responseArray['payKey'];
            header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='.$this->paykey);
            //return array('username')
        }
        else{
            //something on failure.
        }


    }
}