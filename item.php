<?php 
	session_start();
	include "scripts/config.php"; 
	
	date_default_timezone_set('America/New_York');
	
	if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['id'])) {
	
		$id=mysql_real_escape_string($_GET['id']);
		$error1 = '';
		$error2='';
		$error3='';

		if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error1')){

			$error1 = 'You can only claim 1 item at a time, please check your profile page';

		}

		else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error2')){

			$error2 = 'You cant claim your own item!';

		}

		else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error3')){

			$error3 = 'Not enough ProPoints';

		}

		$query = mysql_query("select * from donate_item where id='$id'") or die(mysql_error());
		$fetch_donate=mysql_fetch_array($query);

		if($fetch_donate <= 0) {
		
			echo "Invalid request logging";
			$ipaddress = "Donate: ".$_SERVER['REMOTE_ADDR']."\r\n";
			$file = 'admin/maliciouslogger.txt';
			$fp = fopen($file, 'a');
			fwrite($fp, $ipaddress);
			fclose($fp);
			exit(0);
					
		}

		$item_id=$fetch_donate['item_id'];
		$donated_by=$fetch_donate['donated_by'];
		$donate_type=$fetch_donate['donate_type'];


		$item_name=$fetch_donate['item_name'];
	

		$sql_user=mysql_query("select * from userinfo where Id='$donated_by'") or die(mysql_error());
		$fetch_user=mysql_fetch_array($sql_user);

		$ebase=$fetch_donate['ebase'];

		$condition=$fetch_donate['condition'];
		$sql_cond=mysql_query("select * from conditions where id ='$condition'");
		$fetch_cond=mysql_fetch_array($sql_cond);

		$condition_detail=$fetch_cond['condition'];

		$location=$fetch_user['Location'];
		$sql_loc = mysql_query("select * from location where id='$location'") or die(mysql_error());
		$fetch_location=mysql_fetch_array($sql_loc);


	} else {
	
		echo "Invalid request logging";
		$ipaddress = "Item: ".$_SERVER['REMOTE_ADDR']."\r\n";
		$file = 'admin/maliciouslogger.txt';
		$fp = fopen($file, 'a');
		fwrite($fp, $ipaddress);
		fclose($fp);
		exit(0);
	
	}
		
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/mootools/1.2.1/mootools-yui-compressed.js"></script>
    <script src=
    "js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script> <script src="js/theme.js" type="text/javascript"></script>
</head>
<body>

<!-- begins navbar -->
<?php include"topNav.php"; ?>
<!-- ends navbar -->

<div id="blog_wrapper">
<div style="color:#F00;"><?php if($error1!='') {?> <script>alert("You can only claim 1 item at a time, please check your profile page");</script><?php } if($error2!='')  {?> <script>alert("You cant claim your own item!");</script><?php  } if($error3!=''){?><script>alert("Not enough ProPoints");</script><?php }
		   ?></div>
<div class="container">
<section id="" style="">
</section>
<div class="row">

<div class="span8">
<h1><?php echo $item_name;?></h1>

<div class="post">
<div class="row">
<div class="span3"><a> <img class="main_pic" alt="main pic" src="drive/donations/<?php echo $fetch_donate['image'];?>" style="width:300px; height:300px;" /></a></div>
<div class="span4 info">

<p><?php echo $condition_detail;?></p>
<p><?php echo $fetch_donate['item_description'];?></p>

<div class="post_info">
<p class="author"><?php echo $fetch_user['Username'];?></p>
<p class="date muted">Donated <?php 

	$expires = strtotime($fetch_donate['donated_time']);
	$start = date("Y-m-d H:i:s", time());
	$end = date("Y-m-d H:i:s", $expires);

	require_once('lib/carbon/carbon_script.php');
	$script = new CarbonScript();
	$fuzzy_date = $script->fuzzy_date_diff($start, $end,0);
	echo $fuzzy_date;
	
?></p>
<p><?php 
	$sql_loc=mysql_query("select * from location where id='$location'") or die(mysql_error());
			$fetch_loc=mysql_fetch_array($sql_loc);	
			$mylocation=$fetch_loc['location'];
echo $mylocation;?></p>
<script>function fbs_click() {u=location.href;t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;}</script><style> html .fb_share_link { padding:2px 0 0 20px; height:16px; background:url(http://static.ak.facebook.com/images/share/facebook_share_icon.gif?6:26981) no-repeat top left; }</style><a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>" onclick="return fbs_click()" target="_blank" class="fb_share_link">Share on Facebook</a>
<br>
<a href="https://twitter.com/share" class="twitter-share-button" data-text="claim my item!!" data-via="procityTN" data-hashtags="procity">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>
</div>
</div>

<input type="submit" onClick="processclaim(<?php echo $fetch_donate['id'];?>)" class="btn" value="claim - <?php echo $ebase;?> PP"></div>

</div>
<div class="span3 sidebar offset1">
</div>
</div>
</div>
</div>
<section id="" style="margin-top:-81px; margin-left:140px;">

</section>

<?php include "footer.php";?>

<script>

	function processclaim(id) {
	
		$.ajax({
			type: "POST",
			url:  "scripts/claim.php", 
			data: {id: id},
			success: function(data) {
				
				if(data.status == 'same') {
					window.location = "item.php?id="+id+"&msg=error2";
				} else if(data.status == 'already') {
					window.location = "item.php?id="+id+"&msg=error1";
				} else if(data.status == 'points') {
					window.location = "item.php?id="+id+"&msg=error3";
				} else if(data.status == 'success') {
					window.location = "profile.php?show=donated";
				}

			}

		});
		
		//window.location = "scripts/claim.php?id="+id;
		
	}
	
</script>


</body>
</html>