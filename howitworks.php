<?php 

session_start();
ob_start();
include "scripts/config.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/external-pages.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "topNav.php"; ?><!-- faq -->
    <div id="faq" class="faq_page">
        <div class="container">
            <!-- header -->
            <h2 class="section_header">
                <hr class="left visible-desktop">
                <span>How it works</span>
                <hr class="right visible-desktop">
            </h2>

			<center>
				<iframe width="560" height="315" src="//www.youtube.com/embed/rshVhUpv6nE" frameborder="0" allowfullscreen></iframe>
			</center>
			
			<center>
				<iframe width="560" height="315" src="//www.youtube.com/embed/CyTCv_cxPzc" frameborder="0" allowfullscreen></iframe>
			</center>
			
			<center>
				<iframe width="560" height="315" src="//www.youtube.com/embed/V5GGodn9oNw" frameborder="0" allowfullscreen></iframe>
			</center>
	
            <!-- list
            <div class="row">
                <div class="span12">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How does Procity work?
                        </div>
                        <div class="answer">
                            Procity mission is to reward those who do good in society with ProPoints! <br> <br>
							Either donate an item or volunteer with an organization, to earn ProPoints. Use those points
							for items,coupons,gift codes and more rewards!
<!--Step 1: You have an item that you no longer use (i.e. clothing, textbooks, furniture). <br> <br>
Step 2: Donate the item using the form above and watch it appear in The City. <br> <br>
Step 3: Once your item has been claimed, meet up with the claimer on campus and you will receive ProPoints (valued at 60% higher than monetary worth). <br> <br>
Step 4: Use those ProPoints to get items that you do want! <br> <br>
Step 5: If your item has not been claimed, you can get immediate ProPoints by donating at our on-campus pickup locations every other Friday.

                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">2</div>
                        <div class="question">
						What are ProPoints exactly?
                        </div>
                        <div class="answer">
						ProPoints act as your reward for doing good to the community. Use them for items and rewards such has coupons,and e-gift cards!
						<!--ProPoints are valued at up to 60% more than monetary value. If you donate a laptop in great condition worth $300, you will receive 480 ProPoints (PP). At minimum, ProPoints are valued at 20% higher than monetary value even for poorly conditioned items (useable). What does this mean? Procity is always giving more value for your donated items. You can use your ProPoints at any time to claim other items in The City. Please see our <a href="termsandconditions.php"> Terms and Conditions </a> for ProPoint specifics.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">3</div>
                        <div class="question">
                            What are the benefits of Procity? 
                        </div>
                        <div class="answer">
						<!--With Procity you will be able to... Get what you want, Donate what you can, Live clutter free, Help those in need, Meet new people, Benefit from it all! <br> <br>
						Procity has no bidding <br> 
						Procity has no shipping <br> 
						Procity is FREE to use <br> 
						Procity is campus exclusive <br>
						Procity directly supports charity <br> 
						Procity gives high value with ProPoints (60% higher than monetary worth)
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">4</div>
                        <div class="question">
                            What is a unique donation?  
                        </div>
                        <div class="answer">
                            Users may submit a unique donation at any time. 
							Use this option if you feel that the brand of your item is worth more than the generic value given by Procity, or if there is no category that matches your item. 
							This type of donation may take up to 48 hours to appear in The City. 
							If you are not happy with the ProPoint value never be afraid to cancel your donation.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">5</div>
                        <div class="question">
                            Which items can be donated to Procity? 
                        </div>
                        <div class="answer">
                            You can donate any item! If you're item does not have an appropriate category, feel free to select Unique Donation on the Donate page.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">6</div>
                        <div class="question">
                            What if my item never gets claimed? 
                        </div>
                        <div class="answer">
                            If your item is not wanted by another procitizen 
							(AND is an <a href="http://www.pickupplease.org/acceptable-donations"> acceptable donation item</a>), 
							you always have the option to further donate it to our on-campus, 
							charity pick-up & STILL RECEIVE PROPOINTS! 
							Also, if you want your ProPoints even faster and there is a 
							donation pick up date approaching, 
							stop by and give your item to charity--Instant ProPoints!
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">7</div>
                        <div class="question">
                            How many items can I claim at once? 
                        </div>
                        <div class="answer">
                            Even though you may have enough ProPoints to claim multiple items, 
							our system only allows you to claim one item at a time. 
							Why is this? We feel that Procity is a much more efficient system when donators can give their item and receive their ProPoints in a timely fashion. 
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">8</div>
                        <div class="question">
                            How many items can I donate for ProPoints? 
                        </div>
                        <div class="answer">
                            Feel free to donate up to ten items at a time! These are active donations, meaning that the items have not yet been claimed by another Procitizen.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">9</div>
                        <div class="question">
                            Do you have any claiming tips?
                        </div>
                        <div class="answer">
                            Procity encourages claimers to only use exchange items at UMD Preferred Locations. The Preferred Locations list can be found within a user's profile page, under "Change preferred location." <br><br>
Procity encourages all claimers to check and test electronics before walking away from the exchange. <br><br>
If you have a change in heart with your claimed item prior to the exchange, you may cancel the claim from your profile and be reimbursed with equivalent ProPoint value.

                        </div>
                    </div>
					<div class="faq">
                        <div class="number">10</div>
                        <div class="question">
                            Give me some donation tips!
                        </div>
                        <div class="answer">
                            Procity encourages donators to only exchange items at UMD Preferred Locations. The Preferred Locations list can be found within a user's profile page, under "Change preferred location." <br> <br>
Donators, be prepared to allow claimers to test your electronic items for functionality. <br> <br>
If you want your ProPoints faster, and there is a Vietnam Veterans of America donation pick up date approaching, stop by the Friday pick up location and give your item to charity--Instant ProPoints!


                        </div>
                    </div>
                </div>
				<!--<script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 

					

				</script> 
				
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> // 
				
            </div>-->
        </div>
    </div>

   

    <!-- starts footer -->
    <?php include "footer.php";?>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
	
</body>
</html>
