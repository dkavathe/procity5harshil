<?php     
//harshil is working on this
session_start();
ob_start();
include "scripts/config.php";

?>
<!DOCTYPE html>

<html>

	<head>

		<title>Procity - Rewarding Those Who Do-Good</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<!-- Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="img/icon.ico" rel="shortcut icon">
		<link href="css/theme.css" rel="stylesheet" type="text/css">
		<link href="css/validationEngine.jquery.css" rel="stylesheet" type=
		"text/css">
		<link href="css/template.css" rel="stylesheet" type="text/css">
		<link href="css/external-pages.css" rel="stylesheet" type="text/css">
		<link href=
		'http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic'
		rel='stylesheet' type='text/css'>
		<script>

			function isNumberKey(evt) {
			  
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;

			
			}
			
			function propointhandler() {
			
				var pts=document.getElementById('points').value;
				var val1 = parseFloat(pts, 10);
				var new1 =val1.toFixed(2);
				
				if(pts <=0 || pts > 1000)    {
				
					document.buypro.buy.disabled=true;
					return false;
					
				} 
				
				var cost = 0.15;
				var propts = pts * cost;
				var new1 = propts.toFixed(2);
				
				document.buypro.buy.disabled = false;
				document.getElementById('amount').value = new1;
				
			}
			
		</script>
		
		
		
	</head>

	<body>
		
		<!-- begins navbar -->
		<?php include"topNav.php"; ?><!-- starts pricing -->

		<div class="pricing_page" id="pricing">
			<div class="container">
				<!-- header -->

				<h2 class="section_header" style="font-size:30px;"><span style=
				"width:50%;">Give money to an organization!</span></h2>
				<p class="section_header" > * Earn ProPoints by giving money through PayPal to a service organization, they need it! At the same time earn ProPoints! Only 5% will go to Procity</p>
				<form action="scripts/paypal.php" enctype="multipart/form-data" id="buypro" method="post" name="buypro">
					<div class="row" style="margin-left:0px;">
						<div class="donationform">
							<table style=
							"margin: 20PX 0px -20px 40px; width:100%;">
								<tr>
									<td colspan="2">Give money to an service organization! (5% will go to Procity)</td>
								</tr>
								
								<tr>
								<td class="tbltext">Donate to an organization </td>
								<td><select required="" name="organization" id="organization">
								<option value="">Select service organization</option>
								<?php $sql_org=mysql_query("select * from orginfo WHERE status = 1 AND paypal != ''") or die(mysql_error());
									while($fetch_org=mysql_fetch_array($sql_org))
									{
								?>
								<option  value="<?php echo $fetch_org['id'];?>"><?php echo $fetch_org['name'];?></option>
								<?php }?>
								</select></td>
								</tr>


								<tr>
									<td class="tbltext">Earn these many ProPoints:</td>

									<td><input  required="" id=
									"points" name="points" onkeypress=
									"return isNumberKey(event)" onkeyup=
									"propointhandler();" type="text"></td>
								</tr>

								<tr>
									<td class="tbltext">Amount ($)</td>

									<td><input required="" id=
									"amount" name="amount" readonly="true" type=
									"text"></td>
								</tr>

								<tr>
									<td align="center" colspan="2"><input class=
									"btn" disabled="true" name="buy" onclick=
									"sendtopaypal()" type="submit" value=
									"Purchase via PayPal *"></td>
								</tr>
							</table>
						</div>
					</div>
				</form>
			</div>
		
		<!--<section id="" style="margin-top:18px; margin-left:358px;">
			
			<script type="text/javascript">
				
				google_ad_client = "ca-pub-5663904797620784"; 

				/* advert1 */ 

				google_ad_slot = "2888958757"; 
				google_ad_width = 728; 
				google_ad_height = 90; 

			</script> 
			
			<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script>
			
		</section>-->
		

		<script>

			function sendtopaypal() {
			
				var pts = getElementById("points").value;
				var amt = getElementById("amount").value;
				
				$.ajax({
				
					type: "POST",
					url:  "scripts/paypal.php", 
					data: {points: pts, amount: amt},
					dataType: "json"
				
				});
				
				window.location = "scripts/paypal.php";
					
			}

		</script>
		
		<script src="lib/jquery/jquery.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		
		


		<?php include "footer.php";?>
		
	</body>
	
</html>