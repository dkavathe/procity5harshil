<?php
	
	include "../scripts/config.php";
	
	$response = array("pic" => $tag, "success" => 0, "error" => 0);  
	
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['Username']) && isset($_POST['Id']) && isset($_POST['procitycode'])) {

		$pcode =  mysql_real_escape_string($_POST['procitycode']);
		
		if($pcode == "3109") {
		
			$username =  mysql_real_escape_string($_POST['Username']);
			
			$getusers = mysql_query("select * from userinfo where Username='$username'") or die(mysql_error());
			$fetch_maxdonate = mysql_fetch_array($getusers);	
			$user_id = $fetch_maxdonate['Id'];	
			
			$donate_id = mysql_real_escape_string($_POST['Id']);
			  
			$sql_maxclaim = mysql_query("select * from userinfo where Id='$user_id'");
			$fetch_maxclaim = mysql_fetch_array($sql_maxclaim);
			$numclaimed = $fetch_maxclaim['num_claimed'];
			
			if($numclaimed > 0) {
			
				$sql_comments = mysql_query("delete from comments where item_id='$donate_id'");
			
				$numclaimed = $numclaimed - 1;
				$update_claim = mysql_query("update userinfo set num_claimed='$numclaimed' where Id='$user_id'");
			
				$update_donate1 = mysql_query("update donate_item set claimed_time=0, claim_status='0', claimed_by='' where id='$donate_id' ") or die(mysql_error());
				
				$sql_user = mysql_query("select * from userinfo where Id='$user_id'") or die(mysql_error());
				$fetch_user = mysql_fetch_array($sql_user);
				$propts = $fetch_user['ProPoints'];
				$claim_email = $fetch_user['Email'];
				$claim_username = $fetch_user['Username'];
				 
				$sql_donateitem = mysql_query("select * from donate_item where id='$donate_id' ") or die(mysql_error());
				$fetch_donateitem = mysql_fetch_array($sql_donateitem);
				$item_id = $fetch_donateitem['item_id'];
				$donator_id = $fetch_donateitem['donated_by'];	
				
				$sql_donator = mysql_query("select * from userinfo where Id='$donator_id'") or die(mysql_error());
				$fetch_donator = mysql_fetch_array($sql_donator);
				$donate_email = $fetch_donator['Email'];
				$donate_username = $fetch_donator['Username'];
			 
				$ebase = $fetch_donateitem['ebase'];
			 
				$pts = $propts+$ebase;
			 
				$update_user = mysql_query("update userinfo set ProPoints='$pts' where Id='$user_id'") or die(mysql_error());
			
				$claim_msg = "Hello Procitizen $claim_username, \n\nYour Procity cancellation request has been processed. ProPoints have been added back to your Procity account. \n\nDouble check your profile to make sure that this ProPoint value is correct and matches the original value of the item. \n\nWe hope that you find an even better item in The City next time!\n\nRegards,\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
				$subject = "Processed claim cancellation";
				$headers = "From: procitystaff@gmail.com \r\n";
				$headers .= "Reply-To: procitystaff@gmail.com \r\n";
				$headers .= "Return-Path: procitystaff@gmail.com \r\n";
				$from = 'procitystaff@gmail.com';
				
				mail($claim_email,$subject,$claim_msg,$headers);	 
				
				$donate_msg = "Hello Procitizen $donate_username, \n\n Unfortunately, $claim_username has cancelled the claim of your item. There could have been a variety of reasons for this cancellation to occur. However, we have good news! \n\nYour item has already been automatically re-entered into The City for another Procitizen to claim. \n\nWant your ProPoints faster? If there is a Vietnam Veterans of America pick up date approaching, stop by the donation location and give your item to charity--Instant ProPoints! The donation pick up information can be found on our homepage.\n\nBest wishes,\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
				$subject2 = "Claim cancelled";
				$headers2 = "From: procitystaff@gmail.com \r\n";
				$headers2 .= "Reply-To: procitystaff@gmail.com\r\n";
				$headers2 .= "Return-Path: procitystaff@gmail.com\r\n";
				
				mail($donate_email,$subject2,$donate_msg,$headers2);	 
				
				$response["success"] = 1;
				echo json_encode($response);
			
			}
		
		}
			
	}
?>