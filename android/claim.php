<?php 
	
	include "../scripts/config.php";

	$response = array("pic" => $tag, "success" => 0, "error" => 0);  
	
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['Username']) && isset($_POST['Id']) && isset($_POST['procitycode'])) {

	
		$pcode = mysql_real_escape_string($_POST['procitycode']);
		
		if($pcode == "3109") {
		
			$username =  mysql_real_escape_string($_POST['Username']);
			
			$getusers = mysql_query("select * from userinfo where Username='$username'") or die(mysql_error());
			$fetch_maxdonate = mysql_fetch_array($getusers);	
			$user_id = $fetch_maxdonate['Id'];	
			
			$id=mysql_real_escape_string($_POST['Id']);
			$sql_donate=mysql_query("select * from donate_item where id='$id'") or die(mysql_error());
			$fetch_donate=mysql_fetch_array($sql_donate);
			
			$item_id=$fetch_donate['item_id'];
			$donated_by=$fetch_donate['donated_by'];
			$donate_type= $fetch_donate['donate_type'];
		
			if($donated_by == $user_id) {
	 
				$response["success"] = -1;
				echo json_encode($response);				
				return;
				
			} else {

		 
				$sql_user=mysql_query("select * from userinfo where Id='$user_id'") or die(mysql_error());
				$fetch_user=mysql_fetch_array($sql_user);
				$numclaimed = $fetch_user['num_claimed'];
				$claim_username = $fetch_user['Username'];
				
				$claim_loc = $fetch_user['Location'];
				$claimsql= mysql_query("select * from location where id='$claim_loc'");
				$fetch_claimloc=mysql_fetch_array($claimsql);
				$claimer_location = $fetch_claimloc['location'];
				
				$claim_email = $fetch_user['Email'];
				$maxclaimed = 1;
				
				if($numclaimed >= $maxclaimed) {
		 
					$response["success"] = -2;
					echo json_encode($response);				
					return;
					
				} else {
				

					 $propts = $fetch_user['ProPoints'];
					 $ebase = $fetch_donate['ebase'];				
	 
					 if($propts < $ebase) {
					 
						$response["success"] = -3;
						echo json_encode($response);				
						return;
						
					 } else {
					 
						$numclaimed = $numclaimed + 1;
						$time = date("Y-m-d H:i:s"); 
						$update_maxclaimed = "update userinfo set num_claimed='$numclaimed' where Id='$user_id'";
										mysql_query($update_maxclaimed) or die(mysql_error());
										
						$sql_donator = mysql_query("select * from userinfo where Id='$donated_by'") or die(mysql_error());
						$fetch_donator = mysql_fetch_array($sql_donator);
						$donator_email = $fetch_donator['Email'];
						$donate_username = $fetch_donator['Username'];
						 
						$donate_loc = $fetch_donator['Location'];
						$donatesql= mysql_query("select * from location where id='$donate_loc'");
						$fetch_donateloc=mysql_fetch_array($donatesql);
						$donate_location = $fetch_donateloc['location'];
				
						$pts = $propts - $ebase;
		
						$update_user = mysql_query("update userinfo set ProPoints='$pts' where Id='$user_id'") or die(mysql_error()); 
						$update_donateitem = mysql_query("update donate_item set claimed_time = NOW(), claim_status='1', claimed_by='$user_id' where id='$id'") or die(mysql_error());
						 
						$update = mysql_query("update history set claimed_by='$user_id' where donate_id='$id' ") or die(mysql_error());
						$update = mysql_query("update history set claim_status='1' where donate_id='$id' ") or die(mysql_error());

						$claim_msg = "Hey Procitizen $claim_username!\n\nCongratulations! You have claimed $donate_username ’s item!\n\nPlease contact their email $donator_email to set up a convenient time to meet up and receive your item. $donate_username has been notified as well and may even contact you first. \n\n*Once you have received your item, remember to confirm your transaction in your profile under 'Claimed Items.' This will activate your ability to claim other items and ensures proper ProPoints are given to the donator.\n\nWe hope you are happy with your exchange!\n\nRegards, \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
						$subject = "Claimed an item";
						$headers = "From: procitystaff@gmail.com \r\n";
						$headers .= "Reply-To: procitystaff@gmail.com\r\n";
						$headers .= "Return-Path: procitystaff@gmail.com\r\n";
						
						mail($claim_email,$subject,$claim_msg,$headers);	 
						
						$donate_msg = "Hey Procitizen $donate_username! \n\nCongratulations! $claim_username has claimed your item!\n\nPlease contact $claim_email to set up a convenient time to meet up and exchange your item. Only then will you be able to receive your ProPoints! $claim_username has been notified as well and may even contact you first.\n\n*Once you have given your item, remember to confirm your transaction in your profile under 'Donated Items' to ensure proper ProPoints are given to you.\n\nBest wishes, \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
						$subject2 = "Your item has been claimed";
						$headers2 = "From: procitystaff@gmail.com \r\n";
						$headers2 .= "Reply-To: procitystaff@gmail.com\r\n";
						$headers2 .= "Return-Path: procitystaff@gmail.com\r\n";
						
						mail($donator_email,$subject2,$donate_msg,$headers2);	 
						
						$response["success"] = 1;
						echo json_encode($response);				
						return;
					 
					 }
				 
				 }
			 
			 }
			
		}
		
	}
	
?>
