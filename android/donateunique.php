<?php 

include "../scripts/config.php";
include "../lib/resizeimage.php";

$response = array("pic" => $tag, "success" => 0, "error" => 0);  

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['category']) && isset($_POST['username']) && isset($_POST['item']) && isset($_POST['descp'])) {

	$pic = $_FILES['image']['name'];		
	
	$file_size = $_FILES['image']['size'];
		
	if ($file_size <= 3145728){ 
	
		$donate_username = mysql_real_escape_string($_POST['username']);
		$getusers = mysql_query("select * from userinfo where Username='$donate_username'") or die(mysql_error());
		$fetch_maxdonate = mysql_fetch_array($getusers);
		$maxdonate = $fetch_maxdonate['num_donated'];
		$donator_email = $fetch_maxdonate['Email'];
		$userid = $fetch_maxdonate['Id'];
		
		# top works
		
		if($maxdonate >= 0 && $maxdonate < 10) {
			
			$categoryname = mysql_real_escape_string($_POST['category']);
			$cat_ret=mysql_query("SELECT * FROM category WHERE cat_name='$categoryname'") or die(mysql_error());	
			$fetchcat_id=mysql_fetch_array($cat_ret);
			$cat_id = $fetchcat_id['cat_id'];
			
			$item =  mysql_real_escape_string($_POST['item']);
			$descp = mysql_real_escape_string($_POST['descp']);
				
			$sql_item_rec=mysql_query("select * from item where item_name='$item'") or die(mysql_error());	
			$fetch_item_rec=mysql_fetch_array($sql_item_rec);
			$time = date("Y-m-d H:i:s"); 	
			
			$itemid = $fetch_item_rec['prod_id'];
			
			$insert= "INSERT INTO donate_item (`donated_id` ,`category` ,`sub_category` ,`item_id` ,`item_description` ,`condition` ,`donated_by` ,
							`claimed_by` ,`claim_status` ,`donated_time`, `claimed_time` ,`donate_type`,`image`,`show_status`,`ebase`,`emax`,`emed`,`emin`,`item_name`)

							VALUES ( '1', '$cat_id', '', '$itemid', '$descp', '$condition', '$userid', '', '0', NOW(), 0 ,'unique','','0','0','0','0','0','')";
			
			mysql_query($insert) or die(mysql_error());
			
			
			$lastid = mysql_insert_id();	
			$currdate = date("Y-m-dH-i-s");
			$rename = $currdate.$pic;
			$uploadDir = '../drive/donations/'; 
			$filename = mysql_real_escape_string($_FILES['image']['name']);
			
			
			$uploadfile = $uploadDir.$rename;             
			move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
			
			$resize = new ResizeImage($uploadfile);
			$resize->resizeTo(300,300,'exact');
			$resize->saveImage($uploadfile);

			$update = "update donate_item set image='$rename' where id='$lastid'";
			mysql_query($update) or die(mysql_error());
			
			$ids = '0000000';
			$record = $ids.$lastid;
			// $record;
			$update = "update donate_item set donated_id='$record' where id='$lastid'";
			mysql_query($update);
			
			$insert2= "INSERT INTO history (`donate_id` ,`category` ,`sub_category` ,`item_id` ,`item_description` ,`condition` ,`donated_by` ,
						`claimed_by` ,`claim_status` ,`donated_time` , `claimed_time`,`donate_type`,`show_status`,`ebase`,`emax`,`emed`,`emin`,`item_name`)

						VALUES ( '$record', '$cat_id', '', '$item', '$descp', '$condition', '$userid', '', '0', 'NOW()', 'NOW()', 'unique','0','0','0','0','0','')";
				
					mysql_query($insert2) or die(mysql_error());
		
			
	
			$incr= $maxdonate + 1;
			$update_maxdonate="update userinfo set num_donated='$incr' where Id='$userid'";
			mysql_query($update_maxdonate) or die(mysql_error());

			$subject= "Donated an unique item";
			$headers = "From: procitystaff@gmail.com \r\n";
			$donate_msg="Hey Procitizen $donate_username! \n\nYou have successfully donated an item to Procity’s network! Now you have a few options. Wait for us to process your unique item within 48 hours, then you can sit back, relax, and wait for someone to claim your item, or if you want your ProPoints even faster and there is a Vietnam Veterans of America pick up date approaching, stop by the donation location and give your item to charity -- Instant ProPoints!\n\nYou can find the pick up information on the the homepage of myprocity.com!\n\nBest wishes,\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
			$headers .= "Reply-To: procitystaff@gmail.com\r\n";
			$headers .= "Return-Path: procitystaff@gmail.com\r\n";
				
			mail($donator_email,$subject,$donate_msg,$headers);
		
			$response["success"] = 1;
			echo json_encode($response);
		}
		
	} else {
	
		# file too big
		$response["success"] = -1;
		echo json_encode($response);
		
	}
		
}

?>
