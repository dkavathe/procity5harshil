-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 10, 2014 at 09:44 AM
-- Server version: 5.1.69
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `procity`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `businessmin` int(100) NOT NULL,
  `businessavg` int(100) NOT NULL,
  `businessmax` int(100) NOT NULL,
  `orgproject` int(100) NOT NULL,
  `orghourly` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `businessmin`, `businessavg`, `businessmax`, `orgproject`, `orghourly`) VALUES
(1, 'procitystaff', '2e5dad94e496a9440285d5f63ae9bcd0', 13, 19, 25, 47, 12);

-- --------------------------------------------------------

--
-- Table structure for table `businessinfo`
--

CREATE TABLE IF NOT EXISTS `businessinfo` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(10) NOT NULL,
  `numsales` int(100) NOT NULL,
  `numearned` int(100) NOT NULL,
  `numupload` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `businessinfo`
--

INSERT INTO `businessinfo` (`id`, `name`, `username`, `email`, `password`, `status`, `numsales`, `numearned`, `numupload`) VALUES
(1, 'Procitystaff', 'procitystaff', 'procitystaff@gmail.com', '2e5dad94e496a9440285d5f63ae9bcd0', 0, 0, 0, 0),
(2, 'Marathon Deli', 'marathonsdeliumd', 'agape31978@aol.com', '7d1c03ad7104a3d63dac94cdcfad2ef7', 1, 8, 2, 1),
(3, 'Garbanzo Mediterranean Grill', 'garbanzoumd', 'cristina.mudge@eatgarbanzo.com', '1e7c885a6a93557ed397c1f90d6f99c8', 1, 25, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `business_item`
--

CREATE TABLE IF NOT EXISTS `business_item` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `businessid` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `propointworth` int(100) NOT NULL,
  `saleworth` int(100) NOT NULL,
  `numcount` int(100) NOT NULL,
  `numsales` int(100) NOT NULL,
  `rewardlinkpath` varchar(1000) NOT NULL,
  `showstatus` int(100) NOT NULL,
  `type` int(11) NOT NULL,
  `promocode` varchar(100) NOT NULL,
  `websitelink` varchar(1000) NOT NULL,
  `picpath` varchar(1000) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `business_item`
--

INSERT INTO `business_item` (`id`, `businessid`, `name`, `description`, `propointworth`, `saleworth`, `numcount`, `numsales`, `rewardlinkpath`, `showstatus`, `type`, `promocode`, `websitelink`, `picpath`, `time`, `count`) VALUES
(1, 2, '$1 off a $5 purchase', 'Get one dollar off a five dollar purchase!', 13, 4, 43, 172, '1marathonsdelipdf.pdf', 1, 0, '', '', '1marathonsdelilogo.jpg', '2014-01-19 20:04:49', -1),
(2, 3, 'Free Falafel', 'get a free falafel from Garbanzo Mediterranean Grill!', 14, 5, 6, 30, '', 1, 0, '', '', '2falafel.JPG', '2014-01-20 19:56:02', -1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `cat_id` int(50) NOT NULL,
  `cat_name` varchar(500) NOT NULL,
  `CatWorth` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_id`, `cat_name`, `CatWorth`) VALUES
(1, 1, 'Clothing', ''),
(2, 2, 'Clothing Accessories', ''),
(3, 3, 'Entertainment', ''),
(4, 4, 'Household', ''),
(5, 5, 'Miscellaneous', ''),
(6, 6, 'Novel Books', ''),
(7, 7, 'Shoes', ''),
(8, 8, 'Sports', ''),
(9, 9, 'Supplies', ''),
(10, 10, 'Textbooks', ''),
(11, 11, 'Toys', '');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(250) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `text`, `timestamp`, `item_id`, `user_id`) VALUES
(18, 'asdf', '2014-01-22 16:07:24', 188, 22),
(35, 'when do you want to meet up?', '2014-01-28 16:39:47', 55, 1),
(36, 'how about Tuesday at 3pm! At STAMP?', '2014-01-28 16:40:41', 55, 22);

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE IF NOT EXISTS `conditions` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `condition` varchar(500) NOT NULL,
  `condition_pts` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `condition`, `condition_pts`) VALUES
(1, 'My treasure, your pleasure (Like new/New)', 5),
(2, 'Just needs a new home (Used/Show of wear and tear)', 10),
(3, 'Out of mind, out of sight (Usable/Shows significant signs of use)', 15);

-- --------------------------------------------------------

--
-- Table structure for table `dealrecords`
--

CREATE TABLE IF NOT EXISTS `dealrecords` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `businessid` int(100) NOT NULL,
  `userid` int(100) NOT NULL,
  `rewardid` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='when many people press claim adds to this table,when confirm' AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Table structure for table `donate_item`
--

CREATE TABLE IF NOT EXISTS `donate_item` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `donated_id` int(10) NOT NULL,
  `category` varchar(500) NOT NULL,
  `sub_category` varchar(500) NOT NULL,
  `item_id` varchar(500) NOT NULL,
  `item_description` text NOT NULL,
  `condition` text NOT NULL,
  `donated_by` varchar(500) NOT NULL,
  `claimed_by` varchar(500) NOT NULL,
  `claim_status` int(1) NOT NULL,
  `donated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `claimed_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `donate_type` varchar(500) NOT NULL,
  `image` text NOT NULL,
  `show_status` int(1) NOT NULL,
  `ebase` int(10) NOT NULL,
  `emax` int(10) NOT NULL,
  `emed` int(10) NOT NULL,
  `emin` int(10) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=200 ;

--
-- Dumping data for table `donate_item`
--

INSERT INTO `donate_item` (`id`, `donated_id`, `category`, `sub_category`, `item_id`, `item_description`, `condition`, `donated_by`, `claimed_by`, `claim_status`, `donated_time`, `claimed_time`, `donate_type`, `image`, `show_status`, `ebase`, `emax`, `emed`, `emin`, `item_name`) VALUES
(139, 139, '5', '', 'Harmonica', 'new harmonica C scale', '1', '22', '', 0, '2013-09-26 17:28:47', '0000-00-00 00:00:00', 'unique', '139harmonia.jpg', 1, 20, 25, 24, 23, 'Harmonica'),
(143, 143, '3', '', 'iPod touch', '2nd generation ipod touch, still works, pass code is 2400', '3', '1', '', 0, '2013-09-26 17:29:07', '0000-00-00 00:00:00', 'unique', '143CAM00039.jpg', 1, 44, 28, 27, 26, 'iPod touch 2nd gen'),
(171, 171, '7', '', '145', 'womens slippers size small', '1', '1', '', 0, '2014-01-02 01:53:31', '0000-00-00 00:00:00', 'unique', '171CAM00063.jpg', 1, 15, 24, 21, 18, 'Women''s Sandals'),
(170, 170, '9', '', '155', 'GUESS backpack', '1', '1', '', 0, '2013-10-23 18:04:14', '0000-00-00 00:00:00', 'unique', '170CAM00062.jpg', 1, 17, 28, 24, 21, 'GUESS Backpack'),
(62, 62, '7', '', '128', 'Nike 6.0s worn once around campus', '1', '21', '', 0, '2014-01-18 15:18:07', '0000-00-00 00:00:00', 'unique', '6220130907_134054.jpg', 1, 23, 37, 33, 28, 'Nike shoes'),
(16, 16, '10', '', 'AASP200 - Textbooks16', 'AASP200 - Textbooks barely used', '2', '22', '', 0, '2013-09-26 17:26:44', '0000-00-00 00:00:00', 'unique', '16CAM00023.jpg', 1, 47, 50, 49, 48, 'AASP200 - Textbooks'),
(19, 19, '1', '', 'UMD Terp Hoodie - Medium19', 'Hey this item is free to go! However it does have a small, ugly stain on the front of it. If you take the time to buy some Tide-to-go....its as good as new!', '2', '2', '', 0, '2013-09-26 17:26:55', '0000-00-00 00:00:00', 'unique', '19IMG_0302.JPG', 1, 35, 56, 49, 42, 'UMD Terp Hoodie - Medium'),
(20, 20, '1', '', '12', 'M collared shirt, small bleach stain at bottom of shirt', '2', '28', '', 0, '2013-10-18 22:28:18', '0000-00-00 00:00:00', 'unique', '200903131528.jpg', 1, 15, 24, 21, 18, 'Polo shirt'),
(21, 21, '1', '', '12', 'M collared shirt, been worn many times.  Still in good condition', '2', '28', '', 0, '2013-10-18 22:28:33', '0000-00-00 00:00:00', 'unique', '210903131527a.jpg', 1, 15, 24, 21, 18, 'Polo shirt'),
(22, 22, '1', '', '17', 'M polo t-shirt, still very wearable.  Small detergent stains only visible when not wearing', '3', '28', '', 0, '2013-10-18 22:28:47', '0000-00-00 00:00:00', 'unique', '220903131527.jpg', 1, 13, 21, 19, 16, 'Polo shirt'),
(23, 23, '1', '', '17', 'L t-shirt, reads \\"COLLEGE\\" on front and has the mascot on the back', '1', '28', '', 0, '2013-10-18 22:29:17', '0000-00-00 00:00:00', 'unique', '230903131526a.jpg', 1, 13, 21, 19, 16, 'Tshirt'),
(24, 24, '1', '', '12', 'XL dress shirt, never been worn', '1', '28', '', 0, '2013-10-18 22:29:30', '0000-00-00 00:00:00', 'unique', '240903131525.jpg', 1, 15, 24, 21, 18, 'Dress shirt'),
(144, 144, '1', '', '20', 'Women''s jeggings, tags off but never been worn.  Size small 4/6', '1', '114', '', 0, '2013-10-18 22:29:47', '0000-00-00 00:00:00', 'unique', '144IMG_20130924_210750.jpg', 1, 10, 16, 14, 12, 'Women''s Pants'),
(50, 50, '11', '', 'Lightsaber', 'I have upgraded to mace windu\\''s light saber and I have no need for this lightsaber any longer.  I have won many duels with this lightsaber and I am sure you will too. ', '2', '31', '', 0, '2013-10-02 01:19:43', '0000-00-00 00:00:00', 'unique', '50photo 3.JPG', 1, 7, 9, 8, 7, 'Lightsaber'),
(55, 55, '3', '', '68', 'computer screen with TV reciever', '2', '1', '22', 2, '2013-10-18 22:29:57', '2014-01-28 16:38:18', 'unique', '5520130907_133609.jpg', 1, 60, 78, 75, 73, 'Television'),
(138, 138, '10', '', 'CHEM135 Textbook', 'Intro chem text by Nivaldo J. Tro', '2', '23', '', 0, '2013-09-26 17:28:43', '0000-00-00 00:00:00', 'unique', '13820130915_135812.jpg', 1, 25, 35, 34, 33, 'CHEM135 Textbook'),
(59, 59, '5', '', 'Womens Handbag new', 'new womens purse/handbag', '1', '21', '', 0, '2013-09-26 17:27:26', '0000-00-00 00:00:00', 'unique', '5920130907_133748.jpg', 1, 25, 28, 27, 26, 'Womens Handbag new'),
(65, 65, '7', '', '129', 'Polo shoes size 10', '1', '22', '', 0, '2013-10-18 22:30:17', '0000-00-00 00:00:00', 'unique', '6520130907_134154.jpg', 1, 22, 36, 31, 27, 'Polo shoes'),
(66, 66, '7', '', '128', 'Nikes size 10', '3', '22', '1', 1, '2014-01-16 19:45:12', '2014-02-07 19:33:03', 'unique', '6620130907_134209.jpg', 1, 23, 37, 33, 28, 'Nike sneakers'),
(67, 67, '9', '', 'Women\\''s handbag', 'big womens handbag brand new', '1', '22', '', 0, '2013-09-26 17:27:43', '0000-00-00 00:00:00', 'unique', '6720130907_134221.jpg', 1, 23, 26, 25, 24, 'Womens big purse'),
(70, 70, '4', '', '116', 'Big Fan perfect for North Campus, worn out but still works', '3', '22', '', 0, '2013-10-18 22:31:47', '0000-00-00 00:00:00', 'unique', '7020130907_134331.jpg', 1, 27, 44, 38, 33, 'Dorm fan'),
(71, 71, '2', '', 'Women\\''s Sunglasses', 'Large H&M Sunglasses', '2', '15', '', 0, '2013-09-26 17:28:04', '0000-00-00 00:00:00', 'unique', '71Picture 1001.JPG', 1, 5, 8, 7, 6, 'Women''s Sunglasses'),
(72, 72, '2', '', 'Women\\''s PDA Case', 'Steve Madden Teal PDA Case - can hold a phone and 7 cards', '1', '15', '', 0, '2013-09-26 17:28:08', '0000-00-00 00:00:00', 'unique', '72Picture 1000.JPG', 1, 7, 9, 8, 8, 'Women''s PDA Case'),
(73, 80, '2', '', '29', 'Adjustable Red Betsey Johnson Belt', '1', '15', '', 0, '2013-10-18 22:32:04', '0000-00-00 00:00:00', 'unique', '73Picture 1003.JPG', 1, 13, 21, 19, 16, 'Belt'),
(74, 80, '9', '', 'Drawer', 'Yellow Two-Tiered Drawer', '2', '15', '', 0, '2013-09-26 14:58:28', '0000-00-00 00:00:00', 'unique', '74Picture 1004.JPG', 1, 21, 27, 26, 25, 'Drawer'),
(76, 80, '7', '', '139', 'Black leather boots with heels. Barely used, but one has a scratch. It\\''s hardly noticeable with sharpie lol', '2', '37', '', 0, '2013-10-18 22:32:16', '0000-00-00 00:00:00', 'unique', '7620130911_091207.jpg', 1, 17, 28, 24, 21, 'Women''s Boots'),
(77, 80, '10', '', 'The Making and Remaking of a Multiculturalist by Carlos Cortes', 'Book needed for BSCV 301, a CIVICUS class.', '2', '37', '', 0, '2013-09-26 14:58:28', '0000-00-00 00:00:00', 'unique', '7720130911_091419.jpg', 1, 18, 21, 20, 19, 'The Making and Remaking of a Multiculturalist by Carlos Cortes'),
(78, 80, '1', '', '17', 'White Polo size S, never used really.', '1', '37', '', 0, '2013-10-18 22:32:32', '0000-00-00 00:00:00', 'unique', '7820130911_090015.jpg', 1, 13, 21, 19, 16, 'Men\\''s Tops'),
(80, 80, '1', '', '5', 'Brand new jeans, never used. Size 28 X 30 (Silver Tab - Baggy)', '1', '37', '', 0, '2014-01-16 18:29:02', '0000-00-00 00:00:00', 'unique', '8020130911_085817.jpg', 1, 18, 29, 26, 22, 'Men''s Pants'),
(172, 172, '5', '', 'Conair [number] cut 20 piece haircut kit', 'Originally mistook for an electric razor so it is still in brand new condition and part of the packaging is still unopened. ', '1', '47', '', 0, '2013-11-11 15:41:58', '0000-00-00 00:00:00', 'unique', '172IMG_0143.JPG', 1, 23, 27, 26, 25, 'Conair haircut kit');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `donate_id` int(12) NOT NULL,
  `category` text NOT NULL,
  `sub_category` text NOT NULL,
  `item_id` text NOT NULL,
  `item_description` text NOT NULL,
  `condition` text NOT NULL,
  `donated_by` text NOT NULL,
  `claimed_by` text NOT NULL,
  `claim_status` text NOT NULL,
  `donated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `donate_type` text NOT NULL,
  `show_status` int(11) NOT NULL,
  `ebase` int(11) NOT NULL,
  `emax` int(11) NOT NULL,
  `emed` int(11) NOT NULL,
  `emin` int(11) NOT NULL,
  `item_name` varchar(300) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `donate_id`, `category`, `sub_category`, `item_id`, `item_description`, `condition`, `donated_by`, `claimed_by`, `claim_status`, `donated_time`, `donate_type`, `show_status`, `ebase`, `emax`, `emed`, `emin`, `item_name`, `image`) VALUES
(139, 189, '1', '', 'cool shoes', 'item8', '', '22', '', '0', '2014-01-22 15:42:39', 'unique', 0, 158, 0, 221, 190, '', ''),
(8, 0, '10', '', 'Physics for Scientists and Engineers Volume 1 by Randall Knight8', 'Physics textbook for PHYS161. All the text is there but the cover separated from the pages.', '3', '17', '130', '3', '2013-10-18 22:58:42', 'unique', 1, 24, 38, 36, 34, 'Physics for Scientists and Engineers Volume 1 by Randall Knight', '8photo.jpg'),
(9, 0, '5', '', 'UMD Eye Black9', 'Stick-on eye black that says ALL IN. Two pairs of eye black, one is black background and one is MD flag.', '1', '17', '25', '1', '2013-10-18 22:59:06', 'unique', 1, 6, 10, 8, 7, 'Terps black eye', '9image_1.jpg'),
(10, 0, '5', '', 'LED night light10', 'Battery powered (2 AAA) with detachable and buildable blocks so the shape can be reconfigured, similar to LEGOs. The base is 2x2 inches and the height depends on how you build it.', '1', '17', '49', '1', '2013-10-18 22:59:22', 'unique', 1, 5, 8, 7, 6, 'LED night light', '10image.jpg'),
(11, 0, '10', '', 'BSCV181 - Tuesdays with Morrie11', 'CIVICUS first semester book Tuesday with Morrie in great condition (BSCV181)', '1', '21', '', '0', '2013-10-18 22:56:49', 'unique', 1, 9, 11, 10, 9, 'BSCV181 - Tuesdays with Morrie', '11CAM00018.jpg'),
(12, 0, '10', '', 'BSCV301 - Leadership the Eleanor Roosevelt Way 12', 'wrote a bit in it, but its good to go, for Sue\\''s class in CIVICUS sophomore year', '2', '21', '', '0', '2013-10-18 22:57:01', 'unique', 1, 19, 25, 24, 23, 'BSCV301 - Leadership the Eleanor Roosevelt Way', '12CAM00019.jpg'),
(13, 0, '10', '', 'SOCY105 - Social Problems 5th Edition13', 'SOCY105 textbook in good condition, paperback version', '1', '21', '', '0', '2013-10-18 22:57:18', 'unique', 1, 25, 31, 30, 29, 'SOCY105 - Social Problems', '13CAM00020,jpg'),
(137, 187, '3', '', 'psp', 'A PSP with cool case', '', '21', '1', '3', '2014-01-18 16:11:37', 'unique', 0, 71, 0, 99, 85, '', ''),
(138, 188, '1', '', 'PSP with case', 'item7', '', '22', '', '0', '2014-01-22 15:40:46', 'unique', 0, 36, 0, 50, 43, '', ''),
(135, 185, '3', '', 'PSP with case', 'cool psp with case intact', '', '21', '1', '3', '2014-01-18 15:59:23', 'unique', 0, 50, 0, 70, 60, '', ''),
(136, 186, '3', '', 'PSP with case', 'cool psp with case intact!!', '', '21', '1', '3', '2014-01-18 16:05:26', 'unique', 0, 50, 0, 70, 60, '', ''),
(16, 0, '10', '', 'AASP200 - Textbooks16', 'AASP200 - Textbooks barely used', '2', '22', '', '0', '2013-09-02 11:39:54', 'unique', 1, 47, 50, 49, 48, 'AASP200 - Textbooks', ''),
(17, 0, '10', '', 'CMLT280 - Film Art in a Global Society17', 'Contemporary World Cinema, Shohini Chaudhuri ', '1', '22', '', '0', '2013-09-02 11:40:22', 'unique', 1, 23, 27, 26, 25, 'CMLT280 - Film Art in a Global Society', ''),
(18, 0, '10', '', 'HIST157 - assorted books18', 'highlighted Of the People rest is like brand new', '2', '22', '', '0', '2013-09-02 11:40:57', 'unique', 1, 57, 59, 58, 57, 'HIST157 ', ''),
(19, 0, '1', '', 'UMD Terp Hoodie - Medium19', 'Hey this item is free to go! However it does have a small, ugly stain on the front of it. If you take the time to buy some Tide-to-go....its as good as new!', '2', '2', '', '0', '2013-09-03 16:59:55', 'unique', 1, 35, 56, 49, 42, 'UMD Terp Hoodie - Medium', ''),
(20, 0, '1', '', '12', 'M collared shirt, small bleach stain at bottom of shirt', '2', '28', '', '0', '2013-10-18 23:06:32', 'unique', 1, 15, 24, 21, 18, 'Polo shirt', '200903131528.jpg'),
(21, 0, '1', 'Men\\''s Tops', '12', 'M collared shirt, been worn many times.  Still in good condition', '2', '28', '', '0', '2013-10-18 23:07:07', 'unique', 1, 15, 24, 21, 18, 'Polo shirt', '220903131527.jpg'),
(22, 0, '1', 'Men\\''s Tops', '17', 'M polo t-shirt, still very wearable.  Small detergent stains only visible when not wearing', '3', '28', '', '0', '2013-10-18 23:07:03', 'unique', 1, 13, 21, 19, 16, 'Polo shirt', '240903131525.jpg'),
(23, 0, '1', 'Men\\''s Tops', '17', 'L t-shirt, reads \\"COLLEGE\\" on front and has the mascot on the back', '1', '28', '136', '1', '2013-10-18 23:06:58', 'unique', 1, 13, 21, 19, 16, 'Tshirt', '230903131526a.jpg'),
(24, 0, '1', 'Men\\''s Tops', '12', 'XL dress shirt, never been worn', '1', '28', '', '0', '2013-09-03 12:51:10', 'generic', 1, 15, 24, 21, 18, '', ''),
(134, 184, '3', '', 'psp', 'brand new psp', '', '21', '1', '3', '2014-01-18 15:40:42', 'unique', 0, 79, 0, 111, 95, '', ''),
(32, 0, '9', 'Calculator', '159', 'calculator scientific good condition with battery life', '1', '1', '', '0', '2013-10-18 23:08:30', 'unique', 1, 14, 23, 20, 17, '', ''),
(133, 183, '1', '', 'TEST', 'test max price', '', '154', '', '4', '2013-11-06 20:50:32', 'unique', 0, 100000, 0, 140000, 120000, '', ''),
(132, 182, '5', '', 'Desktop', 'Desktop Computer', '', '154', '', '4', '2013-10-26 21:44:51', 'unique', 0, 160, 0, 224, 192, '', ''),
(131, 181, '1', '', 'shoes', 'testa', '', '154', '155', '4', '2013-10-24 15:33:17', 'unique', 0, 18, 0, 25, 22, '', ''),
(36, 0, '4', '', 'Sandwich Maker', 'sandwich maker still works ', '3', '1', '12', '3', '2013-09-12 17:47:47', 'unique', 0, 0, 0, 0, 0, '', ''),
(37, 0, '1', 'Men\\''s Tops', '17', 'Art Attack White T', '1', '32', '', '5', '2013-10-18 23:08:22', 'unique', 1, 13, 21, 19, 16, '', '492013-09-06 20.17.32.jpg'),
(38, 0, '1', 'Men\\''s Tops', '17', 'MGM Sustainability Black T with Word Tree', '1', '32', '', '5', '2013-10-18 23:08:27', 'unique', 1, 13, 21, 19, 16, '', '482013-09-06 20.16.24.jpg'),
(39, 0, '11', '', 'Lightsaber', 'I have upgraded to mace windu\\''s light saber and I have no need for this lightsaber any longer.  I have won many duels with this lightsaber and I am sure you will too. ', '2', '31', '', '0', '2013-10-18 22:02:52', 'unique', 0, 0, 0, 0, 0, '', '50photo 3.JPG'),
(40, 0, '8', '', 'Dumbells 8 lbs', 'dumbells 8lbs', '1', '1', '114', '3', '2013-10-18 22:56:19', 'unique', 0, 0, 0, 0, 0, '', '51dumbells.jpg'),
(41, 0, '3', '', 'PSP with case', 'PSP with case no charger no games, works like new', '1', '1', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(42, 0, '7', 'Women\\''s Athletic', '136', 'BRAND NEW PUMA Osuran NM Sneaker; Size 8; White & Green', '1', '40', '123', '2', '2013-10-18 23:08:16', 'generic', 1, 23, 37, 33, 28, '', '53IMG00102.jpg'),
(43, 0, '9', '', 'Pencil holders', 'holds pencils, sharpies, pens, erasers anything you need', '1', '1', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(44, 0, '3', 'Television', '68', 'computer screen with TV reciever', '2', '1', '', '0', '2013-09-07 11:13:22', 'generic', 1, 139, 223, 195, 167, '', ''),
(46, 0, '7', 'Men\\''s Sandals', '134', 'shower flip flips', '1', '21', '1', '3', '2013-09-12 17:34:38', 'generic', 1, 19, 31, 27, 23, '', ''),
(129, 179, '7', '', 'shoes', 'aosdjfoisd', '', '154', '155', '4', '2013-10-22 20:15:17', 'unique', 0, 27, 0, 38, 32, '', ''),
(130, 180, '7', '', 'shoes', 'ttesta', '', '154', '', '4', '2013-10-23 17:46:12', 'unique', 0, 17, 0, 24, 20, '', ''),
(48, 0, '5', '', 'Womens Handbag new', 'new womens purse/handbag', '1', '21', '', '0', '2013-10-18 23:12:04', 'unique', 0, 0, 0, 0, 0, '', '5920130907_133748.jpg'),
(49, 0, '2', '', 'Womens purse', 'pretty great condition womens handbag', '1', '21', '', '0', '2013-10-18 23:11:52', 'unique', 0, 0, 0, 0, 0, '', '6020130907_133814.jpg'),
(51, 0, '7', 'Men\\''s Athletic', '128', 'Nike 6.0s worn once around campus', '1', '21', '', '0', '2013-09-07 11:38:08', 'generic', 1, 23, 37, 33, 28, '', ''),
(128, 178, '1', '', 'TEST', 'TEST', '', '154', '', '4', '2013-10-19 14:49:23', 'unique', 0, 34, 0, 54, 41, '', ''),
(54, 0, '7', 'Men\\''s Athletic', '129', 'Polo shoes size 10', '1', '22', '7', '1', '2013-09-17 19:35:48', 'generic', 1, 7, 9, 8, 7, 'Pencil holders', ''),
(55, 0, '7', 'Men\\''s Athletic', '128', 'Nikes size 10', '3', '22', '', '0', '2013-09-07 11:43:55', 'generic', 1, 23, 37, 33, 28, '', ''),
(56, 0, '9', '', 'Women\\''s handbag', 'big womens handbag brand new', '1', '22', '', '0', '2013-10-18 23:11:11', 'unique', 0, 0, 0, 0, 0, '', '6720130907_134221.jpg'),
(57, 0, '3', 'Video Games', '105', 'Homefront for the xbox beat it, great game', '1', '22', '', '0', '2013-10-18 23:03:53', 'generic', 1, 20, 32, 28, 24, '', '6820130907_134313.jpg'),
(58, 0, '3', 'Video Games', '106', 'Dead Island, for xbox', '1', '22', '', '0', '2013-10-18 23:04:16', 'generic', 1, 30, 48, 42, 36, '', '6920130907_134318.jpg'),
(59, 0, '4', 'Fan', '116', 'Big Fan perfect for North Campus, worn out but still works', '3', '22', '', '0', '2013-10-18 23:04:32', 'generic', 1, 40, 45, 43, 42, 'Large Fan', ''),
(60, 0, '2', '', 'Women\\''s Sunglasses', 'Large H&M Sunglasses', '2', '15', '', '0', '2013-10-18 23:11:30', 'unique', 0, 0, 0, 0, 0, '', '71Picture 1001.jpg'),
(61, 0, '2', '', 'Women\\''s PDA Case', 'Steve Madden Teal PDA Case - can hold a phone and 7 cards', '1', '15', '73', '1', '2013-10-18 23:11:41', 'unique', 0, 0, 0, 0, 0, '', '72Picture 1000.jpg'),
(62, 0, '2', 'Belts', '29', 'Adjustable Red Betsey Johnson Belt', '1', '15', '', '0', '2013-09-08 10:18:47', 'generic', 1, 13, 21, 19, 16, '', ''),
(63, 0, '9', '', 'Drawer', 'Yellow Two-Tiered Drawer', '2', '15', '9', '1', '2013-10-18 23:12:20', 'unique', 0, 0, 0, 0, 0, '', '74Picture 1004.jpg'),
(64, 0, '8', '', 'Nike Duffle Bag (pink)', 'Very convenient for sports and traveling. It\\''s only been used a few times so it\\''s still in good condition!', '2', '37', '', '0', '2013-10-18 23:09:58', 'unique', 0, 0, 0, 0, 0, 'Nike Duffle Bag (pink)', '7520130911_090639.jpg'),
(65, 0, '7', 'Women\\''s Boots', '139', 'Black leather boots with heels. Barely used, but one has a scratch. It\\''s hardly noticeable with sharpie lol', '2', '37', '', '0', '2013-09-11 09:51:55', 'generic', 1, 17, 28, 24, 21, '', ''),
(66, 0, '10', '', 'The Making and Remaking of a Multiculturalist by Carlos Cortes', 'Book needed for BSCV 301, a CIVICUS class.', '2', '37', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(67, 0, '1', 'Men\\''s Tops', '17', 'White Polo size S, never used really.', '1', '37', '', '0', '2013-09-11 10:14:42', 'generic', 1, 13, 21, 19, 16, '', ''),
(69, 0, '1', 'Men\\''s Pants', '5', 'Brand new jeans, never used. Size 28 X 30 (Silver Tab - Baggy)', '1', '37', '', '0', '2013-09-11 10:31:24', 'generic', 1, 18, 29, 26, 22, '', ''),
(94, 0, '10', '', 'CHEM135 Textbook', 'Intro chem text by Nivaldo J. Tro', '2', '23', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(95, 0, '5', '', 'Harmonica', 'new harmonica C scale', '1', '22', '', '0', '2013-10-18 23:13:06', 'unique', 0, 0, 0, 0, 0, 'Harmonica', '139harmonia.jpg'),
(96, 0, '1', '', 'Men''s jacket', 'fresh new jacket, thin layer size small', '1', '1', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(97, 0, '1', '', 'Flight Suit', 'size medium flight suit perfect for Halloween', '1', '1', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(98, 0, '1', '', '3', 'size 28/30 pants used once', '1', '1', '', '0', '2013-10-18 22:20:55', 'unique', 1, 16, 26, 23, 20, 'Men''s Pants', '142CAM00038.jpg'),
(99, 0, '3', '', 'iPod touch', '2nd generation ipod touch, still works, pass code is 2400', '3', '1', '', '0', '0000-00-00 00:00:00', 'unique', 0, 0, 0, 0, 0, '', ''),
(100, 0, '1', 'Women''s Pants', '20', 'Women''s jeggings, tags off but never been worn.  Size small 4/6', '1', '114', '', '0', '2013-09-25 02:10:03', 'generic', 1, 10, 16, 14, 12, '', ''),
(140, 190, '4', '', 'backpack', 'item9', '', '22', '', '0', '2014-01-22 15:43:15', 'unique', 0, 27, 0, 38, 32, '', ''),
(141, 191, '10', '', 'Physics book', 'item10', '', '22', '1', '1', '2014-01-22 17:10:37', 'unique', 0, 39, 0, 55, 47, '', ''),
(122, 170, '9', 'Backpack', '155', 'GUESS backpack', '1', '1', '21', '1', '2014-01-28 16:39:04', 'unique', 1, 17, 28, 24, 21, 'GUESS Backpack', '170CAM00062.jpg'),
(123, 171, '7', 'Women''s Sandals', '145', 'womens slippers size small', '1', '1', '2', '1', '2014-01-02 01:53:31', 'unique', 1, 15, 24, 21, 18, 'Women''s Sandals', '171CAM00063.jpg'),
(124, 172, '5', '', 'Conair [number] cut 20 piece haircut kit', 'Originally mistook for an electric razor so it is still in brand new condition and part of the packaging is still unopened. ', '1', '47', '199', '1', '2013-11-11 15:41:58', 'unique', 0, 0, 0, 0, 0, 'Conair haircut kit', '172IMG_0143[1].JPG'),
(125, 173, '5', '', 'iPhone car charger', 'iPhone car charger purchased for an iPhone 3G. Never used, but no original packaging.', '1', '147', '154', '4', '2013-10-28 16:42:15', 'unique', 0, 0, 0, 0, 0, 'iPhone car charger', '1734-3 Stuff 038.JPG'),
(142, 192, '1', '', 'PSP', 'ad', '', '1', '', '0', '2014-01-22 22:42:56', 'unique', 0, 65, 0, 91, 78, '', ''),
(143, 193, '3', '', 'PSP with case', 'asdf', '', '1', '', '4', '2014-01-22 19:20:16', 'unique', 0, 36, 0, 51, 44, '', ''),
(144, 194, '3', '', 'PSP with case', 'asd', '', '22', '1', '4', '2014-01-22 20:41:48', 'unique', 0, 36, 0, 50, 43, '', ''),
(145, 195, '1', '', 'test', 'test', '', '22', '', '0', '2014-01-25 18:23:55', 'unique', 0, 28, 0, 39, 34, '', ''),
(146, 196, '8', '', 'asdfs', 'asd', '', '22', '', '0', '2014-01-25 18:33:23', 'unique', 0, 225, 0, 315, 270, '', ''),
(147, 197, '1', '', 'asdf', 'asdf', '', '22', '', '0', '2014-01-25 18:56:24', 'unique', 0, 78, 0, 109, 94, '', ''),
(148, 198, '9', '', 'asdf', 'sdf', '', '22', '', '0', '2014-01-25 21:01:47', 'unique', 0, 78, 0, 109, 94, '', ''),
(149, 199, '8', '', 'asdf', 'asd', '1', '22', '', '0', '2014-01-25 21:07:59', 'unique', 0, 0, 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `prod_id` int(50) NOT NULL,
  `sub_category` varchar(500) NOT NULL,
  `cat_id` int(4) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `ebase` varchar(500) NOT NULL,
  `emax` varchar(500) NOT NULL,
  `emed` varchar(500) NOT NULL,
  `emin` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`prod_id`, `sub_category`, `cat_id`, `item_name`, `ebase`, `emax`, `emed`, `emin`) VALUES
(1, 'Men''s Pants', 1, 'General - Corduroys', '16', '26', '23', '20'),
(2, 'Men''s Pants', 1, 'General - Denim', '11', '18', '16', '14'),
(3, 'Men''s Pants', 1, 'General - Dress', '16', '26', '23', '20'),
(4, 'Men''s Pants', 1, 'General - Sweatpants', '16', '26', '23', '20'),
(5, 'Men''s Pants', 1, 'Levi''s - Denim', '18', '29', '26', '22'),
(6, 'Men''s Pants', 1, 'Wrangler - Denim', '12', '20', '17', '15'),
(7, 'Men''s Shorts', 1, 'General - Cargo', '12', '20', '17', '15'),
(8, 'Men''s Shorts', 1, 'General - Plain', '11', '18', '16', '14'),
(9, 'Men''s Shorts', 1, 'General - Swimming', '13', '21', '19', '16'),
(10, 'Men''s Shorts', 1, 'Levi''s - Cargo', '15', '24', '21', '18'),
(11, 'Men''s Shorts', 1, 'Levi''s - Plain', '14', '23', '20', '17'),
(12, 'Men''s Tops', 1, 'General - Dress Shirt', '15', '24', '21', '18'),
(13, 'Unisex Clothing', 1, 'General - Heavy Jacket', '24', '39', '34', '29'),
(14, 'Unisex Clothing', 1, 'General - Hoodie', '15', '24', '21', '18'),
(15, 'Unisex Clothing', 1, 'General - Light Jacket', '17', '28', '24', '21'),
(16, 'Men''s Tops', 1, 'General - Sweater', '14', '23', '20', '17'),
(17, 'Men''s Tops', 1, 'General - T-Shirt', '13', '21', '19', '16'),
(18, 'Women''s Dresses', 1, 'General - One-piece', '12', '20', '17', '15'),
(19, 'Women''s Pants', 1, 'General - Casual', '12', '20', '17', '15'),
(20, 'Women''s Pants', 1, 'General - Denim', '10', '16', '14', '12'),
(21, 'Women''s Shorts', 1, 'General - Knee Length', '12', '20', '17', '15'),
(22, 'Women''s Shorts', 1, 'General - Thigh', '7', '12', '10', '9'),
(23, 'Women''s Skirts', 1, 'General', '8', '13', '12', '10'),
(24, 'Women''s Tops', 1, 'General - Blouse', '11', '18', '16', '14'),
(25, 'Women''s Tops', 1, 'General - Strap', '14', '23', '20', '17'),
(26, 'Women''s Tops', 1, 'General - Strapless', '13', '21', '19', '16'),
(27, 'Women''s Tops', 1, 'General - T-Shirt', '9', '15', '13', '11'),
(28, 'Belts', 2, 'General - Dress', '12', '20', '17', '15'),
(29, 'Belts', 2, 'General - Fashion', '13', '21', '19', '16'),
(30, 'Bracelets', 2, 'General - Ankle', '5', '8', '7', '6'),
(31, 'Bracelets', 2, 'General - Wrist', '5', '8', '7', '6'),
(32, 'Earings', 2, 'General - Metal', '8', '13', '12', '10'),
(33, 'Earings', 2, 'General - Non-metal', '5', '8', '7', '6'),
(34, 'Hats', 2, 'General - Casual', '12', '20', '17', '15'),
(35, 'Hats', 2, 'General - Fitted', '15', '24', '21', '18'),
(36, 'Hats', 2, 'General - Winter', '7', '12', '10', '9'),
(37, 'Necklace', 2, 'General - Metal', '10', '16', '14', '12'),
(38, 'Necklace', 2, 'General - Non-metal', '10', '16', '14', '12'),
(39, 'Scarves', 2, 'General - Knitted', '6', '10', '9', '8'),
(40, 'Scarves', 2, 'General - Silk', '7', '12', '10', '9'),
(41, 'Ties', 2, 'General - Bow Tie', '5', '8', '7', '6'),
(42, 'Ties', 2, 'General - Neck Tie', '7', '12', '10', '9'),
(43, 'Watches', 2, 'General - Metal', '15', '24', '21', '18'),
(44, 'Watches', 2, 'General - Plastic', '12', '20', '17', '15'),
(45, 'Board Games', 3, 'Chess Set - All Pieces', '23', '37', '33', '28'),
(46, 'Board Games', 3, 'General - All Pieces', '14', '23', '20', '17'),
(47, 'Cell Phone Case', 3, 'General - iPhone 4/4S', '6', '10', '9', '8'),
(48, 'Cell Phone Case', 3, 'General - iPhone 5', '6', '10', '9', '8'),
(49, 'Cell Phone Case', 3, 'General - Non-Smartphone', '5', '8', '7', '6'),
(50, 'Cell Phone Case', 3, 'General - Smartphone', '7', '12', '10', '9'),
(51, 'Cell Phone Case', 3, 'OtterBox - iPhone 4/4S', '21', '34', '30', '26'),
(52, 'Cell Phone Case', 3, 'OtterBox - iPhone 5', '19', '31', '27', '23'),
(53, 'iPod', 3, 'Nano - Generation 2 2GB', '25', '40', '35', '30'),
(54, 'iPod', 3, 'Nano - Generation 2 4GB', '35', '56', '49', '42'),
(55, 'iPod', 3, 'Nano - Generation 3 4GB', '45', '72', '63', '54'),
(56, 'iPod', 3, 'Nano - Generation 3 8GB', '55', '88', '77', '66'),
(57, 'iPod', 3, 'Nano - Generation 4 16GB', '85', '136', '119', '102'),
(58, 'iPod', 3, 'Nano - Generation 4 8GB', '50', '80', '70', '60'),
(59, 'iPod', 3, 'Nano - Generation 5 16GB', '110', '176', '154', '132'),
(60, 'iPod', 3, 'Nano - Generation 5 8GB', '65', '104', '91', '78'),
(61, 'Laptop', 3, '"MacBook Air - 11"""', '677', '1084', '948', '813'),
(62, 'Laptop', 3, '"MacBook Air - 13"""', '762', '1220', '1067', '915'),
(63, 'Laptop', 3, '"MacBook Pro - 13"""', '679', '1087', '951', '815'),
(64, 'Laptop', 3, '"MacBook Pro - 15"""', '696', '1114', '975', '836'),
(65, 'Laptop', 3, 'PC - Netbook (mini)', '130', '208', '182', '156'),
(66, 'Laptop', 3, 'PC - Notebook', '175', '280', '245', '210'),
(67, 'Laptop', 3, 'PC - Ultrabook', '427', '684', '598', '513'),
(68, 'Television', 3, '"LCD - 20-29"""', '139', '223', '195', '167'),
(69, 'Television', 3, '"LCD - 30-39"""', '165', '264', '231', '198'),
(70, 'Television', 3, '"LCD - 40-49"""', '334', '535', '468', '401'),
(71, 'Television', 3, '"LCD - Under 20"""', '73', '117', '103', '88'),
(72, 'Television', 3, '"Plasma - 40-49"""', '248', '397', '348', '298'),
(73, 'Television', 3, 'Universal Remote Control', '13', '21', '19', '16'),
(74, 'Video Game Accessories', 3, 'PS3 - Controller', '23', '37', '33', '28'),
(75, 'Video Game Accessories', 3, 'PS3 - Move Controller + Eye', '47', '76', '66', '57'),
(76, 'Video Game Accessories', 3, 'Xbox 360 - Controller', '19', '31', '27', '23'),
(77, 'Video Game Accessories', 3, 'Xbox 360 - Kinect Sensor', '48', '77', '68', '58'),
(78, 'Video Game Console', 3, 'Nintendo - 3DS XL', '168', '269', '236', '202'),
(79, 'Video Game Console', 3, 'Nintendo - DS Lite', '56', '90', '79', '68'),
(80, 'Video Game Console', 3, 'Nintendo - Wii', '71', '114', '100', '86'),
(81, 'Video Game Console', 3, 'PS3 - Original', '133', '213', '187', '160'),
(82, 'Video Game Console', 3, 'PS3 - Slim 120GB/160GB', '171', '274', '240', '206'),
(83, 'Video Game Console', 3, 'PS3 - Slim 320GB', '198', '317', '278', '238'),
(84, 'Video Game Console', 3, 'Xbox 360 - Elite', '114', '183', '160', '137'),
(85, 'Video Game Console', 3, 'Xbox 360 - White', '90', '144', '126', '108'),
(86, 'Video Game Console ', 3, 'Nintendo - 3DS', '119', '191', '167', '143'),
(87, 'Video Game Console ', 3, 'Xbox 360 - Slim', '136', '218', '191', '164'),
(88, 'Video Games', 3, '3DS - Game 2011', '10', '16', '14', '12'),
(89, 'Video Games', 3, '3DS - Game 2012', '15', '24', '21', '18'),
(90, 'Video Games', 3, '3DS - Game 2013', '25', '40', '35', '30'),
(91, 'Video Games', 3, 'DS - Game 2004/2005/2006/2007', '4', '7', '6', '5'),
(92, 'Video Games', 3, 'DS - Game 2008/2009/2010', '7', '12', '10', '9'),
(93, 'Video Games', 3, 'DS - Game 2011', '12', '20', '17', '15'),
(94, 'Video Games', 3, 'PS3 - Game 2006/2007', '7', '12', '10', '9'),
(95, 'Video Games', 3, 'PS3 - Game 2008/2009', '10', '16', '14', '12'),
(96, 'Video Games', 3, 'PS3 - Game 2010/2011', '20', '32', '28', '24'),
(97, 'Video Games', 3, 'PS3 - Game 2012', '30', '48', '42', '36'),
(98, 'Video Games', 3, 'PS3 - Game 2013', '45', '72', '63', '54'),
(99, 'Video Games', 3, 'Wii - Game 2006/2007', '5', '8', '7', '6'),
(100, 'Video Games', 3, 'Wii - Game 2008/2009', '7', '12', '10', '9'),
(101, 'Video Games', 3, 'Wii - Game 2010/2011', '15', '24', '21', '18'),
(102, 'Video Games', 3, 'Wii - Game 2012', '30', '48', '42', '36'),
(103, 'Video Games', 3, 'Xbox 360 - Game 2005/2006/2007', '7', '12', '10', '9'),
(104, 'Video Games', 3, 'Xbox 360 - Game 2008/2009', '10', '16', '14', '12'),
(105, 'Video Games', 3, 'Xbox 360 - Game 2010/2011', '20', '32', '28', '24'),
(106, 'Video Games', 3, 'Xbox 360 - Game 2012', '30', '48', '42', '36'),
(107, 'Video Games', 3, 'Xbox 360 - Game 2013', '45', '72', '63', '54'),
(108, 'Blender', 4, 'General - Countertop', '27', '44', '38', '33'),
(109, 'Carpet', 4, 'General - 3'' x 5''', '53', '85', '75', '64'),
(110, 'Carpet', 4, 'General - 4'' x 6''', '54', '87', '76', '65'),
(111, 'Carpet', 4, 'General - 5'' x 8''', '70', '112', '98', '84'),
(112, 'Carpet', 4, 'General - 8'' x 10''', '108', '173', '152', '130'),
(113, 'Clock', 4, 'General - Wall Clock', '9', '15', '13', '11'),
(114, 'Coffee Maker', 4, 'General - Countertop', '30', '48', '42', '36'),
(115, 'Coffee Maker', 4, 'Keurig - Regular', '62', '100', '87', '75'),
(116, 'Fan', 4, 'General - Floor Fan', '27', '44', '38', '33'),
(117, 'Iron', 4, 'General - Steam Iron', '30', '48', '42', '36'),
(118, 'Microwave', 4, 'General - Countertop', '55', '88', '77', '66'),
(119, 'Toaster', 4, 'General - 2-Slice', '26', '42', '37', '32'),
(120, 'Toaster', 4, 'General - 4-Slice', '39', '63', '55', '47'),
(121, 'Toaster Oven', 4, 'General - Countertop', '42', '68', '59', '51'),
(122, 'Vacuum', 4, 'Hoover - WindTunnel', '100', '160', '140', '120'),
(123, 'Artwork', 5, 'General - Canvas Painting', '97', '156', '136', '117'),
(124, 'Bike', 5, 'General - BMX', '164', '263', '230', '197'),
(125, 'Bike', 5, 'General - Mountain/Speed/Road', '97', '156', '136', '117'),
(126, 'Fill Out Unique Donation', 6, 'Fill Out Unique Donation', '0', '0', '0', '0'),
(127, 'Men''s Athletic', 7, 'General - Running', '29', '47', '41', '35'),
(128, 'Men''s Athletic', 7, 'General - Sports', '23', '37', '33', '28'),
(129, 'Men''s Athletic', 7, 'General - Walking', '22', '36', '31', '27'),
(130, 'Men''s Boots', 7, 'General - Fashion', '30', '48', '42', '36'),
(131, 'Men''s Boots', 7, 'General - Outdoor/Work', '29', '47', '41', '35'),
(132, 'Men''s Boots', 7, 'Timberland Brand', '43', '69', '61', '52'),
(133, 'Men''s Formal Wear', 7, 'General - Low-Heel', '22', '36', '31', '27'),
(134, 'Men''s Sandals', 7, 'General - Flip Flop', '19', '31', '27', '23'),
(135, 'Men''s Sandals', 7, 'General - Slide', '14', '23', '20', '17'),
(136, 'Women''s Athletic', 7, 'General - Running', '23', '37', '33', '28'),
(137, 'Women''s Athletic', 7, 'General - Sports', '24', '39', '34', '29'),
(138, 'Women''s Athletic', 7, 'General - Walking', '18', '29', '26', '22'),
(139, 'Women''s Boots', 7, 'General - Fashion', '17', '28', '24', '21'),
(140, 'Women''s Boots', 7, 'General - Outdoor/Work', '18', '29', '26', '22'),
(141, 'Women''s Boots', 7, 'General - Rubber Boot', '14', '23', '20', '17'),
(142, 'Women''s Boots', 7, 'UGG Brand', '57', '92', '80', '69'),
(143, 'Women''s Heels', 7, 'General - Platform', '15', '24', '21', '18'),
(144, 'Women''s Sandals', 7, 'General - Flat', '16', '26', '23', '20'),
(145, 'Women''s Sandals', 7, 'General - Wedge', '15', '24', '21', '18'),
(146, 'Baseball', 8, 'General - Ball x4', '8', '13', '12', '10'),
(147, 'Baseball', 8, 'General - Metal Bat', '23', '37', '33', '28'),
(148, 'Baseball', 8, 'General - Wooden Bat', '22', '36', '31', '27'),
(149, 'Basketball', 8, 'General - Ball', '15', '24', '21', '18'),
(150, 'Football', 8, 'General - Ball', '11', '18', '16', '14'),
(151, 'Soccer', 8, 'General - Ball', '10', '16', '14', '12'),
(152, 'Tennis', 8, 'General - Ball x10', '4', '7', '6', '5'),
(153, 'Tennis', 8, 'General - Racket', '18', '29', '26', '22'),
(154, 'Backpack', 9, 'General - Drawstring', '12', '20', '17', '15'),
(155, 'Backpack', 9, 'General - Regular', '17', '28', '24', '21'),
(156, 'Backpack', 9, 'Jansport - Big Student', '36', '58', '51', '44'),
(157, 'Backpack', 9, 'Jansport - Regular', '28', '45', '40', '34'),
(158, 'Backpack', 9, 'Under Armour - Drawstring', '21', '34', '30', '26'),
(159, 'Calculator', 9, 'Casio - Scientific', '14', '23', '20', '17'),
(160, 'Calculator', 9, 'General - Basic Desktop', '8', '13', '12', '10'),
(161, 'Calculator', 9, 'Texas Instruments - TI-83', '50', '80', '70', '60'),
(162, 'Calculator', 9, 'Texas Instruments - TI-84', '80', '128', '112', '96'),
(163, 'Fill Out Unique Donation', 10, 'Fill Out Unique Donation', '0', '0', '0', '0'),
(164, 'Fill Out Unique Donation', 11, 'Fill Out Unique Donation', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `location` text NOT NULL,
  `count` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location`, `count`) VALUES
(1, '24 Hour Shop', 0),
(2, 'Adele H. Stamp Student Union', 0),
(3, 'Annapolis Hall', 0),
(4, 'Cambridge Community Center', 0),
(5, 'Centreville Hall', 0),
(6, 'Clarice Smith Performing Arts Center', 0),
(7, 'Cumberland Hall', 0),
(8, 'Denton Hall', 0),
(9, 'Easton Hall', 0),
(10, 'Elkton Hall', 0),
(11, 'Ellicott Hall', 0),
(12, 'Eppley Recreation Center', 0),
(13, 'Hagerstown Hall', 0),
(14, 'Hornbake Library', 0),
(15, 'LaPlata Hall', 0),
(16, 'Leonardtown Community Center', 0),
(17, 'McKeldin Library', 0),
(18, 'Oakland Hall', 0),
(19, 'Priddy Library', 0),
(20, 'Queen Anne''s Hall', 0),
(21, 'Ritchie Coliseum', 0),
(22, 'South Campus Commons #1', 0),
(23, 'South Campus Commons #3', 0),
(24, 'South Campus Commons #6', 0),
(25, 'South Campus Dining Hall', 0),
(26, 'The Diner (North Campus)', 0),
(27, 'Van Munching Hall', 0),
(28, 'Somerset Hall', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orginfo`
--

CREATE TABLE IF NOT EXISTS `orginfo` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `paypal` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `totalpointsawarded` int(100) NOT NULL,
  `totalusersawarded` int(100) NOT NULL,
  `timesuploaded` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `orginfo`
--

INSERT INTO `orginfo` (`id`, `name`, `username`, `email`, `paypal`, `password`, `status`, `totalpointsawarded`, `totalusersawarded`, `timesuploaded`) VALUES
(1, 'Procitystaff', 'procitystaff', 'procitystaff@gmail.com', 'donationorg@donationorg.net', '2e5dad94e496a9440285d5f63ae9bcd0', 1, 790, 30, 13),
(2, 'Food Recovery Network- UMCP', 'frnumd', 'frn.umd@gmail.com', '', '83f665a7e3310def34574cb1209698a9', 1, 0, 0, 0),
(3, 'Circle K International - UMCP', 'circlekumcp', 'umdckipresident@gmail.com', '', 'ddcc1f2d328f919c11314b59e50fd9d7', 1, 0, 0, 0),
(4, 'Habitat for Humanity - UMCP', 'umdhabitat', 'president@umdhabitat.org', '', '5ad401d6151fd91e730dfffd0af16aa5', 1, 0, 0, 0),
(5, 'American Red Cross Club - UMCP', 'americanredcrossclubumd', 'arc.umd@gmail.com', '', 'd96a3288b9a5ca25394af61a30fd1724', 1, 0, 0, 0),
(6, 'Delta Epsilon Mu - UMCP', 'demumd', 'demgammacommunityservice@gmail.com', '', 'e7589d310c5d4305a4fd8cb1a94b7645', 1, 0, 0, 0),
(7, 'CIVICUS - UMCP', 'civicusumd', 'sbriggs@umd.edu', '', '2be17616f4d866d3f4945b8ed93547e0', 1, 0, 0, 0),
(8, 'Smith School Womens Society - UMCP', 'sswsumcp', 'smithwomen@gmail.com', '', '6f84279d86adca72b10bb33ea75a85f1', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orgupload`
--

CREATE TABLE IF NOT EXISTS `orgupload` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `orgid` int(100) NOT NULL,
  `eventname` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `csvpath` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `orgupload`
--

INSERT INTO `orgupload` (`id`, `orgid`, `eventname`, `description`, `time`, `csvpath`) VALUES
(3, 2, 'Basketball Game Recovery 1/15/14', 'Volunteers collected leftover food from concession stands at the game on January 15.  The food was then transported to a local homeless shelter.', '2014-01-21 12:10:13', '224Procity Basketball Game Recovery January 15, 2014.csv'),
(13, 1, 'testinsert', 'testinsert', '2014-01-26 16:34:41', '2014-01-2611-34-41uploadtest_jan25.csv'),
(14, 1, 'testinsert2', 'testinsert2', '2014-01-26 16:42:10', '2014-01-2611-42-10uploadtest_jan25.csv'),
(15, 1, 'testalready', 'testalready', '2014-01-26 16:51:08', '2014-01-2611-51-08uploadtest_jan25.csv'),
(16, 1, 'test insert again 7', 'test insert again 7', '2014-01-26 17:02:04', '2014-01-2612-02-04uploadtest_jan25.csv'),
(17, 0, 'Fall 2013 Semester Hours', 'Total Fall 2013 Hours', '2014-01-27 06:01:41', '2014-01-2701-01-41AmericanRedCrossMyProCity- Jan 2014.csv'),
(18, 5, 'American Red Cross Fall 2013 Hours', 'Fall 2013 Service Hours', '2014-01-27 06:03:28', '2014-01-2701-03-28AmericanRedCrossMyProCity- Jan 2014.csv'),
(19, 1, 'test insert with confirm email', 'test insert with confirm email', '2014-01-28 14:57:20', '2014-01-2809-57-20uploadtest_jan25.csv'),
(20, 1, 'test again with confirm email', 'test again with confirm email', '2014-01-28 14:58:42', '2014-01-2809-58-42uploadtest_jan25.csv'),
(21, 1, 'test status code 1with confirm email', 'test status code 1with confirm email', '2014-01-28 15:04:40', '2014-01-2810-04-40uploadtest_jan25.csv'),
(22, 1, 'test status code 1with confirm email again', 'test status code 1with confirm email again', '2014-01-28 15:08:12', '2014-01-2810-08-12uploadtest_jan25.csv'),
(23, 1, 'email check', 'email check', '2014-01-28 15:09:37', '2014-01-2810-09-37uploadtest_jan25.csv'),
(24, 1, 'no hash, status 0 devkavathekar', 'no hash, status 0 devkavathekar', '2014-01-28 15:13:27', '2014-01-2810-13-27uploadtest_jan25.csv'),
(25, 2, 'Basketball Game Recovery 1/15/14', 'Volunteers collected leftover food from concession stands at the game on January 15.  The food was then transported to a local homeless shelter.', '2014-01-28 15:15:55', '2014-01-2810-15-55Procity Basketball Game Recovery 1.15.14.csv'),
(26, 1, 'status 0 new pass devkavathekar', 'status 0 new pass devkavathekar', '2014-01-28 15:16:11', '2014-01-2810-16-11uploadtest_jan25.csv'),
(27, 1, 'status 0 new pass devkavathekar again', 'status 0 new pass devkavathekar again', '2014-01-28 15:19:06', '2014-01-2810-19-06uploadtest_jan25.csv'),
(28, 1, 'clean test', 'clean test', '2014-01-28 16:04:18', '2014-01-2811-04-18uploadtest_jan25.csv'),
(29, 1, 'clean test 2', 'clean test 2', '2014-01-28 16:05:51', '2014-01-2811-05-51uploadtest_jan25.csv'),
(30, 1, 'test new', 'test new', '2014-01-28 16:20:48', '2014-01-2811-20-48uploadtest_jan25.csv'),
(31, 1, 'test new 2', 'test new 2', '2014-01-28 16:21:44', '2014-01-2811-21-44uploadtest_jan25.csv'),
(32, 1, 'test new 3', 'test new 3', '2014-01-28 16:23:24', '2014-01-2811-23-24uploadtest_jan25.csv'),
(33, 1, 'test new 4', 'test new 4', '2014-01-28 16:25:12', '2014-01-2811-25-12uploadtest_jan25.csv'),
(34, 1, 'test new 5', 'test new 5', '2014-01-28 16:27:22', '2014-01-2811-27-22uploadtest_jan25.csv'),
(35, 1, 'NEW NEW', 'NEW NEW', '2014-01-28 16:28:58', '2014-01-2811-28-58uploadtest_jan25.csv'),
(36, 1, 'test new 8', 'test new 8', '2014-01-28 16:29:37', '2014-01-2811-29-37uploadtest_jan25.csv'),
(37, 1, 'test 10', 'test 10', '2014-01-28 16:31:10', '2014-01-2811-31-10uploadtest_jan25.csv'),
(38, 2, 'Basketball Game Recovery 1/15/14', 'Volunteers collected leftover food from concession stands at the game on January 15.  The food was then transported to a local homeless shelter.', '2014-01-29 02:28:48', '2014-01-2821-28-48Procity Basketball Game Recovery January 15, 2014.csv'),
(39, 2, 'Diner Recovery - January 27, 2014', 'Volunteers recovered leftover food from 251 North and delivered it to the Family Crisis Center.', '2014-01-29 03:46:56', '2014-01-2822-46-56Diner Recovery January 27, 2014.csv'),
(40, 1, 'hhh', 'hhh', '2014-02-06 13:00:53', '2014-02-0608-00-53csvfileexample.csv'),
(41, 1, 'uuu', 'uuu', '2014-02-06 15:31:42', '2014-02-0610-31-42csvfileexample.csv'),
(42, 1, 'asdf', 'asdf', '2014-02-07 20:39:25', '2014-02-0715-39-25csvfileexample.csv'),
(43, 1, 'kol', 'kol', '2014-02-07 20:42:09', '2014-02-0715-42-09csvfileexample.csv'),
(44, 1, 'asdf', 'asdf', '2014-02-07 20:43:32', '2014-02-0715-43-32csvfileexample.csv'),
(45, 1, 'nnmmm', 'nnmm', '2014-02-07 21:02:17', '2014-02-0716-02-17csvfileexample.csv'),
(46, 1, 'kjjj', 'kjjj', '2014-02-07 21:04:40', '2014-02-0716-04-40csvfileexample.csv'),
(47, 1, 'hhh', 'hhh', '2014-02-07 21:06:11', '2014-02-0716-06-11csvfileexample.csv'),
(48, 1, 'as', 'as', '2014-02-07 21:07:27', '2014-02-0716-07-27csvfileexample.csv'),
(49, 1, 'ass', 'ssd', '2014-02-07 21:10:33', '2014-02-0716-10-33csvfileexample.csv'),
(50, 1, 'assasdf', 'asdfsdf', '2014-02-07 21:11:09', '2014-02-0716-11-09csvfileexample.csv'),
(51, 1, 'asdfs', 'asdfs', '2014-02-07 21:12:41', '2014-02-0716-12-41csvfileexample.csv'),
(52, 1, 'csc', 'scsc', '2014-02-07 21:13:49', '2014-02-0716-13-49csvfileexample.csv');

-- --------------------------------------------------------

--
-- Table structure for table `propoints`
--

CREATE TABLE IF NOT EXISTS `propoints` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `orgid` int(10) NOT NULL,
  `points` int(100) NOT NULL,
  `totalamount` decimal(19,4) NOT NULL,
  `procityamount` decimal(19,4) NOT NULL,
  `orgamount` decimal(19,4) NOT NULL,
  `status` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `Username` varchar(1024) NOT NULL,
  `Password` varchar(1024) NOT NULL,
  `Email` varchar(1024) NOT NULL,
  `ProPoints` mediumint(6) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `loginid` varchar(500) NOT NULL,
  `status` int(15) NOT NULL DEFAULT '0',
  `Location` int(2) NOT NULL,
  `num_donated` int(5) NOT NULL,
  `num_claimed` int(5) NOT NULL,
  `num_rewards` int(100) NOT NULL,
  `propicpath` varchar(400) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=302 ;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`Id`, `FirstName`, `LastName`, `Username`, `Password`, `Email`, `ProPoints`, `hash`, `loginid`, `status`, `Location`, `num_donated`, `num_claimed`, `num_rewards`, `propicpath`) VALUES
(1, 'Chris', 'Lane', 'chrislane', '70d5efa1bc0349b0d586779babe33e46', 'dkavathe@umd.edu', 797, '', '', 1, 2, 4, 1, 0, '2014-01-2012-59-16chrisprofpic.jpg'),
(2, 'Christopher', 'Lane', 'ice1158', 'fb4fcba2acb19a8ab11bc2b8d996a5cc', 'clane124@umd.edu', 22, '', '', 1, 2, 1, 0, 0, '2014-01-0118-48-00Chris Lane Fun.jpg'),
(3, '', '', 'EJMolleur', '64ec0a50e2d969643230b286eeb89ee4', 'molleur1@terpmail.umd.edu', 40, '', '', 1, 0, 0, 0, 0, ''),
(4, '', '', 'Ceddy', 'cf6ebf3453bf1877ee3f1dce7bd1ec19', 'Ceddy1@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(5, '', '', 'jhil', 'a3f8304b7b9e994bda9d34399cd78659', 'jhilnbra@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(6, '', '', 'ethernetdan', 'ab2467695802fe0a8e17af75b177e26e', 'danielg@umd.edu', 26, '', '', 1, 0, 0, 0, 0, ''),
(7, '', '', 'tdaffuso', 'e213aac199193c4bd92c8ad54abe1bfb', 'tdaffuso@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(8, '', '', 'hyillah', '580d7255857c657bacae84eab899ab49', 'hyillah@umd.edu', 25, '', '50e1f46725d2143a651b82cd1ff32d6962660865', 1, 0, 0, 0, 0, ''),
(9, '', '', 'hackbyrd', 'ddda8e93855fcedd8587fded9df1a3f1', 'jxmoneyz@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(10, '', '', 'kshah12', 'baccae35c70140174c11331b11b69afc', 'kshah12@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(11, '', '', 'deldes44', 'c347b645238789534cb15ce4f18512fe', 'ddesouza@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(12, '', '', 'jarch', '5225a581dc9877951f77c7d129957ff8', 'jarch@umd.edu', 14, '', '', 1, 0, 0, 0, 0, ''),
(13, '', '', 'nribisi', 'fe1fee40257c8053a0c6793efdd4abb7', 'nribisi@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(14, '', '', 'Ac11793', 'ea06da1220909d668ebe799c3ab8b26b', 'Ac11793@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(15, 'Tiffany', 'Lu', 'TLu', '32c4ba97880714263b5c623ecfd6a2a6', 'tlu2014@terpmail.umd.edu', 25, '', '', 1, 0, 4, 0, 0, ''),
(16, '', '', 'esamak', 'e1b16b9632a683b3ff0fd60d759ae6ef', 'esamak@umd.edu', 25, '', '5ced8b65a83c29d00ad5b032bdce78aef6c844a2', 1, 22, 0, 0, 0, ''),
(17, 'Grace', 'H', 'ghsu', '164a57a373e9629c8aaafd0a15825f57', 'ghsu@umd.edu', 73, '', '', 1, 2, 0, 0, 0, ''),
(18, '', '', 'cher_rose', 'ddf1fede34305f571dbcefcb6227a257', 'clisar@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(19, '', '', 'nickf115', '2f7d315d2e64e47f546443dd12e3f4e4', 'nfleming@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(20, '', '', 'mlinden', 'd98b7c0c638e92dae995bd3c648131cb', 'mlinden@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(21, 'Atul', 'Kavathekar', 'akavathe', '70d5efa1bc0349b0d586779babe33e46', 'devkavathekar23@gmail.com', 1329, '', '', 1, 26, 2, 0, 0, '2014-01-1808-20-26atul.jpg'),
(22, 'Ayushna', 'Kavathekar', 'ayublu101', '70d5efa1bc0349b0d586779babe33e46', 'dkavathe@terpmail.umd.edu', 96, '', '', 1, 4, 6, 1, 0, '2014-01-2918-32-262014-01-1808-20-26atul.jpg'),
(141, '', '', 'dvsancho ', '833eebf244ec144cf076c43e13be8f05', 'dvsancho@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(23, '', '', 'c00p4', 'a03529320077a507d478e81c612555c9', 'kwayman@terpmail.umd.edu', 25, '', '', 1, 0, 1, 0, 0, ''),
(24, '', '', 'JasyC', 'a70f6b576a97264071088629916f0a9c', 'jcampbe2@umd.edu', 25, '', 'bbf88a9805bc3d6af5d11943bcd4a24109078e94', 1, 0, 0, 0, 0, ''),
(25, '', '', 'Eric', '5a22e6c339c96c9c0513a46e44c39683', 'erosenbe@umd.edu', 25, '4a9bd19b3b8676199592a346051f950c', '', 1, 0, 0, 0, 0, ''),
(26, '', '', 'mcarralero', '581252902f4fe63e3603d39115aa22ad', 'marcoc16@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(27, '', '', 'terps21', '29295d9ff561be02afc5a3a2a206aacc', 'emayo21@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(28, '', '', 'cpb93', 'c4b6b1392e1856c7e896d2fa5d5193ee', 'cpb93@terpmail.umd.edu', 25, '', '0397751764c8f4412f34508439f9b18ec6b69c17', 1, 20, 5, 0, 0, ''),
(29, '', '', 'siona100', 'f0a55c3ea44f64beb26aa36a1ff8bf4c', 'sslepoy@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(30, '', '', 'alamar', 'f4b8fe85bdb6095320793994fc8eef14', 'alamar@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(31, 'Ian', 'Moritz', 'IanMoritz', 'cacf82189451ea3a18eccf961de28bd6', 'imoritz@umd.edu', 25, '', '', 1, 0, 1, 0, 0, '2014-02-0712-24-35p81Eh87.jpg'),
(32, '', '', 'zack13532', 'd3290aa6609cf738faf417e859249277', 'zsiegel@umd.edu', 67, '', '', 1, 0, 0, 0, 0, ''),
(33, '', '', 'nayafrazier', '012d53890f7a1bdd9f27a1e3175a6502', 'nfrazier@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(34, '', '', 'mmoody93', '4be4afa15aedab9f3cd66bf7a8c5ee22', 'mmoody@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(38, '', '', 'Sam', '0a5e7211bcbee1c44cd0e153201eb087', 'sesteve@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(37, '', '', 'vpsinz', '7073339747c6222edd1f4008f5f37975', 'vespinoz@umd.edu', 60, '', '', 1, 0, 4, 0, 0, ''),
(39, '', '', 'fatima18', 'a6aeda06745ff9190d8cca8db549883f', 'fatima18@umd.edu', 25, '', '8dcff4d1e45842d6d7be9de2b83822dea5fe4023', 1, 0, 0, 0, 0, ''),
(40, '', '', 'rfleskes', '656bb54e63550469e68e2c6290850b11', 'rfleskes@umd.edu', 37, '', '', 1, 0, 0, 0, 0, ''),
(41, '', '', 'aleahpisarz', '363ccdc00ccbba837c6e13e23c8478fd', 'apisarz@terpmail.umd.edu', 25, '', 'c49eef97f214297e9095dedf842461a67e90f95f', 1, 3, 0, 0, 0, ''),
(42, '', '', 'Taneisha1994', 'd4f3f9fed7f38f1b721f749cbfd1cc42', 'tcarter6@terpmail.umd.edu', 25, '', '0d15ec950dc55c5d6c653934fcac6c5ce916301c', 1, 0, 0, 0, 0, ''),
(43, '', '', 'amarkoolk', 'a180032389f8adb9264d9dcb60b5ac25', 'akulkarn@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(44, '', '', 'rdelaney', 'dd5a84ca480c3c71e19760fa5bfae6a9', 'rdelaney@terpmail.umd.edu', 25, '', '8ab6b978221290eb239f1c3c3d1a57195810956c', 1, 0, 0, 0, 0, ''),
(45, '', '', 'ltrach1', '968c7c251efb7e32aedbcb4108414b6b', 'ltrach1@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(46, '', '', 'ltrach10', '968c7c251efb7e32aedbcb4108414b6b', 'ltrach1@terpmail.umd.edu', 25, '', '96c424fbca410571e9d2433fb96a8055498b44a8', 1, 0, 0, 0, 0, ''),
(47, '', '', 'Nathan Brandli', '2ff839d4ddb975109cd8c1711f4cf179', 'nbrandli@terpmail.umd.edu', 25, '', '', 1, 0, 1, 0, 0, ''),
(48, '', '', 'Vaughn', '27c042f992ed8c110b468852e633ece4', 'vmidder@umd.edu', 25, '', '15690225ac64570bc28df37d8a975029975a41ee', 1, 0, 0, 0, 0, ''),
(49, '', '', 'yaletv', 'd4d3cf32f7e4a4862ccc91e31a0f6d12', 'tvelines@umd.edu', 14, '', '', 1, 2, 0, 0, 0, ''),
(53, '', '', 'joshdeese93', 'c82287bbb6829eebf097e81b3f50c92f', 'jdeese@terpmail.umd.edu', 25, '', '', 1, 2, 0, 0, 0, ''),
(57, 'Dustin', 'Kang', 'dustinkang', '1429c25a116ee45d3bc1fcbf2a1e0e2b', 'dkang@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(61, '', '', 'josh20740', 'c82287bbb6829eebf097e81b3f50c92f', 'jdeese@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(65, '', '', 'sstebbs', '45b986c71901ec4f5dd971ff2c1695a4', 'stebbins@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(69, '', '', 'cegan', 'ebd17838853a6f01a37f061e205f77fd', 'cegan12@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(73, '', '', 'egan', '2b051e49734d71419fb82d92d805a266', 'cegan12@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(77, '', '', 'sstebbins', '45b986c71901ec4f5dd971ff2c1695a4', 'stebbins@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(111, '', '', 'nmalladi', 'd6aa7ee265372620d9886060ddeaa20e', 'nmalladi@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(113, 'Mary Rose', 'Winnard', 'MaryRose88', '0c9d4fbbbd4cc79dde3bacd26e4f370b', 'mwinnard@umd.edu', 25, '', '', 1, 6, 0, 0, 0, ''),
(114, 'Kate', 'Galbreath', 'katiegal', '6ec23b507eb694002c9413d813e6c613', 'katiegal@terpmail.umd.edu', 43, '039a9443ae7f3297f30981915d526360', '', 1, 0, 1, 0, 0, ''),
(115, '', '', 'rocky123', 'a93ba5938bea4370e0af1dd8932f7fdb', 'rshivach@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(116, '', '', 'lindseyganey', 'd79bead87ea52324cf3f0d8e21b7634e', 'lganey1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(117, '', '', 'echagnon', '4f04fafbe6e23f06af86354a9a147c8f', 'echagnon@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(118, '', '', 'EJGrim', 'd57295123500281c6e5a7fa48fb80bc9', 'egriming@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(120, 'Lin', 'Dan', 'babyizblue', 'a4f132ce76419b18314ea1415cbbd5a2', 'oaddeh@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(121, '', '', 'trflach', '053578746a4ee658fb5a2db23ab980e7', 'trflach@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(122, '', '', 'Reed92', 'bb4f90f0c4fc6e87c568891e545ede59', 'Alwhitti@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(123, '', '', 'Awreed', '61e998ae81be112af9a8550b92263398', 'alwhitti@umd.edu', 2, '', '', 1, 25, 0, 0, 0, ''),
(124, '', '', 'cree318', '098d8f45a5cf74f58dac70d1d89dfcee', 'ckaiser@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(125, '', '', 'rhefter', 'efedf6bc63601198d71ddbe9812d93d1', 'rhefter1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(126, '', '', 'skoolaee', '983923e9f4467f7be4d4b018629f32ba', 'skoolaee@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(127, '', '', 'Kdixon', 'ad9bca7dfd629696869b535b4fd2fc5f', 'Kdixon14@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(128, '', '', 'smusavi', '4b5e4b1dbd5a7abbbf11acaf90dc8ebc', 'smusavi@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(129, '', '', 'Kennybabyyy', '5a22e6c339c96c9c0513a46e44c39683', 'kbrowne1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(130, '', '', 'rkshirsagar', 'c73542e5f88cd19ab049c60aced67479', 'rkshirsa@terpmail.umd.edu', 1, '', '', 1, 0, 0, 0, 0, ''),
(131, '', '', 'dhowarth', 'eb8db4899bef39ccc622582cf7f35d6f', 'dhowarth@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(132, '', '', 'ivesingh', '90fd431bb5dcc53f9c5c65abe29594fd', 'ivesingh@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(135, '', '', 'mparks12', '8c398a0abd78c80fdac558bb7c9576a1', 'mparks12@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(136, '', '', 'lkeyes', '3016ff4d7e853fd6ac955af0308e9e20', 'lkeyes@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(137, '', '', 'skruger', 'c66ac81c9001a2488252a869447145ad', 'skruger@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(138, '', '', 'Fahleelah', '6818dfc9aa62f068b907376baeed0cc4', 'fahleelah@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(139, '', '', 'Falilat', '6818dfc9aa62f068b907376baeed0cc4', 'fshuaib@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(140, '', '', 'ashleyperalta', '067011f8d5e1ab2a2bdb578483ff22c3', 'aperalt1@terpmail.umd.edu', 25, '9271c0b57808899380a8aa6771ce320b', '', 1, 0, 0, 0, 0, ''),
(142, '', '', 'sinfante', '9ca18a3a6f02d93c9f37e0b8303b9bfa', 'sinfante@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(143, '', '', 'rchong', '74f08b8b9d4c55f230c1b55eace7ff29', 'rchong@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(144, '', '', 'lwittick', '2e7ef8280fa67f84d6856c67a872a08f', 'lwittick@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(145, '', '', 'alxlwn', 'e2c0ee3016c598097b2828dab4fc02c1', 'alewyn@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(156, '', '', 'russell2', '6c8c6fdd06b7466b6e3d96139d10013f', 'russell2@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(146, '', '', 'brookins', 'd04e53a8dca78582267bd9aa1f1c3148', 'brookins@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(147, '', '', 'valeriehoy', '4510a3e25d32852f6f9da63513415bd6', 'vhoy@umd.edu', 25, '', '', 1, 2, 0, 0, 0, ''),
(157, '', '', 'Cathy15', 'f215411b5ceb601d98281e79ea4eedc2', 'cathy15@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(153, '', '', 'vtanner', '55ed5e5e6d58227d0ef8df798262804c', 'vtanner@terpmail.umd.edu', 25, '', '', 1, 17, 0, 0, 0, ''),
(149, '', '', 'TeddyAmen', 'f046a9e621acdfced21869fe1181d762', 'eamenaba@terpmail.umd.edu', 40, '', '', 1, 24, 0, 0, 0, ''),
(150, '', '', 'Sarah Jessee', '1a00eb9b8a1aa7f174455fc4a57be579', 'sjessee@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(155, 'Dev', 'Kavathekar', 'test1', '54f74fbfb94518a527a36474dc904c25', 'dkavathe2@umd.edu', 100, '', '', 1, 0, 0, 0, 0, ''),
(158, '', '', 'bryanfarrell21', 'b5e46771f4699b2e0e7e3ffa4bdff415', 'bfarrel1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(159, '', '', 'triddlestorffer', '39ffaf7d90b1552844a7144129acb7b3', 'triddle@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(160, '', '', 'kgarciar', '52080ccaa8e1fb983e2dda964456e444', 'kgarciar@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(161, '', '', 'baileypendergast', '8f33de590498e49055551f567ca55da5', 'bpenderg@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(162, '', '', 'wallyloh', '4a13294d56e1c3a7033f1d2edfceea2b', 'rgreenst@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(163, '', '', 'mikekesting', '692a03ddfdc2c0a35135e39b1bb4b9bd', 'mkesting@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(164, 'Brigette', 'Fine', 'bfine93', '1d462fa1e52074d3c05bbb11311c1e98', 'bfine@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(165, '', '', 'mdoran93', '9b06e5fe74d3379d9a08626be45ace41', 'maggiedoran93@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(166, '', '', 'maggied93', '9b06e5fe74d3379d9a08626be45ace41', 'mdoran93@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(167, '', '', 'lfaf37', 'c5668fd5326fb69859e5ff1cc43a587a', 'lfox12@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(168, '', '', 'sjkrahling', '26c20c8c197355daf4e7252dd3d252cd', 'skrahlin@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(169, '', '', 'ahussla', '74df448d83fdb4f3ffdf3f49d18c1009', 'ahuss@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(170, '', '', 'ncohen52', 'b539fd4a58ec8167c9c19e6f87e3ca92', 'ncohen52@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(171, '', '', 'crocca17', 'f6c5f636d13cd492acd74d7f57694786', 'crocca17@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(172, '', '', 'kswanz', '80325401fd44f749c06193da6d60bac1', 'kswanson@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(173, '', '', 'dan_hauser', 'd925d7dfdc858f991c9ae187c1493fe7', 'dhauser1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(174, 'Tessa', 'Trach', 'ttrach', 'caec91012c9f9fee219a863694c0be09', 'ttrach@terpmail.umd.edu', 25, '', '', 1, 28, 0, 0, 0, '2013-12-2923-25-53IMG_1314.JPG'),
(175, '', '', 'aliharrington', '0ad5f436a8554e46eeed8595d3353768', 'aharrin3@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(176, '', '', 'mkjames', '6cd587784b2a9eac9b5c871430ed1707', 'mjames1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(177, '', '', '3109 four lyph', '06a4e0534e518d60bd45c1b9ba0240a4', 'ratner@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(178, '', '', '2birdsonawire', 'cdcfb5e6538eff7fb781edc25801d40e', 'jkeane1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(179, '', '', 'gdailey', '22e8ccaac759f6fc9897f18b2142aff7', 'gdailey@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(181, '', '', 'rlewis16', 'c3a980ea574d4ef230435fc9ac68a9c1', 'rlewis16@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(182, '', '', 'kpmcmahon', 'a0e3e6a8da8f8ee43d3a208ef379b504', 'kmcmaho2@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(183, '', '', 'kmcmaho2', 'a0e3e6a8da8f8ee43d3a208ef379b504', 'kmcmaho2@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(184, '', '', 'Jstor18', 'fe9f14bfcc6244bc4f55345f0a755580', 'jstory1@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(186, '', '', 'bass', '04de888a45cd50876c47d3b53b5a9eb7', 'jdiaz126@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(187, '', '', 'pearpearyy', 'd909e84d6e3cc104f5587062bfd34f22', 'pyukkas@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(188, '', '', 'AMHuisentruit', 'dbef7c4f2ad15f7c316217d2a91def78', 'amhuisen@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(190, '', '', 'MJS', 'd74a98aaecb65fe48597aed5bfdce569', 'mjsmith@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(191, '', '', 'MJSmith', 'd74a98aaecb65fe48597aed5bfdce569', 'mjsmith@terpmail.umd.edu', 40, '', '', 1, 2, 0, 0, 0, ''),
(192, '', '', 'brandilabarre', 'bceaddd64f779b4784fd5adba61bd8aa', 'blabarre@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(193, '', '', 'Sally', 'ded8729ef96e3b3aebb04b20d9694782', 'sdeleon@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(196, '', '', 'vchauhan', 'cbf2cfa788172fe0c68cf64161fb0264', 'vchauhan@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(197, '', '', 'Zhubin', '68ec37f8c4296a9253e5b054141b16ed', 'zja@umd.edu', 25, 'ee685d908ba947bfd7c5a1ff7e02fea9', '', 0, 0, 0, 0, 0, ''),
(198, '', '', 'lkirkire', '9f80a2eeed2729dc8093e10297b40c2f', 'lkirkire@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(199, 'Emily', 'Roark', 'eroark', '516900cb8869805bd1a19951eeba6bba', 'eroark@terpmail.umd.edu', 25, '', '', 1, 17, 0, 0, 0, ''),
(200, '', '', 'folan.3elan', 'c56d0e9a7ccec67b4ea131655038d604', 'folan.3elan@umd.edu', 25, 'c03c87ada0def061e909f2a2972ad52a', '', 0, 0, 0, 0, 0, ''),
(201, '', '', 'tester', 'a849371d9ec51368c54fc3743ed117ed', 'dfkfjkl@umd.edu', 25, '0aec8473145a198875eace2593e88cbb', '', 0, 0, 0, 0, 0, ''),
(202, '', '', 'abdots', 'eca4b84cdd5295f7c1d863fda33b1ea8', 'abdoz.com@umd.edu', 25, 'febad46a93d591c88b5b8b7f61810394', '', 0, 0, 0, 0, 0, ''),
(203, '', '', 'sammotamedi', 'f2d3c4d244cfa2bc6c50dc6af36e9e28', 'motamedi@umd.edu', 25, '0efc829ef5e5088c8bb28d6516e3defa', '', 0, 0, 0, 0, 0, ''),
(204, '', '', 'CJ', 'aacd6e8bfb5f9423b1635035023bc738', 'cmaduka@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(205, '', '', 'Young Kim', 'e7b7e1b173d4b19c63022b38a42bde9d', 'youngkim@umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(207, '', '', 'blairwarrick', '2598d0010f0124baa5fc7fcf4b0cb533', 'bwarrick@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(208, '', '', 'sarmaypak', 'fe3ebfdf824ba3a7087591ce51918e45', 'smpakzad@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(209, 'Dev', 'Kavathekar', 'devsteelers', '70d5efa1bc0349b0d586779babe33e46', 'devsteelers@gmail.com', 89, '', '', 1, 0, 0, 0, 0, '015CAM00022.jpg'),
(210, '', '', 'mwinnard0', '58b2b0c22130be80fa4c3ac450ea564f', 'mwinnard@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(254, 'Procity Staff', 'Procity Staff', 'procitystaff', '27ced9e3f81cc51fc7c1eac12f00d0f0', 'procitystaff@gmail.com', 31, '', '', 1, 0, 0, 0, 0, ''),
(255, '', '', 'avines1255', '9f9178988877b262d4a06289d06e3680', 'avines1@terpmail.umd.edu', 49, '1e43931f38922ea83740fca8502734f8', '', 0, 0, 0, 0, 0, ''),
(256, '', '', 'ameehan256', 'c5c0d1ae06b6929d33fd7a5ffa3d96b2', 'ameehan@umd.edu', 61, '269b4fcb5dad9cee827b7ed9ba412332', '', 0, 0, 0, 0, 0, ''),
(257, '', '', '1013acs257', 'e24b36b2762695e3eec542298b4040ff', '1013acs@gmail.com', 49, 'c752229d2824901627306a19cc752fd7', '', 0, 0, 0, 0, 0, ''),
(258, '', '', 'aray1235258', 'b77a41adb27bff1c936c30288c31b56d', 'aray1235@terpmail.umd.edu', 85, 'dfb9727f0a262f22a2dbd77d172d3b8a', '', 0, 0, 0, 0, 0, ''),
(259, '', '', 'Akbaker22259', '3b1105e6f67fb3ab3143025815d25873', 'Akbaker22@yahoo.com', 45, '4b1a1fbea1b104079e7aaa3b1fd487ed', '', 0, 0, 0, 0, 0, ''),
(260, '', '', 'azehra260', 'd314508f4d237edd437652fd5d323a43', 'azehra@terpmail.umd.edu', 97, '79dd5b019a2493726e3159c3d1a0631a', '', 0, 0, 0, 0, 0, ''),
(261, '', '', 'bcalhoun261', '0ee45a128fb217f5a9baf42b7072190d', 'bcalhoun@terpmail.umd.edu', 61, '27dcef2833f24d16ec6f357f28b78f7f', '', 0, 0, 0, 0, 0, ''),
(262, '', '', 'csolow262', '9951bcfae26a3d6ec2ac2583ffb03a3e', 'csolow@terpmail.umd.edu', 49, 'bdd2288003d1c9be47e34c02a3eb4fca', '', 0, 0, 0, 0, 0, ''),
(263, '', '', 'jaspermok1127263', 'ef73591d8e5786e7c424e5ab0cab323a', 'jaspermok1127@gmail.com', 145, '2391363e99ded2c80ad96394548d8ee6', '', 0, 0, 0, 0, 0, ''),
(264, '', '', 'las819264', '369a587f56aeeb954c95d08c81770322', 'las819@terpmail.umd.edu', 49, '8ed4a1868636186cd12386879eb00910', '', 0, 0, 0, 0, 0, ''),
(265, '', '', 'ldavis14265', '7bc09c367b610caee1ad3b6ceb250e48', 'ldavis14@terpmail.umd.edu', 49, '140ea4f1dcd8ab37fc1750f95c5e047f', '', 0, 0, 0, 0, 0, ''),
(266, 'Lily ', 'Hong', 'lilypopyh266', '07df3840ad014b2b7b5931d4cab67922', 'lilypopyh@yahoo.com', 181, '', '', 1, 0, 0, 0, 0, ''),
(267, 'Madeeha', 'Lughmani', 'mlughman267', 'd09ed9666b8b315ca9e6e9b3ec9e8908', 'mlughman@terpmail.umd.edu', 97, '', '', 1, 0, 0, 0, 0, ''),
(268, '', '', 'mehwish268', '71316704d0f1c6d6326b724be557ad1c', 'mehwish@terpmail.umd.edu', 73, '45c9e6277230946fb9526c88162539aa', '', 0, 0, 0, 0, 0, ''),
(269, '', '', 'sab.coaxum269', '9c4f5b644de74613bf2314da89ca1a95', 'sab.coaxum@gmail.com', 121, '0b82249c82bee3376a1cf3279162084a', '', 0, 0, 0, 0, 0, ''),
(270, '', '', 'sulinwu2011270', '234b7af3ed9d9f70953e48ef598e9c31', 'sulinwu2011@gmail.com', 49, '36ecc60a09a42c80dff3c6db201b3641', '', 0, 0, 0, 0, 0, ''),
(271, '', '', 'tovinguyen1271', '2459272ec6c16074dad0bf88c675a75b', 'tovinguyen1@icloud.com', 85, '46e49936b31042ba5e5ae6ee0782bbd4', '', 0, 0, 0, 0, 0, ''),
(284, '', '', 'hlee1213284', '45e57fa22e2d2621ea1842f01614587c', 'hlee1213@umd.edu', 25, '5d2e8f7ad113067bf204fa46de205b62', '', 0, 0, 0, 0, 0, ''),
(285, 'Harrison', 'Lee', 'hlee1213285', '45e57fa22e2d2621ea1842f01614587c', 'hlee1213@terpmail.umd.edu', 25, '', '', 1, 0, 0, 0, 0, ''),
(286, '', '', 'alexkrefetz286', '64c5d044d3917b705613bd056d652eda', 'alexkrefetz@gmail.com', 37, '62ac36bc5aae8c8750d15da6a8c4eaf7', '', 0, 0, 0, 0, 0, ''),
(287, '', '', 'mmassumi287', '224f14eae146cbb607ebd52e2799789f', 'mmassumi@terpmail.umd.edu', 37, '9cb2661789b61a806ff1f8a273776f8e', '', 0, 0, 0, 0, 0, ''),
(288, '', '', 'ltrach1288', '6dead986eeed86ad29ebab574df3ff1f', 'ltrach1@comcast.net', 37, 'b14cc546b830ad5f59b7336123eb7592', '', 0, 0, 0, 0, 0, ''),
(289, '', '', 'strach289', '19b86f11c34a0a214432e9ea4705f245', 'strach@terpmail.umd.edu', 37, '1b5d385fec60016d2b7a0e814e22dc38', '', 0, 0, 0, 0, 0, ''),
(290, '', '', 'ckettl290', 'bb97fdf1108a15650659899f675ea028', 'ckettl@terpmail.umd.edu', 37, '62ac36bc5aae8c8750d15da6a8c4eaf7', '', 0, 0, 0, 0, 0, ''),
(291, 'Vera', 'Pervitsky', 'verap291', 'd2e6a911fc52b6217701c4b94bef4aba', 'verap@terpmail.umd.edu', 37, '', '', 1, 0, 0, 0, 0, ''),
(222, '', '', 'rschaefe0', '1b43b2af55302d300d824be661f6d6f4', 'rschaefe@terpmail.umd.edu', 41, '2391363e99ded2c80ad96394548d8ee6', '', 0, 0, 0, 0, 0, ''),
(223, '', '', 'Mnaj0', '75a18a1b79d65440bc78cf67c0986d65', 'Mnaj@terpmail.umd.edu', 41, '6d45fe6da0a16009411b6ce9c7b11e8a', '', 0, 0, 0, 0, 0, ''),
(224, '', '', 'lkestner223', '9e5475fe94486229542744c2498644d4', 'lkestner@terpmail.umd.edu', 43, '0a2a593538b148e074490a672d539cf2', '', 0, 0, 0, 0, 0, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
