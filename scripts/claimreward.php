<?php 
	
	include "config.php";
	session_start();
   
   // redirect to sign in page if not signed in
   if(!isset($_SESSION['username'])){
		
		header("location:../signin.php");
		return;	
		
   }
	
	if(isset($_POST['id']) && isset($_POST['businessid'])) {
			
		$username=$_SESSION['username'];
		$user_id=$_SESSION['userid'];		
		$id=mysql_real_escape_string($_POST['id']);
		$businessid=mysql_real_escape_string($_POST['businessid']);
		$sql_donate=mysql_query("select * from business_item where id='$id' AND showstatus='1'") or die(mysql_error());
		$fetch_reward=mysql_fetch_array($sql_donate);
		$fetch_reward_num = mysql_num_rows($sql_donate);
		
		if($fetch_reward_num <= 0) {
		
			//header("location:../profile.php");
			return;
		
		}
		
		$checkdatabase=mysql_query("select * from dealrecords where userid='$user_id' and rewardid='$id'") or die(mysql_error());
		$fetch_numdeal = mysql_num_rows($checkdatabase);
		
		if($fetch_numdeal > 0) {
 
			header('Content-type: application/json');
			$response_array['status'] = 'already';
			echo json_encode($response_array);
			//header("location:../itemreward.php?id=".$id."&msg=error2");
			return;
			
		} else {

	 
			$sql_user=mysql_query("select * from userinfo where Id='$user_id'") or die(mysql_error());
			$fetch_user=mysql_fetch_array($sql_user);
			$numrewards = $fetch_user['num_rewards'];
			$claim_username = $fetch_user['Username'];
			
			$claim_email = $fetch_user['Email'];
			$maxclaimed = 10;
			
			if($numrewards >= $maxclaimed) {
	 
				header('Content-type: application/json');
				$response_array['status'] = 'limit';
				echo json_encode($response_array);
				//header("location:../itemreward.php?id=".$id."&msg=error1");	
				return;
				
			} else {
			

				 $propts = $fetch_user['ProPoints'];
				 $ebase = $fetch_reward['propointworth'];				
					
				 if($propts < $ebase) {
				 
					 header('Content-type: application/json');
					$response_array['status'] = 'points';
					echo json_encode($response_array);
					//header("location:../itemreward.php?id=".$id."&msg=error3");
					return;
					
				 } 

				$count = $fetch_reward['count'];
				
				if($count == '0') {
				
					header('Content-type: application/json');
					$response_array['status'] = 'zero';
					echo json_encode($response_array);
					//header("location:../itemreward.php?id=".$id."&msg=error4");
					return;
				}
				
				
				if($count > 0) {
				
					$count = $count - 1;
					
					if($count < 0) {
					
						header('Content-type: application/json');
						$response_array['status'] = 'zero';
						echo json_encode($response_array);
						//header("location:../itemreward.php?id=".$id."&msg=error4");
						return;
						
					}
					
					mysql_query("update business_item set count='$count' where id='$id'") or die(mysql_error()); 
				
				}
				
				$numrewards = $numrewards + 1;
				 
				$update_maxclaimed = "update userinfo set num_rewards='$numrewards' where Id='$user_id'";
				mysql_query($update_maxclaimed) or die(mysql_error());
								
				$sql_donator = mysql_query("select * from userinfo where Id='$donated_by'") or die(mysql_error());
				$fetch_donator = mysql_fetch_array($sql_donator);
				$donator_email = $fetch_donator['Email'];
				$donate_username = $fetch_donator['Username'];
				 
				$pts = $propts - $ebase;

				$update_user = mysql_query("update userinfo set ProPoints='$pts' where Id='$user_id'") or die(mysql_error()); 
				
				$update_dealrec = mysql_query("INSERT INTO dealrecords (businessid,userid,rewardid) VALUES ('$businessid','$user_id','$id')") or die(mysql_error());
				 
				$claim_msg = "Hey Procitizen $claim_username!\n\nCongratulations! You've claimed a reward! Go to your profile page under Business Rewards and click Show Reward when at the cashier to use your coupon. We hope you are satisfied! \n\nRegards,\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
				$subject = "Claimed an reward";
				$headers = "From: procitystaff@gmail.com \r\n";
				
				mail($claim_email,$subject,$claim_msg,$headers);	 
				
				header('Content-type: application/json');
				$response_array['status'] = 'success';
				echo json_encode($response_array);					
				//header("location:../profile.php"); 
			 
			 }
		 
		 }
		
	} else {
	
		echo "Invalid request logging";
		$ipaddress = "Claim: ".$_SERVER['REMOTE_ADDR']."\r\n";
		$file = '../admin/maliciouslogger.txt';
		$fp = fopen($file, 'a');
		fwrite($fp, $ipaddress);
		fclose($fp);
		exit(0);
	
	
	}
	
?>