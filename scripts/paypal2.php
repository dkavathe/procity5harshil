<?php
/**
 * Created by PhpStorm.
 * User: Micheal
 * Date: 2/11/14
 * Time: 12:02 AM
 */

session_start();

//include 'config.php';

$_SESSION['points'] = $_POST['points'];
$_SESSION['amount'] = $_POST['amount'];
$server_ip =$_SERVER['SERVER_ADDR'];

if($_GET['action'] == 'ipn')
{
    $ipn_post_data = $_POST;

    // Choose url
    if(array_key_exists('test_ipn', $ipn_post_data) && 1 === (int) $ipn_post_data['test_ipn'])
    {
        $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    }

   /* else
        $url = 'https://www.paypal.com/cgi-bin/webscr'; */

// Set up request to PayPal
    $request = curl_init();
    curl_setopt_array($request, array
    (
        CURLOPT_URL => $url,
        CURLOPT_POST => TRUE,
        CURLOPT_POSTFIELDS => http_build_query(array('cmd' => '_notify-validate') + $ipn_post_data),
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HEADER => FALSE,
        CURLOPT_SSL_VERIFYPEER => TRUE,
    ));

// Execute request and get response and status code
    $response = curl_exec($request);
    $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);

// Close connection
    curl_close($request);
//
    if( $status == 200 && $response == 'VERIFIED')
    {
        // All good! Proceed...
        if(array_key_exists('charset', $ipn_post_data) && ($charset = $ipn_post_data['charset']))
        {
            // Ignore if same as our default
            if($charset == 'utf-8')
                return;

            // Otherwise convert all the values
            foreach($ipn_post_data as $key => &$value)
            {
                $value = mb_convert_encoding($value, 'utf-8', $charset);
            }

            // And store the charset values for future reference
            $ipn_data['charset'] = 'utf-8';
            $ipn_data['charset_original'] = $charset;

            //check for status
            if($ipn_data['payment_status'] == "Completed")
            {
                $uid = $_SESSION['user_id'];
                $proPointsId = $_SESSION['itemid'];
                $amount = $_SESSION['amount'];

                $points = floatval($points);
                $amount = floatval($amount);

                if($points <= 0 || $points > 1000){

                    return;

                }

                if($amount <= 0 || $amount > 300) {

                    return;

                }


                $number = $points * .15;
                $number = number_format($number, 2, '.', '');
                $amount = number_format($amount, 2, '.', '');

                if($number != $amount) {

                    return;

                }

                $sql_pro=mysql_query("update propoints set status='y' where id='$proPointsId'") or die(mysql_error());
                $getpro=mysql_query("select * from propoints where id='$proPointsId' and status='y'") or die(mysql_error());
                $fetchpro=mysql_fetch_array($getpro);
                $points=$fetchpro['points'];

                $sql_user=mysql_query("select * from userinfo where Id='$uid'") or die(mysql_error());
                $fetch=mysql_fetch_array($sql_user);
                $propoints=$fetch['ProPoints'];

                $pro=$propoints + $points;
                $update=mysql_query("update userinfo set ProPoints='$pro' where Id='$uid'") or die(mysql_error());
            }
        }



    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['points']) && isset($_POST['amount']))
{
    if(!isset($_SESSION['username'])){

        header("Location: signin.php");
        return;

    }
    else
    {
        $uid = $_SESSION['userid'];

        $pts = $_SESSION['points'];
        $amount = $_SESSION['amount'];

        $pts = floatval($pts);
        $amount = floatval($amount);

        if($pts <= 0 || $pts > 1000){

            header('Location:../buypro.php');
            return;

        }

        if($amount <= 0 || $amount > 300) {

            header('Location:../buypro.php');
            return;

        }

        $number = $pts * .15;
        $number = number_format($number, 2, '.', '');

        if($number != $amount) {

            header('Location:../buypro.php');
            return;

        }

        $sql= "INSERT INTO propoints (user_id, points, amount, status) VALUES ('$uid', '$pts', '$amount', 'n')";

        try{
            $pdo = new PDO('mysql:host=localhost;dbname=procity;charset=utf8', 'procity', 'Reciprocity310');
        }
        catch(PDOException $pdoE){
            die('Could not connect to Database: ' . $pdoE->getMessage());
        }

//prepare and execute SQL
        $query = $pdo->prepare($sql);
        $query->execute();

//get last inset id
        $_SESSION['itemid'] = $pdo->lastInsertId();
    }

    //pay with paypal

    $api_user = "testapiuser_api1.apiuser.net";
    $api_pass = "1391907867";
    $api_sig = "AFcWxV21C7fd0v3bYYYRCpSSRl31A8UDnd7UYJHPV0Xixwlr29tAslrH"; //"A-ATjwILvTD6k.gYXQxjhoN6FIHhAAYRQZpxWiRawZ2KrGquUUAOMo3o";
    $app_id = "APP-80W284485P519543T";
    $apiUrl = 'https://svcs.sandbox.paypal.com/AdaptivePayments/';
    $paypalUrl = "https://www.paypal.com/webscr?cmd=_ap-payment&paykey=";
    $call = "Pay";

    $orgName = $_POST['organization'];
    //connect to mysql
    //include 'config.php';

    //get org paypal info
    $sql = "SELECT name, email, paypal FROM orginfo WHERE id ='$orgName'";
    $result = mysql_query($sql);

    $orgInfo = mysql_fetch_assoc($result);
    $donationAmount = $_POST['amount'];
    $pts = $_POST['points'];
    $percentageAmount = (5 / 100) * $donationAmount;

    //create headers
    $headers = array(
        "X-PAYPAL-SECURITY-USERID: ".$api_user,
        "X-PAYPAL-SECURITY-PASSWORD: ".$api_pass,
        "X-PAYPAL-SECURITY-SIGNATURE: ".$api_sig,
        "X-PAYPAL-REQUEST-DATA-FORMAT: JSON",
        "X-PAYPAL-RESPONSE-DATA-FORMAT: JSON",
        "X-PAYPAL-APPLICATION-ID: ".$app_id,
        ""
    );

    // create the pay request
    $createPacket = array(
        "actionType" =>"PAY",
        "currencyCode" => "USD",
        "receiverList" => array(
            "receiver" => array(
                array(
                    "amount"=> $donationAmount, //organizations Take
                    "email"=> $orgInfo['paypal']
                ),
                array(
                    "amount"=> $percentageAmount, //Procity's Take
                    "email"=> "msquared86@gmail.com"
                ),
            ),
        ),
        "returnUrl" => "https://www.myprocity.com/profile.php",
        "cancelUrl" => "https://www.myprocity.com/buypro.php",
        "requestEnvelope" => array(
            "errorLanguage" => "en_US",
            "detailLevel" => "ReturnAll",
        ),
    );



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl.$call);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($createPacket));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    json_decode($response, TRUE);

    var_dump($response);
    die();
    //var_dump(json_decode($response));
    $responseArray = json_decode($response, TRUE);
    if($responseArray['responseEnvelope']['ack'])
    {
        $paykey = $responseArray['payKey'];
        header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='.$paykey);
    }
}


