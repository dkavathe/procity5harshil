<?php

// Define debug function
function print_debug($message) {
  if($_GET['debug_mode'] == 1) {
    echo "$message<br>";
  }
}

// Connect to database
require_once("config.php");

// Get all items that have been unclaimed for two or more weeks
$items_query = "select * from donate_item where claim_status = 1 AND claimed_time + interval 14 day <= now()";
$items_result = mysql_query($items_query);
print_debug("Items query: $items_query");

if($items_result) {
  print_debug("Number of items: " . mysql_num_rows($items_result));
  print_debug("<hr>");

  // Iterate through unclaimed items and reset them
  while($item = mysql_fetch_assoc($items_result)) {
    print_debug("Item ID: $item[id]");
    print_debug("Item description: $item[item_id]");
    print_debug("Item donator: $item[donated_by]");
    print_debug("Item claimer: $item[claimed_by]");
    
    // Reset claim status
    $status_query = "update donate_item set claim_status = 0 where id = $item[id]";
    $status_result = mysql_query($status_query);
    print_debug("Status query: $status_query");

    // Reset claimer ID
    $claimer_query = "update donate_item set claimed_by = '' where id = $item[id]";
    $claimer_result = mysql_query($claimer_query);
    print_debug("Claimer query: $claimer_query");

    // Reset claimed timestamp
    $timestamp_query = "update donate_item set claimed_time = 0 where id = $item[id]";
    $timestamp_result = mysql_query($timestamp_query);
    print_debug("Timestamp query: $timestamp_query");

    // Reset claimer's ProPoints
    $propoints_query = "update userinfo set propoints = propoints + $item[ebase] where id = $item[claimed_by]";
    $propoints_result = mysql_query($propoints_query);
    print_debug("ProPoints query: $propoints_query");

    // Reset claimer's number of claimed items
    $claimed_query = "update userinfo set num_claimed = num_claimed - 1 where id = $item[claimed_by]";
    $claimed_result = mysql_query($claimed_query);
    print_debug("Claimed query: $claimed_query");

    // Delete all comments related to claimed item
    $comments_query = "delete from comments where item_id = $item[id]";
    $comments_result = mysql_query($comments_query);
    print_debug("Comments query: $comments_query");

    // Send an email to the claimer
    $claimer_query = "select email from userinfo where id = $item[claimed_by]";
    $claimer_result = mysql_query($claimer_query);
    $claimer_array = mysql_fetch_assoc($claimer_result);
    print_debug("Claimer query: $claimer_query");

    $email = $claimer_array['email'];
    $subject= "Your claim has expired!";
    $message = "Your claim for \"$item[item_name]\" has passed the 2 week deadline, so we have given you back your ProPoints as well as the ability to claim again. Please ensure next time you contact the donator earlier and set up a time to meet.\n\nThanks for using Procity!\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
    $headers = "From: procitystaff@gmail.com \r\n";
    mail($email, $subject, $message, $headers);
    print_debug("Sending email to $email (claimer)"); print_debug("Email text: $message");

    // Send an email to the donator
    $donator_query = "select email from userinfo where id = $item[donated_by]";
    $donator_result = mysql_query($donator_query);
    $donator_array = mysql_fetch_assoc($donator_result);
    print_debug("Donator query: $donator_query");

    $email = $donator_array['email'];
    $subject= "Your item's claim has expired!";
    $message = "The claim on your item \"$item[item_name]\" has expired after the 2 week deadline and is now available on The City page for anyone else to claim. If you wish to cancel this item permanently, please click \"Cancel Donation\" on your item's section.\n\nThanks for using Procity!\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
    $headers = "From: procitystaff@gmail.com \r\n";
    mail($email, $subject, $message, $headers);
    print_debug("Sending email to $email (donator)"); print_debug("Email text: $message");

    print_debug("<hr>");
  }
} else {
  print_debug("No expired items found in database!");
}
