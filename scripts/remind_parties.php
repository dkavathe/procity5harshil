<?php

// Number of days to wait between reminders
define("REMINDER_INTERVAL", 3);

function echo_debug_message($message) {
    if(isset($_GET['debug_mode']) && $_GET['debug_mode'] == 1) {
        echo "$message<br />";
    }
}

// Connect to database
require_once('config.php');

// Get list of pending claims
$pending_claims_query = "SELECT * FROM donate_item WHERE claim_status = 1";
$pending_claims_result = mysql_query($pending_claims_query);
$num_of_pending_claims = mysql_num_rows($pending_claims_result);

echo_debug_message("Pending claims: $num_of_pending_claims");

// Iterate over pending claims
if($num_of_pending_claims > 0) {
    while($pending_claim = mysql_fetch_row($pending_claims_result, MYSQL_ASSOC)) {
        echo_debug_message("Processing claim #$pending_claim[id]");
        
        // Calculate age of claim
        $current_time = time();
        $claim_time = strtotime($pending_claim['claimed_time']);
        
        echo_debug_message("Now: $current_time, Claim time: $claim_time");
        
        $age_of_claim_secs = $current_time - $claim_time;
        $age_of_claim_days = floor($age_of_claim_secs / (60 * 60 * 24));
        
        echo_debug_message("Age of claim (secs): $age_of_claim_secs secs");
        echo_debug_message("Age of claim (days): $age_of_claim_days days");
        
        if($age_of_claim_days >= REMINDER_INTERVAL && $age_of_claim_days % REMINDER_INTERVAL == 0) {
            echo_debug_message("Reminding claim");
            
            // Calculate fuzzy date
            $claim_expiry_date = strtotime('+14 days', strtotime($pending_claim['claimed_time']));
            $start = date("Y-m-d H:i:s", time());
            $end = date("Y-m-d H:i:s", $claim_expiry_date);

            require_once('../lib/carbon/carbon_script.php');
            $script = new CarbonScript();
            $fuzzy_date = $script->fuzzy_date_diff($start, $end);
            
            // Get claimer information
            $claimer_user_id = $pending_claim["claimed_by"];
            $claimer_info_query = "SELECT * FROM userinfo WHERE id = $claimer_user_id";
            $claimer_info_result = mysql_query($claimer_info_query);
            $claimer_info = mysql_fetch_assoc($claimer_info_result);
            
            echo_debug_message("Claimer username/email: $claimer_info[Username]/$claimer_info[Email]");
            
            // Get donator information
            $donator_user_id = $pending_claim["donated_by"];
            $donator_info_query = "SELECT * FROM userinfo WHERE id = $donator_user_id";
            $donator_info_result = mysql_query($donator_info_query);
            $donator_info = mysql_fetch_assoc($donator_info_result);
            
            echo_debug_message("Donator username/email: $donator_info[Username]/$donator_info[Email]");
            
            // Send reminder email to claimer
            $subject = "Your exchange is still pending!";
            $message = "Dear $claimer_info[Username],\n\nPlease perform the exchange with your fellow Procitizen for the item \"$pending_claim[item_id]\" so everyone can get their items and earn ProPoints, you have $fuzzy_date to complete the exchange.\n\nThanks for using Procity!\n\nProcity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
            $headers = "From: procitystaff@gmail.com \r\n";
            mail($claimer_info["Email"], $subject, $message, $headers);
            
            echo_debug_message("Email text: $message");
            
            // Send reminder email to donator
            $subject = "Your exchange is still pending!";
            $message = "Dear $donator_info[Username],\n\nPlease perform the exchange with your fellow Procitizen for the item \"$pending_claim[item_id]\" so everyone can get their items and earn ProPoints, you have $fuzzy_date to complete the exchange.\n\nThanks for using Procity!\n\nProcity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
            $headers = "From: procitystaff@gmail.com \r\n";
            mail($donator_info["Email"], $subject, $message, $headers);
            
            echo_debug_message("Email text: $message");
        } else {
            echo_debug_message("Not reminding claim");
        }
    }
} else {
    echo_debug_message("No pending claims - done!");
}