<?php

// Start session
session_start();

// Check if user is logged in
if(!isset($_SESSION['userinfo'])) {
	// Redirect to login page
	header('Location: ../signin.php');
} else {
	$id = $_SESSION['userinfo']['id'];
}

// Update user
if(isset($_POST['update_name_submit'])) {
	// Capture form data
	
	include "config.php";
	
	$fname = mysql_real_escape_string($_POST['fname']);
	$lname = mysql_real_escape_string($_POST['lname']);

	// Build query
	$query = 'update userinfo set FirstName = "' . $fname . '", LastName = "' . $lname . '" where Id = ' . $id;

	// Connect to database
	
	// Submit query
	$result = mysql_query($query);

	// Update session
	$_SESSION['userinfo']['firstname'] = $fname;
	$_SESSION['userinfo']['lastname'] = $lname;
	
	// Redirect user
	header('Location: ../profile.php');
}