<?php

// Start session
session_start();
// Connect to database
require_once('config.php');

// Check if user is logged in
if(!isset($_SESSION['userinfo'])) {
	// Redirect to login page
	header('Location: ../signin.php');
} else {
	$id = $_SESSION['userinfo']['id'];
}

// Update user
if(isset($_POST['change_location_submit'])) {
	// Capture form data
	$location = mysql_real_escape_string($_POST['location']);

	// Build queries
	$query = 'update userinfo set location = "' . $location . '" where id = ' . $id;
	$query2 = 'select location from location where id = ' . $location;

	
	// Submit query
	$result = mysql_query($query);
	$result2 = mysql_query($query2);

	// Update session
	$_SESSION['userinfo']['location'] = $location;

	if($result2) {
		$row = mysql_fetch_row($result2);
		$_SESSION['userinfo']['location_name'] = $row[0];
	}

	// Redirect user
	header('Location: ../profile.php?success=location');
}