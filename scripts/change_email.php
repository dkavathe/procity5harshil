<?php

// Start session
session_start();

// Connect to database
require_once('config.php');

// Check if user is logged in
if(!isset($_SESSION['userinfo'])) {
	// Redirect to login page
	header('Location: ../signin.php');
} else {
	$id = $_SESSION['userinfo']['id'];
}

// Update user
if(isset($_POST['change_email_submit'])) {
	// Capture form data
	$email = mysql_real_escape_string($_POST['email']);
	$conf = mysql_real_escape_string($_POST['confirm_email']);

	if($email == '' || $conf == '') {

		header('Location: ../profile.php?error=1');
		return;
		
	}
	
	if($email != $conf) {

		header('Location: ../profile.php?error=3');
		return;
		
	}
	
	$sql_email = mysql_query("select * from userinfo where Email='$email'") or die(mysql_error());
	$rows = mysql_num_rows($sql_email);
	
	if($rows > 0) {
	
		header('Location: ../profile.php?error=5');
		return;
		
	}
	
	$at = strpos($email,"@");
	
	if($at <= 0) {
	
		header('Location: ../profile.php?error=4');
		return;
	}
		
	// Build query
	$query = 'update userinfo set email = "' . $email . '" where id = ' . $id;

	// Submit query
	$result = mysql_query($query);

	// Update session
	$_SESSION['userinfo']['email'] = $email;

	// Redirect user
	header('Location: ../profile.php?success=email');


}