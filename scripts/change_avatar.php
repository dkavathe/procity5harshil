<?php

// Start session
session_start();
// Connect to database
require_once('config.php');
include "../lib/resizeimage.php";

// Check if user is logged in
if(!isset($_SESSION['userinfo'])) {
	// Redirect to login page
	header('Location: ../signin.php');
} else {
	$id = $_SESSION['userinfo']['id'];
}

// Update user
if(isset($_POST['change_avatar_submit'])) {
	// Capture form data
	
	$img = $_SESSION['userinfo']['propicpath'];
	
	if($img != '') {
	
		chmod("../drive/avatars", 0755);
		chmod("../drive/avatars/$img", 0755);
		unlink("../drive/avatars/$img");
	}
	
	$pic = $_FILES['image']['name'];
	
	$types = array('image/jpeg', 'image/gif', 'image/png');  

	if(in_array($_FILES['image']['type'], $types)) {

		
		$file_size = $_FILES['image']['size'];

		if ($file_size <= 3145728){ 

			$lastid = date("Y-m-dH-i-s");
			$rename= $lastid.$pic;
			$uploadDir = '../drive/avatars/'; 
			$filename = mysql_real_escape_string($_FILES['image']['name']);
			  
			$uploadfile = $uploadDir.$rename;          
			move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
			
			$resize = new ResizeImage($uploadfile);
			$resize->resizeTo(300,300,'exact');
			$resize->saveImage($uploadfile);
			
			$userid = $_SESSION['userid'];
						
			$update= "update userinfo set propicpath='$rename' where id='$userid'";
			mysql_query($update) or die(mysql_error());
		
		}
		
	}

	// Update session
	$_SESSION['userinfo']['propicpath'] = $rename;

	// Redirect user
	header('Location: ../profile.php?success=avatar');
}