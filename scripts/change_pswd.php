<?php

// Start session
session_start();
// Connect to database
require_once('config.php');


// Check if user is logged in
if(!isset($_SESSION['userinfo'])) {
	// Redirect to login page
	header('Location: ../signin.php');
} else {
	$id = $_SESSION['userinfo']['id'];
}

// Update user
if(isset($_POST['change_password_submit'])) {
	// Capture form data
	$password = mysql_real_escape_string($_POST['password']);
	$conf = mysql_real_escape_string($_POST['confirm_password']);
	
	if($password == '' || $conf == '') {
	
		header('Location: ../profile.php?error=1');
		return;
	}
	
	if($password != $conf) {
	
		header('Location: ../profile.php?error=2');
		return;
	}
	
	$password = md5(md5(md5($password)));

	// Build query
	$query = 'update userinfo set password = "' . $password . '" where id = ' . $id;

	
	// Submit query
	$result = mysql_query($query);

	// Redirect user
	header('Location: ../profile.php?success=password');
}