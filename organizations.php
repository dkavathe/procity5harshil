<?php 

session_start();
ob_start();
include "scripts/config.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/external-pages.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "topNav.php"; ?><!-- faq -->
    <div id="faq" class="faq_page">
        <div class="container">
            <!-- header -->
            <h2 class="section_header">
                <hr class="left visible-desktop">
                <span>Volunteer to earn ProPoints!</span>
                <hr class="right visible-desktop">
            </h2>

            <!-- list -->
            <div class="row">
                <div class="span12">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How do I get ProPoints for volunteering?
                        </div>
                        <div class="answer">
                            Ask your organization to partner up with Procity to allow ProPoints to be awarded for service projects and community service hours!
							Once integrated, organizations will have the ability and power to award ProPoints based on criteria.
							(freedom for organizations to decide how points are given)

                        </div>
                    </div>
                    
                </div>
				<!--<script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 

					

				</script> 
				
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> //--> 
				
            </div>
			<iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=arc.umd%40gmail.com&amp;color=%23AB8B00&amp;src=frn.umd%40gmail.com&amp;color=%23865A5A&amp;src=terpmail.umd.edu_0hd1lkfohmfhce5rtof6bee1gg%40group.calendar.google.com&amp;color=%232952A3&amp;ctz=America%2FNew_York" style=" border-width:0 " width="1000" height="600" frameborder="0" scrolling="no"></iframe>
        </div>
				
    </div>

    <!-- starts footer -->
    <?php include "footer.php";?>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
	
</body>
</html>
