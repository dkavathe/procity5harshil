<?php

// Start session
session_start();

// Connect to database
require_once('scripts/config.php');

$error = ''; 
$code = 'asdf';
if(isset($_GET['msg'])) {

	if($_GET['msg'] == 'error') {
		
		$error = "Failed - your code may be wrong";
	} else if($_GET['msg'] == 'pass') {
	
		$error = "Passwords don't match";
	} else if($_GET['msg'] == 'empty') {
	
		$error = "Empty fields";
	}
	
	$code = "valid";
	
	
} else if(isset($_GET['code'])) {

	$code = mysql_real_escape_string($_GET['code']);

}

if($code == '') {

	$error = "Only use this if you have received an email";
}

?>
<!DOCTYPE html>

<html>
<head>
    <title>Procity - Rewarding Those Who Do-Good</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"><!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="img/icon.ico" rel="shortcut icon">
    <link href="css/signin.css" rel="stylesheet" type="text/css">
    <link href="css/theme.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type=
    'text/css'>
</head>

<body>
    <?php include"topNav.php"; ?>

    <div id="box_login">
        <div class="container">
            <div class="span12 box_wrapper">
                <div class="span12 box">
                    <div>
                        <div class="head">
                            <h4>Reset your password</h4>
                        </div>

                        <div class="form">
                            <div style="color:#F00;">
                                <?php echo $error;?>
                            </div>

                            <form action='scripts/changepassword.php' method='post'>
								<input id="email" name="Email" placeholder="Email" required="" type="text">
								<input id="password" name="Password" placeholder="New Password" required="" type="password"> 
								<input id="confirm" name="Confirm" placeholder="Confirm New Password" required="" type="password"> 
								<input id="code" name="Code" value="<?php echo $code; ?>" type="hidden">
								
                                <div class="registrationFormAlert" id="incorrectEntry"></div>

                                <p>&nbsp;</p><button class="btn" id="signinbut" style="width:28%;" type="submit">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

  <!--          <section id="" style="margin-top:40px; margin-left:255px;">
                <script type="text/javascript">
<!-- 

                google_ad_client = "ca-pub-5663904797620784"; 

                /* advert1 */ 

                google_ad_slot = "2888958757"; 

                google_ad_width = 728; 

                google_ad_height = 90; 

                //

                </script> <script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script>
            </section>-->
        </div>
    </div><script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>  <script src="js/bootstrap.min.js" type="text/javascript"></script> <script src="js/theme.js" type="text/javascript"></script> <?php include "footer.php";?>
</body>
</html>