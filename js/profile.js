$(document).ready(function() {
    // Handle update name form
    $('#update-name .btn-primary').click(function() {
        $('#update-name form').submit();
    });

    // Handle change avatar form
    $('#change-avatar .btn-primary').click(function() {
        $('#change-avatar form').submit();
    });

    // Handle change username form
    $('#change-username .btn-primary').click(function() {
        $('#change-username form').submit();
    });

    // Handle change password form
    $('#change-password .btn-primary').click(function() {
        $('#change-password form').submit();
    });

    // Handle change email form
    $('#change-email .btn-primary').click(function() {
        $('#change-email form').submit();
    });

    // Handle change location form
    $('#change-location .btn-primary').click(function() {
        $('#change-location form').submit();
    });
});