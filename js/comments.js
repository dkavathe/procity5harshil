$(document).ready(function() {
	$('.toggle-comments').click(function() {
		if($(this).text() === "Show comments") {
			$(this).text("Hide comments");
		} else if($(this).text() === "Hide comments") {
			$(this).text("Show comments");
		}
	});

	$('.comment-form button').click(function(event) {
		// Hide no comments text
		$('.no-comments').hide();

		// Get parent form and div
		$form = $(this).parent('form');
		$div = $form.parent('.span12').children('.comments');
        
        console.debug($form);
        console.debug($div);

		// Get comment text
		$text = $form.children('.comment-text').val();

		// Get item ID and user ID
		$item_id = $form.children('.comment-item-id').val();
		$user_id = $form.children('.comment-user-id').val();

		// Call comment script
		$.ajax({
			url: 'scripts/save_comment.php',
			data:  { text: $text, item_id: $item_id, user_id: $user_id },
			success: function() {
				$html = '';
				$html += '<div class="row comment">';	
				$html +=     '<div class="span1">';
				$html +=         $('.avatar').html();
				$html +=     '</div>';
				$html +=     '<div class="span4">';
				$html +=         '<p>';
				$html +=             '<strong>' + $('.username').text() + '</strong><br />';
				$html +=             $text + '<br />';
				$html +=             '<small class="muted">Posted at ' + moment().format('YYYY-MM-DD HH:mm:ss') + '</small><br />';
				$html +=         '</p>';
				$html +=     '</div>';
				$html += '</div>';
                
                console.debug($html);

				$div.append($html);
			},
			error: function() {
				console.debug('An error occurred');
			}
		});

		// Prevent page from redirecting
		event.preventDefault();
	});
});