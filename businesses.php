<?php 

session_start();
ob_start();
include "scripts/config.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/external-pages.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
    <?php include "topNav.php"; ?><!-- faq -->
    <div id="faq" class="faq_page">
        <div class="container">
            <!-- header -->
            <h2 class="section_header">
                <hr class="left visible-desktop">
                <span>Businesses, support Procity!</span>
                <hr class="right visible-desktop">
            </h2>

            <!-- list -->
            <div class="row">
                <div class="span12">
					
					<div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            I'm an online business and want to post e-codes/discounts on Procity!
                        </div>
                        <div class="answer">
                            Yes, awesome! Send us an email at procitystaff@gmail.com so we can register you with us, and then we can work together to get out the e-codes/discounts to our users based on a number of ProPoints they have to spend!
							
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">2</div>
                        <div class="question">
                            I'm a local business and want to post rewards/coupons on Procity!
                        </div>
                        <div class="answer">
                            Send us an email at procitystaff@gmail.com to begin the integration process. We accept rewards from corporate organizations to local businesses!
							We have an efficient system, headache free that boost your sales, helps increase corporate social responsibility, and gives you a competitive edge!
							
                        </div>
                    </div>
					<div class="faq">
                        <div class="number">3</div>
                        <div class="question">
                            I'm not tech savy but I still want to post and help my business!
                        </div>
                        <div class="answer">
                            No worries, we can upload offers of your business on your behalf. We have tech savy experts standing by to take care of any issues.
							Send us an email at procitystaff@gmail.com to start the integration process.
							
                        </div>
                    </div>
					
                    
                </div>
				<!--<script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 

					

				</script> 
				
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> //--> 
				
            </div>
        </div>
    </div>

   

    <!-- starts footer -->
    <?php include "footer.php";?>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
	
</body>
</html>
