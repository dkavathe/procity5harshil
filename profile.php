<?php
	// In the wrong hands, even a time-saving tool such as version control can become a bother.
	date_default_timezone_set('America/New_York');
	
	// Start session
	session_start();

	// Check if user is logged in
	if(!isset($_SESSION['userinfo'])) {
		// Redirect to login page
		header('Location: signin.php');
	}

	// Connect to database
	require_once('scripts/config.php');

	// Load locations
	$query = 'select id, location from location';
	$result = mysql_query($query);
	$locations = array();

	if($result) {
		while($row = mysql_fetch_assoc($result)) {
			array_push($locations, $row);
		}
	}
	
	$query = "select * from userinfo where Id=" . $_SESSION['userinfo']['id'];
	$result = mysql_query($query);
	$userinfo = mysql_fetch_assoc($result);
	
	
	// Load business rewards
    //$query = "select business_item.* from business_item join businessinfo join dealrecords where dealrecords.businessid = businessinfo.id and dealrecords.rewardid = business_item.id and dealrecords.userid = " . $_SESSION['userinfo']['id'];
	$userid = $_SESSION['userinfo']['id'];
	$getbusiness=mysql_query("select * from dealrecords where (userid='$userid') limit 0,10 ");
	$numbusinessrewards = mysql_num_rows($getbusiness);
	/*$business_rewards = array();
     
    if($result) {
        while($row = mysql_fetch_assoc($result)) {
			array_push($business_rewards, $row);
		}	
    }*/
	
	// Load claimed items
	$query = "select * from donate_item where claimed_by = '" . $_SESSION['userinfo']['id'] . "'";
    $result = mysql_query($query);
    $claimed_items = array();

    if($result) {
    	while($row = mysql_fetch_assoc($result)) {
			array_push($claimed_items, $row);
		}	
    }

	// Load donated items
	$query = "select * from donate_item where donated_by = '" . $_SESSION['userinfo']['id'] . "'";
    $result = mysql_query($query);
    $donated_items = array();

    if($result) {
    	while($row = mysql_fetch_assoc($result)) {
			array_push($donated_items, $row);
		}	
    }
?>

<!doctype html>

<html lang="en">
	<head>
		<!-- Page metadata -->
		<meta charset="UTF-8" />
		<title>Procity - Rewarding Those Who Do-Good</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/icon.ico" />

		<!-- Stylesheets -->
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="lib/bootstrap/css/responsive.min.css" />
		<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/theme.css" />
		<link rel="stylesheet" href="css/profile.css" />

		<!-- Scripts -->
		<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
		<script src="lib/jquery/jquery.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap-tooltip.js"></script>
		<script src="lib/bootstrap/js/bootstrap-popover.js"></script>
		<script src="lib/moment/moment.min.js"></script>
		<script src="js/profile.js"></script>
	    <script src="js/comments.js"></script>
	    <script src="js/share.js"></script>

		<?php if(empty($_SESSION['userinfo']['firstname']) || empty($_SESSION['userinfo']['lastname'])): ?>
			<script>
				$(document).ready(function() {
					$('#update-name').modal();
				});
			</script>
		<?php endif; ?>
		
		<?php if(isset($_GET['error']) && $_GET["error"] == '1'): ?>
			<script>
				$(document).ready(function() {
					$('#show-error1').modal();
				});
			</script>
		<?php endif; ?>
		
		<?php if(isset($_GET['error']) && $_GET['error'] == "2"): ?>
			<script>
				$(document).ready(function() {
					$('#show-error2').modal();
				});
			</script>
		<?php endif; ?>
		
		
		<?php if(isset($_GET['error']) && $_GET['error'] == "3"): ?>
			<script>
				$(document).ready(function() {
					$('#show-error3').modal();
				});
			</script>
		<?php endif; ?>
		
				<?php if(isset($_GET['error']) && $_GET['error'] == "4"): ?>
			<script>
				$(document).ready(function() {
					$('#show-error4').modal();
				});
			</script>
		<?php endif; ?>
		
		<?php if(isset($_GET['error']) && $_GET['error'] == "5"): ?>
			<script>
				$(document).ready(function() {
					$('#show-error5').modal();
				});
			</script>
		<?php endif; ?>
		
		<?php if(isset($_GET['error']) && $_GET['error'] == "6"): ?>
			<script>
				$(document).ready(function() {
					$('#show-error6').modal();
				});
			</script>
		<?php endif; ?>
		<?php if(isset($_GET['success'])): ?>
			<script>
				$(document).ready(function() {
					$('#show-success').modal();
				});
			</script>
		<?php endif; ?>
		
		<?php if(isset($_GET['show']) && $_GET['show'] == "donated"): ?>
			<script>
				$(document).ready(function() {
					$('#show-donated').modal();
				});
			</script>
		<?php endif; ?>
			
	</head>
	<body>
		<div class="container">
		
			<div id="show-error1" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Entries empty</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			
			<div id="show-error2" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Passwords don't match</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>

			<div id="show-error3" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Entries don't match</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			
			<div id="show-error4" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Invalid data</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
		
			<div id="show-error5" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Email taken</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			<div id="show-error6" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Error</h3>
				</div>
				<div class="modal-body">
					<p>Username taken</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			<div id="show-success" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Success</h3>
				</div>
				<div class="modal-body">
					<p>Successfully changed <?php echo $_GET['success']; ?>!</p>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			
			<div id="show-donated" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Watch this to understand the donation-claim process!</h3>
				</div>
				<div class="modal-body">
					
					<center>
						<iframe width="500" height="315" src="//www.youtube.com/embed/CyTCv_cxPzc" frameborder="0" allowfullscreen></iframe>
					</center>
					
				</div>
				
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Close</button>
				</div>
				
			</div>
			
			<!-- Update name modal -->
			
			<div id="update-name" class="modal hide fade" tabindex="-1" role="dialog">
				<div class="modal-header">
					<h3>Missing Info</h3>
				</div>
				<div class="modal-body">
					<p>Please fill in the missing information to continue to your profile. Update your username and password if this is your first time logging in. Both are the same now.</p>
					<form action="scripts/update_name.php" method="post">
    					<input type="text" name="fname" id="fname" placeholder="First name" />
    					<input type="text" name="lname" id="lname" placeholder="Last name" />

    					<input type="hidden" name="update_name_submit" />
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary">Save changes</button>
				</div>
			</div>

			<!-- Navbar -->
			<?php include"topNav.php"; ?>

			<!-- Content -->
			<div class="row">
				<div class="span3 avatar">
					<img <?php if ($_SESSION['userinfo']['propicpath'] == '') { ?> src="img/default.png" <?php } else { ?> src="drive/avatars/<?php echo $_SESSION['userinfo']['propicpath']; ?>" <?php } ?> alt="Your avatar" />
				</div>
				<div class="span9 stats">
					<h2>Welcome to Procity, <?php echo $_SESSION['userinfo']['firstname']; ?>!</h2>
					<div class="row">
						<div class="span3">
							<div class="stat">
								<h3>Pro Points</h3>
								<p>
									<span class="text">
										<?php echo $userinfo['ProPoints']; ?>
									</span>
								</p>
							</div>
						</div>
						<div class="span3">
							<div class="stat">
								<h3>Rank</h3>
								<p>
									<span class="text">
										<?php
											$propoints = $userinfo['ProPoints'];

											if($propoints >= 0 && $propoints <= 49){
					                        	echo "Citizen";
					                        } else if($propoints > 49 && $propoints <= 149){
					                            echo "Helper";
					                        } else if($propoints > 149 && $propoints <= 264) {
					                            echo "Humanitarian";
					                        } else if($propoints > 264 && $propoints <= 449) {
					                            echo "Leader";
					                        } else if($propoints > 449 && $propoints < 1000) {
					                            echo "Mayor";
					                        } else if($propoints > 1000) {
					                            echo "Superhero";
					                        }
										?>
									</span>
								</p>
							</div>
						</div>
						<div class="span3">
							<div class="stat">
								<h3>Location</h3>
								<p>
									<span class="text">
										<?php echo $_SESSION['userinfo']['location_name']; ?>
									</span>
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="span3">
							<div class="stat">
								<h3>Business Rewards</h3>
								<p>
									<span class="text">
										<?php echo $numbusinessrewards; ?>
									</span>
									<a href="#business" class="pull-right">
										<i class="icon-chevron-right icon-large"></i>
									</a>
								</p>
							</div>
						</div>
						<div class="span3">
							<div class="stat">
								<h3>Claimed Items</h3>
								<p>
									<span class="text">
										<?php echo $userinfo['num_claimed']; ?>
									</span> 
									<a href="#claimed" class="pull-right">
										<i class="icon-chevron-right icon-large"></i>
									</a>
								</p>
							</div>
						</div>
						<div class="span3">
							<div class="stat">
								<h3>Donated Items</h3>
								<p>
									<span class="text">
										<?php echo $userinfo['num_donated']; ?>
									</span>
									<a href="#donated" class="pull-right">
										<i class="icon-chevron-right icon-large"></i>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span3 account">
					<div class="well">
						<h3>Manage Account</h3>

						<!-- Modal triggers -->
						<ul class="nav nav-pills nav-stacked">
							<li>
								<a href="#change-avatar" role="button" data-toggle="modal">
									<i class="icon-camera"></i> Change Avatar
								</a>
							</li>
							<li>
								<a href="#change-username" role="button" data-toggle="modal">
									<i class="icon-user"></i> Change Biography
								</a>
							</li>
							<li>
								<a href="#change-password" role="button" data-toggle="modal">
									<i class="icon-lock"></i> Change Password
								</a>
							</li>
							<li>
								<a href="#change-email" role="button" data-toggle="modal">
									<i class="icon-envelope"></i> Change Email
								</a>
							</li>
							<li>
								<a href="#change-location" role="button" data-toggle="modal">
									<i class="icon-globe"></i> Change Location
								</a>
							</li>
						</ul>

						<!-- Change avatar modal -->
						<div id="change-avatar" class="modal hide fade" tabindex="-1" role="dialog">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>Change Avatar</h3>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="span2">
										<h4>Current Avatar</h4>
										<img <?php if ($_SESSION['userinfo']['propicpath'] == '') { ?> src="img/default.png" <?php } else { ?> src="drive/avatars/<?php echo $_SESSION['userinfo']['propicpath']; ?>" <?php } ?> alt="Your avatar" />
									</div>
									<div class="span2">
										<h4>New Avatar</h4>
										<form action="scripts/change_avatar.php" method="post" enctype= "multipart/form-data">
											<div style="position:relative;">
												<a class='btn btn-primary' >
													Choose...
													
												</a>
												<input type="file" name="image" id="image"/>
												<span class='label label-default' id="upload-file-info"></span>
											</div>
											<input type="hidden" name="change_avatar_submit" />
										</form>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">Close</button>
								
								<input type="submit" name="savechanges" class="btn btn-primary" value="Save changes" />
							</div>
						</div>

						<!-- Change username modal -->
						<div id="change-username" class="modal hide fade" tabindex="-1" role="dialog">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>Change Biography</h3>
							</div>
							<div class="modal-body">
								<form action="scripts/change_user.php" method="post">
									<p>
										<b>Current username</b><br />
										<span class="username"><?php echo $_SESSION['userinfo']['username']; ?></span>
									</p>
									<p>
										<b>Current first name</b><br />
										<?php echo $_SESSION['userinfo']['firstname']; ?>
									</p>
									<p>
										<b>Current last name</b><br />
										<?php echo $_SESSION['userinfo']['lastname']; ?>
									</p>

									<label for="username">New username</label>
    								<input type="text" name="username" placeholder="Username" />
									
									<label for="firstname">New firstname</label>
    								<input type="text" name="firstname" placeholder="First Name" />
									
									<label for="lastname">New lastname</label>
    								<input type="text" name="lastname" placeholder="Last Name" />
									
    								<input type="hidden" name="change_username_submit" />
								</form>
							</div>
							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">Close</button>
								<button class="btn btn-primary">Save changes</button>
							</div>
						</div>

						<!-- Change password modal -->
						<div id="change-password" class="modal hide fade" tabindex="-1" role="dialog">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>Change Password</h3>
							</div>
							<div class="modal-body">
								<form action="scripts/change_pswd.php" method="post">
									<label for="password">New password</label>
    								<input required=true type="password" name="password" placeholder="Password" />

    								<label for="confirm_password">Confirm new password</label>
    								<input required=true type="password" name="confirm_password" placeholder="Password" />

    								<input type="hidden" name="change_password_submit" />
								</form>
							</div>
							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">Close</button>
								<button class="btn btn-primary">Save changes</button>
							</div>
						</div>

						<!-- Change email modal -->
						<div id="change-email" class="modal hide fade" tabindex="-1" role="dialog">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>Change Email</h3>
							</div>
							<div class="modal-body">
								<form action="scripts/change_email.php" method="post">
									<p>
										<b>Current email</b><br />
										<?php echo $_SESSION['userinfo']['email']; ?>
									</p>

									<label for="email">New email</label>
    								<input type="email" name="email" placeholder="you@example.com" />
    								
    								<label for="confirm_email">Confirm email</label>
    								<input type="email" name="confirm_email" placeholder="you@example.com" />

    								<input type="hidden" name="change_email_submit" />
								</form>
							</div>
							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">Close</button>
								<button class="btn btn-primary">Save changes</button>
							</div>
						</div>

						<!-- Change location modal -->
						<div id="change-location" class="modal hide fade" tabindex="-1" role="dialog">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>Change Location</h3>
							</div>
							<div class="modal-body">
								<form action="scripts/change_location.php" method="post">
									<p>
										<b>Current location</b><br />
										<?php echo $_SESSION['userinfo']['location_name']; ?>
									</p>

									<label for="email">New location</label>
    								<select name="location" id="location">
    									<option value="">Select a location...</option>
    									<?php foreach($locations as $location): ?>

											<option value="<?php echo $location['id']; ?>"><?php echo $location['location']; ?></option>

			
    									<?php endforeach; ?>
									</select>

									<input type="hidden" name="change_location_submit" />
								</form>
							</div>
							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">Close</button>
								<button class="btn btn-primary">Save changes</button>
							</div>
						</div>
					</div>
				</div>
				<div class="span9 thumbs">
					<div class="business well">
					 
						<h3 id="business" data-toggle="tooltip" data-placement="top" title="Click each box to see more">Business Rewards</h3>
						<?php 
						
						$userid = $_SESSION['userinfo']['id'];
						$getbusiness=mysql_query("select * from dealrecords where (userid='$userid') limit 0,10 ") or die(mysql_error());

						$numrows=mysql_num_rows($getbusiness);
		
						if($numrows != 0) { 
							 
							 while($fetch_dealrec=mysql_fetch_array($getbusiness)) {
								$rewardid = $fetch_dealrec['rewardid'];
								
								
								$getreward=mysql_query("select * from business_item where (id='$rewardid') limit 0,10 ") or die(mysql_error());
								$reward=mysql_fetch_array($getreward);
								
								$bid = $reward['businessid'];
								$type = $reward['type'];
								$promocode = $reward['promocode'];
								$websitelink = $reward['websitelink'];
								
								$getreward2=mysql_query("select * from businessinfo where (id='$bid') limit 0,10 ") or die(mysql_error());
								$businessdata=mysql_fetch_array($getreward2);

							?>
								
							<form method="post" onsubmit="return disp_prompt_reward(<?php echo $type; ?>);" action="showreward.php" name="change_reward" id="change_reward">
								<input type="hidden" id="busid" name="busid" value="<?php echo $fetch_dealrec['rewardid']; ?>">
								<input type="hidden" name="item_id" id="item_id" value="<?php echo $fetch_dealrec['id'];?>">
								<input type="hidden" name="type" id="type" value="<?php echo $type;?>">
							
							<!-- Modal trigger -->
								<a href="<?php echo '#business-reward-' . $reward['id'] . '-modal'; ?>" data-toggle="modal" class="thumb">
									<img src="<?php echo 'business/drive/rewardimages/' . $reward['picpath']; ?>" alt="<?php echo $reward['name']; ?>" class="img-polaroid" />
								</a>
								
								
								<!-- Reward modal -->
								<div id="<?php echo 'business-reward-' . $reward['id'] . '-modal'; ?>" class="modal hide fade" tabindex="-1" role="dialog">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Reward Details</h3>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="span1">
												<img src="<?php echo 'business/drive/rewardimages/' . $reward['picpath']; ?>" alt="<?php echo $reward['name']; ?>" />
											</div>
											<div class="span3">
												<h4><?php echo $reward['name']; ?></h4>
												<p><?php echo $reward['description']; ?></p>
												<table class="table table-striped">
													<tr>
														<th>Company</th>
														<td colspan="3">
														    <?php echo $businessdata['name'];?>
														</td>
													</tr>
													<tr>
														<th>You spent</th>
														<td colspan="3">
															<?php echo $reward['propointworth']; ?> ProPoints
														</td>
													</tr>
													<?php 
													if ($type == '0') { ?>
													<tr>
														<th>Status</th>
														<td colspan="3">UNUSED, press button to show reward to cashier</td>
													</tr>
													
													<?php
													} else { ?>
													
													<tr>
														<th>Code</th>
														<td colspan="3">
															<?php echo $promocode; ?>
														</td>
													</tr>
													
													<tr>
														<th>Website</th>
														<td colspan="3">
															<a href="http://<?php echo $websitelink; ?>" target="_blank"><?php echo $websitelink; ?></a>
															
														</td>
													</tr>
													
													<?php } ?>
												</table>
											</div>
										</div>
									</div>
									
									<div class="modal-footer">
											<?php if ($type == '0'): ?>
												<input type="submit" value="Show cashier coupon" class="btn btn-danger" />
											<?php else: ?>
												<input type="submit" value="Remove promo code" class="btn btn-danger" />
											<?php endif; ?>
										<button class="btn" data-dismiss="modal">Close</button>
									</div>
								</div>
							
							</form>
							<?php } ?>
						<?php } else { ?>
							<h4 class="muted">You have no business rewards!</h4>
						<?php } ?>
					</div>

					<div class="claimed well">
						<h3 id="claimed" data-toggle="tooltip" data-placement="top" title="Click each box to see more">Claimed Items</h3>
						<?php if(sizeof($claimed_items) != 0): ?>
							<?php foreach($claimed_items as $item): ?>
								<!-- Modal trigger -->
								<a href="<?php echo '#claimed-item-' . $item['id'] . '-modal'; ?>" data-toggle="modal" class="thumb">
									<img src="<?php echo 'drive/donations/' . $item['image']; ?>" alt="<?php echo $item['item_name']; ?>" />
									<?php
										switch($item['claim_status']) {
											default:
												echo '<span class="badge">Unreceived</span>';
												break;
											case 2:
												echo '<span class="badge badge-success">Received</span>';
												break;
										}
									?>
								</a>

								<!-- Item modal -->
								<div id="<?php echo 'claimed-item-' . $item['id'] . '-modal'; ?>" class="modal hide fade" tabindex="-1" role="dialog">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Item Details</h3>
										<?php 
											if($item['claimed_time'] != "0000-00-00 00:00:00") {
												$expires = strtotime('+14 days', strtotime($item['claimed_time']));
                                                $start = date("Y-m-d H:i:s", time());
												$end = date("Y-m-d H:i:s", $expires);

                                                require_once('lib/carbon/carbon_script.php');
                                                $script = new CarbonScript();
                                                $fuzzy_date = $script->fuzzy_date_diff($start, $end,1);

//												echo "<p class='muted'>Expires in $days days, $hours hours, $mins minutes, $secs seconds</p>";
                                                echo "<p class='muted'>Expires in $fuzzy_date</p>";
											}
										?>
									</div>
									<div class="modal-body">
										<div class="row">
											<?php
												// Load MobileDetect library
												require_once('lib/mobiledetect/Mobile_Detect.php');
												$detector = new Mobile_Detect;
											?>
											<?php if(!$detector->isMobile()): ?>
												<div class="span1">
													<img src="<?php echo 'drive/donations/' . $item['image']; ?>" alt="<?php echo $item['item_name']; ?>" />
													<?php
														switch($item['claim_status']) {
															default:
																echo '<span class="badge">Unreceived</span>';
																break;
															case 2:
																echo '<span class="badge badge-success">Received</span>';
																break;
														}
													?>
												</div>
												<div class="span4">
											<?php else: ?>
												<div class="span5">
											<?php endif; ?>
												<h4><?php echo $item['item_name']; ?></h4>
												<p><?php echo $item['item_description']; ?></p>
												<table class="table table-striped">
													<tr>
														<th>Condition:</th>
														<td> <p id="conditions">
																<input type="radio" id="great" name="conditions" value="Great">
																&nbsp;Great &nbsp;&nbsp;
																<input type="radio" id="good" name="conditions" value="Good">
																	&nbsp;Good &nbsp;&nbsp;
																<input type="radio" id="ok" name="conditions" value="Ok">
																&nbsp;Ok &nbsp;&nbsp;</p>
															</td>
													</tr>
													<tr>
														<th>Donator</th>
														<td colspan="3">
															<?php if(intval($item['claim_status']) == 0): ?>
																Unclaimed
																<a class="share-button" id="item-<?php echo $item['id']; ?>-share-button">Share on Facebook</a>
															<?php else: ?>
																<?php
																	// Load claimer information
																	$query = 'select email, location from userinfo where id = ' . $item['donated_by'];
																	$result = mysql_query($query);

																	if($result) {
																		$claimer = mysql_fetch_array($result);
																	}
																?>

																<!-- TODO: Change email to fname/lname -->
																<a href="<?php echo 'mailto:' . $claimer['email']; ?>">
																	<?php echo $claimer['email']; ?>
																</a>		
															<?php endif; ?>
														</td>
													</tr>
													<?php if(intval($item['claim_status']) == 1): ?>
														<tr>
															<th>Location</th>
															<td colspan="3">
																<?php
																	if(!empty($claimer['location'])) {
																		// Load location information
																		$query = 'select location from location where id = ' . $claimer['location'];
																		$result = mysql_query($query);

																		if($result) {
																			$array = mysql_fetch_array($result);
																			$location = $array['location'];
																		} else {
																			$location = 'No location set';
																		}
																	} else {
																		$location = 'No location set';
																	}

																	echo $location;
																?>
															</td>
														</tr>
													<?php endif; ?>
												</table>
											</div>
										</div>
										<a href="#" class="btn btn-mini toggle-comments collapsed" data-toggle="collapse" data-target="#<?php echo 'claimed-item-' . $item['id'] . '-comments'; ?>">Show comments</a>
										<div class="row collapse" id="<?php echo 'claimed-item-' . $item['id'] . '-comments'; ?>">
											<div class="span12">
											    <h3>Comments</h3>

												<?php
													// Load comments
													$comments = array();
													$query = 'select text, timestamp, user_id from comments where item_id = ' . $item['id'];
													$result = mysql_query($query);

													if($result) {
														while($row = mysql_fetch_assoc($result)) {
														    array_push($comments, $row);
														}
													}
												?>

												<div class="comments">
												    <?php if(!empty($comments)): ?>
													    <?php foreach($comments as $comment): ?>
													  		<?php
																// Get user information
																$query = 'select * from userinfo where id = ' . $comment['user_id'];
																$result = mysql_query($query);

																if($result) { 
																	$user = mysql_fetch_assoc($result);
																}
															?>
													 
													  		<div class="row comment">
														    	<div class="span1">
															    	<?php if(!empty($user['propicpath'])): ?>
																    	<img src="drive/avatars/<?php echo $user['propicpath']; ?>" alt="<?php echo $user['Username']; ?>'s avatar" />
																  	<?php else: ?>
																    	<img src="img/default.png" alt="<?php echo $user['Username']; ?>'s avatar" />
																  	<?php endif; ?>
															   </div>
															   <div class="span4">
																	<p>
																    	<strong><?php echo $user['Username']; ?></strong><br />
																		<?php echo $comment['text']; ?><br />
																		<small class="muted">Posted <?php  
																		
																		$expires = strtotime($comment['timestamp']);
																		$start = date("Y-m-d H:i:s", time());
																		$end = date("Y-m-d H:i:s", $expires);

																		require_once('lib/carbon/carbon_script.php');
																		$script = new CarbonScript();
																		$fuzzy_date = $script->fuzzy_date_diff($start, $end);
																		echo $fuzzy_date;
																		
																		?></small>
																	</p>
															   </div>
													  		</div>
														<?php endforeach; ?>
													<?php else: ?>
												    	<p class="muted no-comments">No comments yet</p>
													<?php endif; ?>
												</div>

												<h4>Post Comment</h4>
												<form action="scripts/comment.php" method="post" class="comment-form">
												    <textarea name="text" placeholder="Enter your comment here" class="span5 comment-text"></textarea>
												    <input type="hidden" name="item_id" value="<?php echo $item['id']; ?>" class="comment-item-id" />
												    <input type="hidden" name="user_id" value="<?php echo $_SESSION['userinfo']['id']; ?>" class="comment-user-id" />
												    <button class="btn btn-small btn-primary">Post</button>
												</form>
											</div>
										</div>
									</div>
									
									<div class="modal-footer">
										<button class="btn btn-danger" onClick="disp_prompt_confirm(<?php echo $item['id']; ?>)" data-dismiss="modal"<?php if($item['claim_status']=='2'){?> enabled <?php } else {?> disabled <?php }?>>Confirm received</button>	
										<button class="btn btn-primary" onClick="disp_prompt_cancel(<?php echo $item['id']; ?>)" data-dismiss="modal"<?php if($item['claim_status']=='2'){?> disabled <?php } else {?> enabled <?php }?>>Cancel claim</button>
										<button class="btn" data-dismiss="modal">Close</button>
									</div>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<h4 class="muted">You have no claimed items!</h4>
						<?php endif; ?>
					</div>

					<div class="donated well" >
						<h3 id="donated" id="business" data-toggle="tooltip" data-placement="top" title="Click each box to see more"> Donated Items </h3>
						<?php if(sizeof($donated_items) != 0): ?>
							<?php foreach($donated_items as $item): ?>
								<!-- Modal trigger -->
								<a href="<?php echo '#donated-item-' . $item['id'] . '-modal'; ?>" data-toggle="modal" class="thumb">
									<img src="<?php echo 'drive/donations/' . $item['image']; ?>" alt="<?php echo $item['item_name']; ?>" class="img-polaroid"/>
									<?php
										switch($item['claim_status']) {
											case 0:
												echo '<span class="badge">Unclaimed</span>';
												break;
											case 1:
												echo '<span class="badge badge-success">Claimed</span>';
												break;
											case 2:
												echo '<span class="badge badge-warning">Sent</span>';
												break;
											case 3:
												echo '<span class="badge badge-info">Received</span>';
												break;
											case 4:
												echo '<span class="badge badge-important">Cancelled</span>';
												break;
											case 5:
												echo '<span class="badge badge-inverse">Processed</span>';
												break;
										}
									?>
								</a>

								<!-- Item modal -->
								<div id="<?php echo 'donated-item-' . $item['id'] . '-modal'; ?>" class="modal hide fade" tabindex="-1" role="dialog">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Item Details</h3>
										<?php 
											if($item['claimed_time'] != "0000-00-00 00:00:00") {
												
												$expires = strtotime('+14 days', strtotime($item['claimed_time']));
                                                $start = date("Y-m-d H:i:s", time());
												$end = date("Y-m-d H:i:s", $expires);

                                                require_once('lib/carbon/carbon_script.php');
                                                $script = new CarbonScript();
                                                $fuzzy_date = $script->fuzzy_date_diff($start, $end,1);

//												echo "<p class='muted'>Expires in $days days, $hours hours, $mins minutes, $secs seconds</p>";
                                                echo "<p class='muted'>Expires in $fuzzy_date</p>";
											}
										?>
									</div>
									<div class="modal-body">
										<div class="row">
											<?php
												// Load MobileDetect library
												require_once('lib/mobiledetect/Mobile_Detect.php');
												$detector = new Mobile_Detect;
											?>
											<?php if(!$detector->isMobile()): ?>
												<div class="span1">
													<img src="<?php echo 'drive/donations/' . $item['image']; ?>" alt="<?php echo $item['item_name']; ?>" />
													<?php
														switch($item['claim_status']) {
															case 0:
																echo '<span class="badge">Unclaimed</span>';
																break;
															case 1:
																echo '<span class="badge badge-success">Claimed</span>';
																break;
															case 2:
																echo '<span class="badge badge-warning">Sent</span>';
																break;
															case 3:
																echo '<span class="badge badge-info">Received</span>';
																break;
															case 4:
																echo '<span class="badge badge-important">Cancelled</span>';
																break;
															case 5:
																echo '<span class="badge badge-inverse">Processed</span>';
																break;
														}
													?>
												</div>
												<div class="span4">
											<?php else: ?>
												<div class="span5">
											<?php endif; ?>
												<h4><?php if ($item['show_status'] == 1) { echo $item['item_name']; } else { ?> Unprocessed (done within 24 hours) <?php } ?></h4>
												<p><?php echo $item['item_description']; ?></p>
												<table class="table table-striped">
													<tr>
														<th>Value</th>
														<td>
															<span class="label label-success">Great</span> <?php echo $item['emax']; ?>PP
														</td>
														<td>
															<span class="label label-info">Good</span> <?php echo $item['emed']; ?>PP
														</td>
														<td>
															<span class="label label-default">OK</span> <?php echo $item['emin']; ?>PP
														</td>
														
													</tr>
													<tr>
														<th>Claimer</th>
														<td colspan="3">
															<?php if(intval($item['claim_status']) == 0): ?>
																Unclaimed

																<a class="share-button" id="item-<?php echo $item['id']; ?>-share-button">Share on Facebook</a>
																
															<?php else: ?>
																<?php
																	// Load claimer information
																	$query = 'select email, location from userinfo where id = ' . $item['claimed_by'];
																	$result = mysql_query($query);

																	if($result) {
																		$claimer = mysql_fetch_array($result);
																	}
																?>

																<!-- TODO: Change email to fname/lname -->
																<a href="<?php echo 'mailto:' . $claimer['email']; ?>">
																	<?php echo $claimer['email']; ?>
																</a>		
															<?php endif; ?>
														</td>
													</tr>
													<?php if(intval($item['claim_status']) == 1): ?>
														<tr>
															<th>Location</th>
															<td colspan="3">
																<?php
																	if(!empty($claimer['location'])) {
																		// Load location information
																		$query = 'select location from location where id = ' . $claimer['location'];
																		$result = mysql_query($query);

																		if($result) {
																			$array = mysql_fetch_array($result);
																			$location = $array['location'];
																		} else {
																			$location = 'No location set';
																		}
																	} else {
																		$location = 'No location set';
																	}

																	echo $location;
																?>
															</td>
														</tr>
													<?php endif; ?>
												</table>
											</div>
										</div>
										<a href="#" class="btn btn-mini toggle-comments collapsed" data-toggle="collapse" data-target="#<?php echo 'donated-item-' . $item['id'] . '-comments'; ?>">Show comments</a>
										<div class="row collapse" id="<?php echo 'donated-item-' . $item['id'] . '-comments'; ?>">
											<div class="span12">
											    <h3>Comments</h3>

												<?php
													// Load comments
													$comments = array();
													$query = 'select text, timestamp, user_id from comments where item_id = ' . $item['id'];
													$result = mysql_query($query);

													if($result) {
														while($row = mysql_fetch_assoc($result)) {
														    array_push($comments, $row);
														}
													}
												?>

												<div class="comments">
												    <?php if(!empty($comments)): ?>
													    <?php foreach($comments as $comment): ?>
													  		<?php
																// Get user information
																$query = 'select * from userinfo where id = ' . $comment['user_id'];
																$result = mysql_query($query);

																if($result) { 
																	$user = mysql_fetch_assoc($result);
																}
															?>
													 
													  		<div class="row comment">
														    	<div class="span1">
															    	<?php if(!empty($user['propicpath'])): ?>
																    	<img src="drive/avatars/<?php echo $user['propicpath']; ?>" alt="<?php echo $user['Username']; ?>'s avatar" />
																  	<?php else: ?>
																    	<img src="img/default.png" alt="<?php echo $user['Username']; ?>'s avatar" />
																  	<?php endif; ?>
															   </div>
															   <div class="span4">
																	<p>
																    	<strong><?php echo $user['Username']; ?></strong><br />
																		<?php echo $comment['text']; ?><br />
																		<small class="muted">Posted <?php  
																		
																		$expires = strtotime($comment['timestamp']);
																		$start = date("Y-m-d H:i:s", time());
																		$end = date("Y-m-d H:i:s", $expires);

																		require_once('lib/carbon/carbon_script.php');
																		$script = new CarbonScript();
																		$fuzzy_date = $script->fuzzy_date_diff($start, $end,0);
																		echo $fuzzy_date;
																		
																		?></small>
																	</p>
															   </div>
													  		</div>
														<?php endforeach; ?>
													<?php else: ?>
												    	<p class="muted no-comments">No comments yet</p>
													<?php endif; ?>
												</div>

												<h4>Post Comment</h4>
												<form action="scripts/comment.php" method="post" class="comment-form">
												    <textarea name="text" placeholder="Enter your comment here" class="span5 comment-text"></textarea>
												    <button class="btn btn-small btn-primary">Post</button>
												    <input type="hidden" name="item_id" value="<?php echo $item['id']; ?>" class="comment-item-id" />
												    <input type="hidden" name="user_id" value="<?php echo $_SESSION['userinfo']['id']; ?>" class="comment-user-id" />
												</form>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn btn-danger" onClick="disp_prompt_sent(<?php echo $item['id']; ?>)" data-dismiss="modal" <?php if($item['claim_status']=='1'){?> enabled <?php } else {?> disabled <?php }?>>Confirm sent</button>	
										<button class="btn btn-primary" data-dismiss="modal" onClick="disp_prompt_remclaim(<?php echo $item['id']; ?>)" <?php if($item['claim_status']=='1'){?> enabled <?php } else {?> disabled <?php }?>>Remove claimer</button>
										<button class="btn btn-primary" data-dismiss="modal" onClick="disp_prompt_donate(<?php echo $item['id']; ?>)" <?php if($item['claim_status']=='2'){?> disabled <?php } else {?> enabled <?php }?>>Cancel donation</button>
										<button class="btn" data-dismiss="modal">Close</button>
									</div>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<h4 class="muted">You have no donated items!</h4>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>

		<script>
		$(function ()
		{ $("#business").popover();
		});
		</script>
		
		
		<script>
		$(function ()
		{ $("#claimed").popover();
		});
		</script>
		
		
		<script>
		$(function ()
		{ $("#donated").popover();
		});
		</script>

		<script>
		
			function disp_prompt_donate(id) {
				
				var conf=confirm("Are you sure you want to cancel this donation? (This can't be undone)");	
				
				if(conf==true) {
					
					$.ajax({
						type: "POST",
						url:  "scripts/canceldonate.php", 
						data: {id: id},
						success: function(data) {
							window.location = "profile.php";

						}

					});
					
	  
				}  
				
			}	
			
			function disp_prompt_remclaim(id) {
				
				var conf=confirm("Are you sure you want to remove the claimer from claiming this item, and reset it on The City?");	
				
				if(conf==true) {
					
					$.ajax({
						type: "POST",
						url:  "scripts/removeclaimer.php", 
						data: {id: id},
						success: function(data) {
							window.location = "profile.php";
						
						}

					});
					
					
	  
				}  
				
				
			}

			function disp_prompt_sent(id) {

				var conf=confirm("Are you sure you have given this item away? Make sure you inform the donator that they press 'Confirm Recieved' on their profile page so they get their points.");
				
				if(conf==true) {
					$.ajax({
						type: "POST",
						url:  "scripts/confirmsent.php", 
						data: {id: id},
						success: function(data) {
							window.location = "profile.php";
						}

					});
				}
			
				
					
			}	

			function disp_prompt_reward(type) {

				if (type == "0") {
				
					var conf=confirm("WARNING - This will show you your reward and remove it. Make sure you are actually presenting this to the cashier before pressing yes. Are you sure you want to proceed?");	
					
					if(conf==true) {
						
						return true;
						  
					}
					
					return false;
				} else if (type == "1") {
				
					var conf=confirm("Are you sure you have used this promo code and want to remove it?");
					
					if(conf==true) {
						
						return true;
						  
					}
					
					return false;
				
				}
				
			}	
			
			function disp_prompt_confirm(id) {

				if(document.getElementById('great').checked || document.getElementById('good').checked || document.getElementById('ok').checked) {

					var radios = document.getElementsByName("conditions");
					for (var i = 0; i < radios.length; i++) {       
						if (radios[i].checked) {
						
							$.ajax({
								type: "POST",
								url:  "scripts/confirmreceived.php", 
								data: {value: radios[i].value, id: id},
								success: function(data) {
								
									window.location = "profile.php";
								}

							});
							break;
						}
						
					}
			
				} else {

					alert("Please select a condition");
				
				}

			}	

			
			function disp_prompt_cancel(id) {
				
				var conf=confirm("Are you sure you want to cancel this claim? (This can't be undone)");	
				
				if(conf==true) {
					
					$.ajax({
						type: "POST",
						url:  "scripts/cancelclaim.php", 
						data: {id: id},
						success: function(data) {
							
							window.location = "profile.php";

						}

					});
							
				}
								
			}


			
		</script>
		<!-- Footer -->
		<?php include "footer.php";?>
	</body>
</html>