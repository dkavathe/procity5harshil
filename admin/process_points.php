<?php
session_start();
require_once 'library/functions.php';
require_once 'library/config.php';
include 'library/logincheck.php';
	
	$donate_id=mysql_real_escape_string($_GET['item']);
	$user_id=mysql_real_escape_string($_GET['user']);
		
	$sql_item=mysql_query("select * from donate_item where id='$donate_id'") or die(mysql_error());
	$fetch_item=mysql_fetch_array($sql_item);
	$image=$fetch_item['image'];
	$itemid=$fetch_item['item_id'];

	$sql=mysql_query("select * from item where prod_id='$itemid'") or die(mysql_error());
	$fetch=mysql_fetch_array($sql);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/template.css" type="text/css"/>

<script src="js/jquery-1.8.2.min.js" type="text/javascript">
	</script>
	<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>
	<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#category").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>

</head>

<body>

<?php include('left-nav.php');?> <!--left-menu -->

<div class="page">
    
    <div class="page-nav">
    	
        <div class="clear"></div> <!--clear div -->

    </div> <!--page nav -->
    
  <div class="box1">
    	<h1> Edit category</h1>
		<form action="" name="category" id="category" method="post" >
        
<table  width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 3px #CCCCCC;" align="center">
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td width="17%" valign="top"><strong>User ID</strong></td>
    <td width="83%"><input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>" /></td>
  </tr>

  <tr>
    <td width="17%" valign="top"><strong>Item Id</strong></td>
    <td width="83%"><input type="hidden" name="donate_id" id="donate_id" value="<?php echo $donate_id;?>" /></td>
  </tr>
  
  <tr>
  <td >
	<p class="tbltext" id="conditions">
	<input type="radio" id="great" name="conditions" value="Great" /> &nbsp;Great
    <input type="radio"  id="good" name="conditions" value="Good" /> &nbsp;Good
    <input type="radio" id="ok" name="conditions" value="Ok" />&nbsp; Ok</p
	
    </td>
	</tr>
  <tr>
<td colspan="2">&nbsp;</td>
</tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="button" name="process" class="btn1" value="Process" onclick="foo()" /></td>
  </tr>
</table>



</form>

  </div><!--box1 -->
    <!--box 2 -->
<p>
        
   
    <br />
    <br />
    <br />
    <!-- sucess -->
    <!-- error -->
    <!-- warning -->
    <!-- information -->
<?php include "footer.php";?>
<!--footer -->
</div><!--page -->

<div class="clear"></div> <!--clear div -->

<script>
function foo() {

	if(document.getElementById('great').checked || document.getElementById('good').checked || document.getElementById('ok').checked) {

		var id=document.getElementById('user_id').value;
		var donate=document.getElementById('donate_id').value;
		var radios = document.getElementsByName("conditions");
		
		for (var i = 0; i < radios.length; i++) {       

			if (radios[i].checked) {
				window.location="itemprocess.php?condition="+radios[i].value+"&user_id="+id+"&donate_id="+donate;
				break;
				
			}
		}
			
	} else {
	
		alert("Please select a condition");
		
	}

}

</script>

</body>
</html>
