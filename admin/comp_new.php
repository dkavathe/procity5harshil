<?php
session_start();
require_once 'library/functions.php';
require_once 'library/config.php';
include 'library/logincheck.php';

	if(isset($_POST['join_pro']))
	{
	
		$user = mysql_real_escape_string($_POST['Username']);
		$email = mysql_real_escape_string($_POST['Email']);
		$pass = mysql_real_escape_string($_POST['Password']);
		$conf = mysql_real_escape_string($_POST['Confirm']);
		
		if($pass != $conf) {
	
			?>
				<script> alert('Passwords dont match'); window.location="comp_new.php";</script>
				
				<?php
			return;
			
		}
		
		if($user == '' || $email == '' || $pass == '') {
			?>
				<script> alert('Fields empty'); window.location="comp_new.php";</script>
				
				<?php
			return;
		}
		
		$at = strpos($email,"@");

		if($at <= 0) {

			?>
				<script> alert('Invalid email'); window.location="comp_new.php";</script>
				
				<?php
				return;
		}
			
	
		$ensure = "SELECT Username from userinfo WHERE Username = '$user'";
		$rowsu = mysql_query($ensure);

		if(mysql_num_rows($rowsu) > 0) {

			?>
			<script> alert('Username taken'); window.location="comp_new.php";</script>
			
			<?php
			return;
		}

		$ensure = "SELECT Email from userinfo WHERE Email = '$email'";
		$rowse = mysql_query($ensure);


		if(mysql_num_rows($rowse) > 0) {

			?>
			<script> alert('Email taken'); window.location="comp_new.php";</script>
			
			<?php
			return;

		} else{
		
			$location = mysql_real_escape_string($_POST['location']);
			$locq = mysql_query("select * from location where id='$location'") or die(mysql_error());
			$locarr = mysql_fetch_array($locq);
			$oldcount = $locarr['count'];
			
			$loctotal = $oldcount + 1;
			$update_user_rec=mysql_query("update location set count='$loctotal' where id='$location'") or die(mysql_error());
			
			$propointsearn = mysql_real_escape_string($_POST['propoints']);
			$total= $propointsearn + 25;
				
			$encrypt = md5(md5(md5($pass)));				
			$qry = "INSERT INTO userinfo (FirstName, LastName, Username, Password, Email,ProPoints,hash,loginid,status,Location,num_donated,num_claimed,num_rewards,propicpath) VALUES 
			('','','$user', '$encrypt', '$email','$total','','','1','','0','0','0','')";
			
			mysql_query($qry);

					
			$msg= "Hi $user!\n\n Thanks for participating in the donation competition! Check out The City for rewards coming soon from local businesses, which you can use your ProPoints for! \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com"; 
			
			$subject= "Welcome to Procity!";
			$headers = "From: procitystaff@gmail.com \r\n";
			$headers .= "Reply-To: procitystaff@gmail.com\r\n";
			$headers .= "Return-Path: procitystaff@gmail.com\r\n";

			mail($email,$subject,$msg,$headers);

			?>
			<script> alert('Success'); window.location="comp_new.php";</script>
			
			<?php
		}
				
	
	}
	
	
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/template.css" type="text/css"/>

	<script src="js/jquery-1.8.2.min.js" type="text/javascript">
	</script>
	<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>
	<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#category").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>
</head>

<body>

<?php include('left-nav.php');?> <!--left-menu -->

<div class="page">
    
    <div class="page-nav">
    	
        <div class="clear"></div> <!--clear div -->

    </div> <!--page nav -->
    
  <div class="box1">
    	<h1> Donate and create an account</h1>
        
       
		<form action="comp_new.php" name="category" id="category" method="post" >
        
<table  width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 3px #CCCCCC;" align="center">
<tr>
<td colspan="2">&nbsp;</td>
</tr>

  <tr>
    <td width="17%" valign="top"><strong>Username</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Username" id="Username" /></td>
  </tr>

  <tr>
    <td width="17%" valign="top"><strong>Email</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Email" id="Email" /></td>
  </tr>
  <tr>
    <td width="17%" valign="top"><strong>Password</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Password" id="Password" /></td>
  </tr>
  <tr>
    <td width="17%" valign="top"><strong>Confirm Password</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Confirm" id="Confirm" /></td>
  </tr>
    <tr>
    <td width="17%" valign="top"><strong>ProPoints earned</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="propoints" id="propoints" /></td>
  </tr>
   <tr>
    <td width="17%" valign="top"><strong>Location</strong></td>
    <td width="83%">
	
	<select name="location" id="location">
		<option id="28" value="28">Somerset Hall</option>
		<option id="20" value="20">Queen Annes Hall</option>
		
	</select>

	
	</td>
  </tr>
  <tr>
<td colspan="2">&nbsp;</td>
</tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="join_pro" class="btn1" value="Join the City!!" /></td>
  </tr>
</table>



</form>

  </div><!--box1 -->
    <!--box 2 -->
<p>
        
   
    <br />
    <br />
    <br />
    <!-- sucess -->
    <!-- error -->
    <!-- warning -->
    <!-- information -->

<!--footer -->
<?php include "footer.php";?>
</div><!--page -->

<div class="clear"></div> <!--clear div -->

</body>
</html>
