<?php

	session_start();
	require_once 'library/functions.php';
require_once 'library/config.php';
include 'library/logincheck.php';
	
	if(isset($_POST['signup']))
	{
		
		
		$user = mysql_real_escape_string($_POST['Username']);
		$email = mysql_real_escape_string($_POST['Email']);
		$pass = mysql_real_escape_string($_POST['Password']);
		$conf = mysql_real_escape_string($_POST['Confirm']);
		$name = mysql_real_escape_string($_POST['Name']);
		
		$user = trim($user);
		$email = trim($email);
		$pass = trim($pass);
		$conf = trim($conf);
		$name = trim($name);
		
		if($pass != $conf) {
			?>
				<script> alert('Passwords dont match'); window.location="addbusiness.php";</script>
				
				<?php
			return;
	
		}
		
		$at = strpos($email,"@");

		if($at <= 0) {

			?>
				<script> alert('Invalid email'); window.location="addbusiness.php";</script>
				
				<?php
			return;
		}
		
		if($user == '' || $email == '' || $pass == '' || $conf == '' || $name == '') {
			
			?>
				<script> alert('Empty fields'); window.location="addbusiness.php";</script>
				
				<?php
			return;
		}
		
		$ensure = "select name from businessinfo WHERE name = '$name'";
		$rowsu = mysql_query($ensure);

		if(mysql_num_rows($rowsu) > 0) {

			?>
				<script> alert('Name taken'); window.location="addbusiness.php";</script>
				
				<?php
			return;
		}
		
		$ensure = "select username from businessinfo WHERE username = '$user'";
		$rowsu = mysql_query($ensure);

		if(mysql_num_rows($rowsu) > 0) {

			?>
				<script> alert('Username taken'); window.location="addbusiness.php";</script>
				
				<?php
			return;
		}
		
		/*$ensure = "select email from businessinfo WHERE email = '$email'";
		$rowsu = mysql_query($ensure);

		if(mysql_num_rows($rowsu) > 0) {

			?>
				<script> alert('Email taken'); window.location="addbusiness.php";</script>
				
				<?php
			return;
		}*/
		
		$encrypt = md5(md5(md5($pass)));
		$qry = "INSERT INTO businessinfo (name, username, email, password, status,numsales,numearned,numupload) VALUES 
		('$name','$user','$email', '$encrypt', '1','0','0','0')";

		if (mysql_query($qry)) {

			/*$msg= "Hi $name!\n\n Welcome to Procity, we hope our partnership sustains your business's profit. Your username is \n\n Username: $user \n Password: $pass \n\n Access your special account at  http://www.myprocity.com/business . \n\n Once signed in you will have privileges to post rewards from your business (discounts,coupons,specials,deals etc). Please use the Stats page on our site to evaluate the economy of ProPoints before deciding on coupon offers. \n\n  You will have the freedom to change each reward's attributes. Thanks! \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com"; 
			$email_to=$email;
			$template=$msg;
			$subject= "Welcome to Procity!";
			$headers = "From: procitystaff@gmail.com \r\n";
			$headers .= "Reply-To: procitystaff@gmail.com\r\n";
			$headers .= "Return-Path: procitystaff@gmail.com\r\n";

			mail($email_to,$subject,$template,$headers);	*/
			
			?>
				<script> alert('Success welcome to Procity!'); window.location="addbusiness.php";</script>
				
				<?php
				
		} else {
			?>
			<script> alert('Error!'); window.location="addbusiness.php";</script>
				
				<?php
			return;
			
		}
		
	}
	
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/template.css" type="text/css"/>

	<script src="js/jquery-1.8.2.min.js" type="text/javascript">
	</script>
	<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>
	<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#category").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>
</head>

<body>

<?php include('left-nav.php');?> <!--left-menu -->

<div class="page">
    
    <div class="page-nav">
    	
        <div class="clear"></div> <!--clear div -->

    </div> <!--page nav -->
    
  <div class="box1">
    	<h1> Add a local business</h1>
        
       
		<form action="addbusiness.php" name="category" id="category" method="post" >
        
<table  width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 3px #CCCCCC;" align="center">
<tr>
<td colspan="2">&nbsp;</td>
</tr>

	<tr>
    <td width="17%" valign="top"><strong>Name</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Name" id="Name" /></td>
  </tr>
  
  <tr>
    <td width="17%" valign="top"><strong>Email</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Email" id="Email" /></td>
  </tr>

  <tr>
    <td width="17%" valign="top"><strong>Username</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Username" id="Username" /></td>
  </tr>
  
   <tr>
    <td width="17%" valign="top"><strong>Password</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Password" id="Password" /></td>
  </tr>
  
   <tr>
    <td width="17%" valign="top"><strong>Confirm Password</strong></td>
    <td width="83%"><input type="text" class="validate[required] txt-feild-small" name="Confirm" id="Confirm" /></td>
  </tr>
   
  <tr>
<td colspan="2">&nbsp;</td>
</tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="signup" class="btn1" value="Add business" /></td>
  </tr>
</table>



</form>

  </div><!--box1 -->
    <!--box 2 -->
<p>
        
   
    <br />
    <br />
    <br />
    <!-- sucess -->
    <!-- error -->
    <!-- warning -->
    <!-- information -->

<!--footer -->
<?php include "footer.php";?>
</div><!--page -->

<div class="clear"></div> <!--clear div -->

</body>
</html>
