<?php
	// Start session
	session_start();

	// Check if user is logged in
	if(!isset($_SESSION['userinfo'])) {
		// Redirect to login page
		header('Location: signin.php');
	}

	// Connect to database
	include "scripts/config.php";

	if(isset($_POST['busid']) && isset($_POST['item_id']) && isset($_POST['type'])) {
	
		$type = mysql_real_escape_string($_POST['type']); 
				
		$busid = mysql_real_escape_string($_POST['busid']); 
		$sql_donate=mysql_query("select * from business_item where id='$busid' AND showstatus='1'") or die(mysql_error());
		$fetch_reward=mysql_fetch_array($sql_donate);

		$businessid = $fetch_reward['businessid'];
		$bus = mysql_query("select * from businessinfo where id='$businessid'") or die(mysql_error());
		$bussarr = mysql_fetch_array($bus);

		$businessname = $bussarr['name'];
		$name = $fetch_reward['name'];
		$descrip = $fetch_reward['description'];


		$user_id = $_SESSION['userid'];		
		$username = $_SESSION['username'];		
		$reward_id = mysql_real_escape_string($_POST['item_id']);


		// (add profits and count) add to sale total, numearned total

		$findbusiness = mysql_query("select * from dealrecords where id='$reward_id'") or die(mysql_error());
		$numrows = mysql_num_rows($findbusiness);

		if ($numrows <= 0) {

			echo "You've already used this coupon";
			exit(1);

		}
		
		// minus minus num_rewards

		$sql_minusreward = mysql_query("select * from userinfo where Id='$user_id'") or die(mysql_error());
		$fetch_minusreward = mysql_fetch_array($sql_minusreward);
		$numreward = $fetch_minusreward['num_rewards'];

		$numreward = $numreward - 1;
		$update_reward = mysql_query("update userinfo set num_rewards='$numreward' where Id='$user_id'")or die(mysql_error());

		$fetch_business = mysql_fetch_array($findbusiness);

		$businessid = $fetch_business['businessid'];
		$rewardid = $fetch_business['rewardid'];

		$finditem = mysql_query("select * from business_item where id='$rewardid'") or die(mysql_error());
		$fetch_item = mysql_fetch_array($finditem);
		$saleworth = $fetch_item['saleworth'];
		$propointsworth = $fetch_item['propointworth'];
		$numcount = $fetch_item['numcount'];
		$rewardsale = $fetch_item['numsales'];

		$incrbus = mysql_query("select * from businessinfo where id='$businessid'") or die(mysql_error());
		$incrbussarr = mysql_fetch_array($incrbus);
		$currtotal = $incrbussarr['numearned'];
		$currsale = $incrbussarr['numsales'];

		$newsale = $currsale + $saleworth;
		$rewardsale = $rewardsale + $saleworth;

		$currtotal = $currtotal + 1;
		$numcount = $numcount + 1;

		$update_reward = mysql_query("update businessinfo set numsales='$newsale' where id='$businessid'")or die(mysql_error());
		$update_reward = mysql_query("update businessinfo set numearned='$currtotal' where id='$businessid'")or die(mysql_error());
		$update_reward = mysql_query("update business_item set numcount='$numcount' where id='$rewardid'")or die(mysql_error());
		$update_reward = mysql_query("update business_item set numsales='$rewardsale' where id='$rewardid'")or die(mysql_error());

		// delete from dealrecords
		$del = "delete from dealrecords where id='$reward_id'";
		mysql_query($del) or die(mysql_error());			

		if($type == "1") {
		
			header('Location: profile.php');
		}
		
	} else {
	
		echo "Invalid request please go back";
		exit(1);
	
	}
?>

<!DOCTYPE HTML>

<html>
	<head>
		<!-- Page metadata -->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Procity Reward Page</title>

		<!-- Stylesheets -->
		<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/responsive.min.css" />
		<link rel="stylesheet" href="css/theme.css" />
		<style type="text/css">
			@-moz-keyframes blink {0%{opacity:1;} 50%{opacity:0;} 100%{opacity:1;}} /* Firefox */
			@-webkit-keyframes blink {0%{opacity:1;} 50%{opacity:0;} 100%{opacity:1;}} /* Webkit */
			@-ms-keyframes blink {0%{opacity:1;} 50%{opacity:0;} 100%{opacity:1;}} /* IE */
			@keyframes blink {0%{opacity:1;} 50%{opacity:0;} 100%{opacity:1;}} /* Opera/CSS 3 */

			img {
				-moz-transition:all 1s ease-in-out;
				-webkit-transition:all 1s ease-in-out;
				-o-transition:all 1s ease-in-out;
				-ms-transition:all 1s ease-in-out;
				transition:all 1s ease-in-out;

				/* Order: name, direction, duration, iteration-count, timing-function */   
				-moz-animation:blink normal 2s infinite ease-in-out; /* Firefox */
				-webkit-animation:blink normal 2s infinite ease-in-out; /* Webkit */
				-ms-animation:blink normal 2s infinite ease-in-out; /* IE */
				animation:blink normal 2s infinite ease-in-out; /* Opera and prob css3 final iteration */
			}​
		</style> 
	</head>
	<body class="container">
		<h1><?php echo $businessname; ?> - <?php echo $name; ?></h1>
		<img src="img/coupon.png" />
		<h2><?php echo $descrip;?></h2>
		<p class="lead">Procity Reward Page</p>

		<a href="profile.php" class="btn btn-large btn-primary">Done</a>
	</body>
</html>
