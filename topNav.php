<script type="text/javascript" language="javascript" src="js/jquery.dropdownPlain.js"></script>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
	
		<div class="container"><a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand scroller"  href="http://www.myprocity.com/index.php"> <img src="img/procitylogo.png" alt="logo" height="50" width="155"/> </a>
			
			<div class="nav-collapse collapse">
			
				<ul class="nav pull-right">
				
					<!--<li><a> Procitizens: <?php
					/*include "scripts/config.php";
					
					$getusers = mysql_query("select * from userinfo where status=1") or die(mysql_error());
					$total = mysql_num_rows($getusers);
					echo $total;*/
				  ?></a></li> -->

				  <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            The City
                            <b class="caret"></b>
                        </a>
                       <ul class="dropdown-menu">
							<li><a href="items.php">Items</a></li>
                            <li><a href="coupons.php">Coupons</a></li>
                            <li><a href="promocodes.php">Promo-codes</a></li>
                           
                        </ul>
                  </li>
				  <!--<li><a href="howitworks.php">How it Works</a></li>-->
				   <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            How it Works
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="howitworks.php">Become a Procitizen!</a></li>
                            <li><a href="businesses.php">Business information</a></li>
                           
                        </ul>
                  </li>
				  <!--<li><a href="businesses.php">Businesses</a></li>-->
				  <li><a href="organizations.php">Volunteer</a></li>
					<li><a href="donate.php">Donate</a></li>
					<!--<li><a href="items.php">The City</a></li>-->
					
					<!--<li><a href="buypro.php">ProPoints</a></li>-->
										
					<?php
					if(isset($_SESSION['username'])){
						?>

					<li style="padding-top: 8px;"><?php if(!empty($_SESSION['userinfo']['propicpath'])): ?>
								<img src="drive/avatars/<?php echo $_SESSION['userinfo']['propicpath']; ?>" alt="<?php echo $_SESSION['username']; ?>'s avatar" width="25" height="25" />
							<?php else: ?>
								<img src="img/default.png" alt="<?php echo $_SESSION['username']; ?>'s avatar" width="25" height="25" />
							<?php endif; ?></li>
					<li><a href="profile.php"><?php echo $_SESSION['username'];?></a></li>
					<li><a href="scripts/logout.php">Log out</a></li>

					<?php
					}else
					{
					 ?>
					<li><a class="btn-header" href="signup.php">Sign up</a></li>
					<li><a class="btn-header" href="signin.php">Log in</a></li>

					<?php } ?>

				</ul>

			</div>
			
		</div>
		
	</div>
	
</div>
</body>
</html>