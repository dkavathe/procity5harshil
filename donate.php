<?php 
include "scripts/config.php";
include "lib/resizeimage.php";
session_start();

date_default_timezone_set('America/New_York');

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){

	if(!isset($_SESSION['username'])) {
		header('Location:signin.php');
		return;
	} else {

		$userid=$_SESSION['userid'];
		
		$getusers=mysql_query("select * from userinfo where Id='$userid'") or die(mysql_error());
		$fetch_maxdonate=mysql_fetch_array($getusers);
		$donatoremail=$fetch_maxdonate['Email'];
		$donate_username=$fetch_maxdonate['Username'];
		$maxdonate=$fetch_maxdonate['num_donated'];
		
		if($maxdonate >= 0 && $maxdonate < 10) {

			$cat_id=mysql_real_escape_string($_POST['category']);
			$item=mysql_real_escape_string($_POST['item']);
			$condition=1;
			$descp=mysql_real_escape_string($_POST['descp']);
			$time = date("Y-m-d H:i:s"); 
			
			$pic=$_FILES['image']['name'];
		
			$types = array('image/jpeg', 'image/gif', 'image/png');  
		
			if(in_array($_FILES['image']['type'], $types)) {
		
		
				$file_size = $_FILES['image']['size'];
				
				if ($file_size <= 3145728){ 
				
					$userid=$_SESSION['userid'];
					
					
					$ebase=mysql_real_escape_string($_POST['ebase']);
					$emax=mysql_real_escape_string($_POST['great']);
					$emed=mysql_real_escape_string($_POST['good']);
					$emin=mysql_real_escape_string($_POST['ok']);
					
					$ebase = trim($ebase);
					
					if( $ebase < 0 || $ebase > 999 || !is_numeric($ebase) || !is_numeric($emax) || !is_numeric($emed) ||
					   !is_numeric($emin)) {
					
					?>
					<script> alert('Problem with values'); window.location="donate.php";</script>
					
					<?php
						return;
					}
								
					$insert= "INSERT INTO donate_item (`donated_id` ,`category` ,`sub_category` ,`item_id` ,`item_description` ,`condition` ,`donated_by` ,
							`claimed_by` ,`claim_status` ,`donated_time`,`claimed_time`,`donate_type`,`image`,`show_status`,`ebase`,`emax`,`emed`,`emin`,`item_name`)

							VALUES ( '1', '$cat_id', '', '', '$descp', '$condition', '$userid', '', '0', NOW(), 0, 'unique','','0','$ebase','$emax','$emed','$emin','')";
					
			
					mysql_query($insert) or die(mysql_error());

					
					$lastid= mysql_insert_id();
					$currdate = date("Y-m-dH-i-s");
					$rename = $currdate.$pic;
					$uploadDir = 'drive/donations/'; 
					$filename = mysql_real_escape_string($_FILES['image']['name']);
					  //echo  $pic.$curdate;
						
					$uploadfile = $uploadDir.$rename;             
					move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
					
					$resize = new ResizeImage($uploadfile);
					$resize->resizeTo(300,300,'exact');
					$resize->saveImage($uploadfile);
										
					$update= "update donate_item set item_id='$item' where id='$lastid'";
					mysql_query($update) or die(mysql_error());

					$update= "update donate_item set image='$rename' where id='$lastid'";
					mysql_query($update) or die(mysql_error());
			
					$ids='0000000';
					$record=$ids.$lastid;
					$record;
					$update="update donate_item set donated_id='$record' where id='$lastid'";
					mysql_query($update);
				
					$insert2= "INSERT INTO history (`donate_id` ,`category` ,`sub_category` ,`item_id` ,`item_description` ,`condition` ,`donated_by` ,
						`claimed_by` ,`claim_status` ,`donated_time` ,`donate_type`,`show_status`,`ebase`,`emax`,`emed`,`emin`,`item_name`,`image`)

						VALUES ( '$record', '$cat_id', '', '$item', '$descp', '$condition', '$userid', '', '0', '$time', 'unique','0','$ebase','$emax','$emed','$emin','','')";
				
					mysql_query($insert2) or die(mysql_error());
					
					$incr= $maxdonate + 1;
					$update_maxdonate="update userinfo set num_donated='$incr' where Id='$userid'";
					mysql_query($update_maxdonate) or die(mysql_error());

					$donate_msg="Hey Procitizen $donate_username! \n\nYou have successfully donated an item to Procity’s network! Now you have a few options. Wait for us to process your unique item within 48 hours, then you can sit back, relax, and wait for someone to claim your item, or if you want your ProPoints even faster and there is a Vietnam Veterans of America pick up date approaching, stop by the donation location and give your item to charity -- Instant ProPoints!\n\nYou can find the pick up information on the the homepage of myprocity.com!\n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com";
					$subject= "Donated an unique item";
					$headers = "From: procitystaff@gmail.com \r\n";
					$headers .= "Return-Path: procitystaff@gmail.com\r\n";
					$headers .= "Reply-To: procitystaff@gmail.com\r\n";
						
					mail($donatoremail,$subject,$donate_msg,$headers);	 					
					
					header("location:profile.php?show=donated");
						
				} else {
				
					?>
					<script> alert('Upload to big, max is 3 MB file'); window.location="donate.php";</script>
					
					<?php
				
				}
				
				
			} else {
			
				?>
				<script> alert('Invalid file'); window.location="donate.php";</script>
				
				<?php
			
			}
						 
		} else{
			?>
			<script> alert('Maximum donations allowed is 10'); window.location="profile.php";</script>
				
				
			<?php
		}
		
	}
		
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    
	
    <link rel="stylesheet" type="text/css" href="css/external-pages.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    
      
</head>
<body>
<p><a href="#" class="scrolltop"> <span>up</span> </a></p>
<!-- begins navbar -->
<?php include"topNav.php"; ?>
<!-- starts pricing -->
<div id="pricing" class="pricing_page">
<div class="container">
<!-- header --><hr class="left visible-desktop" />
<h2 class="section_header" style="font-size:53px;"><span style="width:50%;"> Donate to earn ProPoints!</span></h2>
<hr class="right visible-desktop" /><!-- pricing charts 1 -->
<form method="POST" onsubmit="return checkfilesize()" action="donate.php" name="donatefrm" id="donates" enctype="multipart/form-data">
<div class="row" style="margin-left:0px;">
<div class="donationform">
<!--<div class="headerdonate">
<div class="textheadForm">
Donate to Procity</div>
</div>-->
<table style="margin: 20PX 0px -20px 40px; width:100%;">
<tr>
<td colspan="2">&nbsp;</td>
</tr>

<tr>
<td class="tbltext">Item Name</td>
<td><div id="citydiv"><input required="" type="text" id="item" name="item"></div></td>
</tr>

<tr>
<td class="tbltext">Look up ProPoints </td>
<td><button type="button" width="20" height="10" name="searchebay" id="searchebay" onclick = "getEbayVal()" class="btn">Whats my item worth?</button></td>
</tr>

<tr>
<td class="tbltext">Category </td>
<td><select required="" name="category" id="category" onChange="getState(this.value)">
<option value="">Select category</option>
<?php $sql_cat=mysql_query("select * from category") or die(mysql_error());
	while($fetch_cat=mysql_fetch_array($sql_cat))
	{
?>
<option  value="<?php echo $fetch_cat['cat_id'];?>"><?php echo $fetch_cat['cat_name'];?></option>
<?php }?>
</select></td>
</tr>

<tr>
<td class="tbltext">Worth (ProPoints)</td>
<td>
<input required="" id="ebase" name="ebase" onkeypress="return isNumberKey(event)" onkeyup="propointhandler();" type="text"></td>
</tr>
<td class="tbltext">Great condition earn: (PP)</td>
<td>
<input required="" id= "great" name="great" readonly="true" type="text"></td>
</tr>

<tr><td class="tbltext">Good condition earn: (PP)</td>
<td>
<input required="" id= "good" name="good" readonly="true" type="text"></td>
</tr>
<tr>
<td class="tbltext">Ok condition earn: (PP)</td>
<td>
<input required="" id= "ok" name="ok" readonly="true" type="text"></td>
</tr>
<tr>
<td class="tbltext">Brief Description</td>
<td><input required="" type="text" id="descp" name="descp"></td>
</tr>
<tr>
<td class="tbltext">Upload Photo</td>
<td><input required="" id="file" type="file" name="image"></td>
</tr>

<tr>
<td class="tbltext"><a target="_blank" href = "termsandconditions.php" target="_blank" >Accept the Terms and Conditions</a></td>
<td><input required="" type="checkbox" name="accept"</td>
</tr>
<tr>
<td class="tbltext">&nbsp;</td>
<td><input type="submit" name="submit" id="donate" value="Donate" class="btn"></td>

</table>
		<br>
				<!-- <script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 

					

				</script> 
				
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> //--> 

</div>


</div>
</form>
</div>
</div>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script src="lib/jquery/jquery.min.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/theme.js" type="text/javascript"></script>
</div>
<?php include "footer.php";?>
</body>
</html>

	<script>
	
		function getEbayVal() {
		
			var userinput=document.getElementById('item').value;
			
			$.ajax({
					type: "GET",
					url:  "scripts/ebay_parser.php", 
					data: {search: userinput},
					dataType: "json",
					success: function(data) {
					
						//var emax=document.getElementById('emaxval').value;
						
					
						var ebase = data["ebase"];
						var emax = data["emax"];
						var emed = data["emed"];
						var emin = data["emin"];
							
				
						if(ebase == '' || emax == '' || emed == '' || emin == '') {
						
							document.getElementById('ebase').value='';	
							document.getElementById('great').value='';	
							document.getElementById('good').value='';	
							document.getElementById('ok').value='';	
							return;
						}
						document.getElementById('ebase').value=ebase;	
						document.getElementById('great').value=emax;	
						document.getElementById('good').value=emed;	
						document.getElementById('ok').value=emin;	
					}

				});
				
			
									
		}

			function isNumberKey(evt) {
			  
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;

			
			}
			
			function propointhandler() {
			
				var pts=document.getElementById('ebase').value;
				 
				var max = pts * 1.6;
				var med = pts * 1.4;
				var min = pts * 1.2;
				
				var emax = Math.ceil(max);
				var emed = Math.ceil(med);
				var emin = Math.ceil(min);
				
				document.getElementById('great').value = emax;
				document.getElementById('good').value = emed;
				document.getElementById('ok').value = emin;
				
			}
			
		</script>
	<script>
		
		function checkfilesize()
		{
			if (document.getElementById("file").files[0].size > 3145728)
			{
			alert("File is too big. 3 MB limit");
			return false;
			}
	
		return true;
		}
		

	</script>  
	
	
