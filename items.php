<?php

error_reporting(0);
session_start();

include "scripts/config.php"; 

$cat_id=mysql_real_escape_string($_GET['cat_id']);
$user_name=$_SESSION['username'];

?>

<html>
	<head>
		<!-- Page metadata -->
		<title>Procity - Rewarding Those Who Do-Good</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="lib/jquery/jquery.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>

		<!-- Styles -->
		<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="shortcut icon" href="img/icon.ico">
		<link rel="stylesheet" type="text/css" href="css/theme.css">
		<link rel="stylesheet" type="text/css" href="css/blog.css">
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<style>
			ul#grid {
			  list-style: none;
			  margin: 20px auto 0;
			  width: 1000px;
			  }

			#grid li {
			  float: left;
			  margin: 0 5px 10px 5px;
			  } 

			.portfolio {
			  padding: 20px;
			  margin-left: 200px; margin-right: auto;  margin-top:-60px;
			  /*background-color: #ffd7ce;*/
			  width:1000px; /*was 510*/
			  /*these two properties will be inherited by .portfolio h2 and .portfolio p */
			  text-align: center;
	
			  }
			  
			.portfolio h2 {
			  clear: both;
			  font-size: 35px;
			  font-weight: normal;
			  color: #58595b;
			  }
			  
			.portfolio p {
			  font-size: 15px;
			  color: #58595b;
			  /*text-shadow: 1px 1px 1px #aaa;
			  */
			  }
						
			#grid li a:hover img {
			  opacity:0.3;  filter:alpha(opacity=30);
			  }

			#grid li img {
			  background-color: white;
			  padding: 7px; margin: 0;
			  border: 1px dotted #58595b;
			  width: 100px;
			  height: 100px;
			  }
			  
			#grid li a {
			  display: block;
			  }
			  
		</style>
 
	</head>

	<body>

		<!-- begins navbar -->
		<?php include "topNav.php"; ?>
		<!-- ends navbar -->

		<div id="blog_wrapper">
			<div style="color:#F00;"><?php if($error1!='') {?> <script>alert("You can only claim 1 item at a time, please check your profile page");</script><?php } if($error2!='')  {?> <script>alert("You cant claim your own item!");</script><?php  } if($error3!=''){?><script>alert("Not enough ProPoints");</script><?php }
					   ?></div>
			<div class="container">
				
				<h1 class="header">The City</h1>
				<div class="row">

				
				<div class="span8">
				<td>
						<input class="input-large search-query" placeholder="Search" id="query" onkeydown="if (event.keyCode == 13) document.getElementById('searchbtn').click()" name="search" type="text" />
						<input type="submit" class="btn" name="searchbtn" id="searchbtn" value="Search" onclick="searchProcity()">
					</td>
				
				
				<hr />
				<?php
						
					
					$tableName="donate_item";		
						
					$limit = 12; 

					if(isset($_GET['cat_id']))
					{
						$catid=mysql_real_escape_string($_GET['cat_id']);
						$targetpage = "items.php?cat_id=$catid";
						if($catid=='') {
						
							$query = "SELECT COUNT(*) as num FROM $tableName where claim_status=0 AND show_status=1";	
							
						} else {
						
							$query = "SELECT COUNT(*) as num FROM $tableName WHERE category='$catid' AND claim_status=0 AND show_status=1";	
							
						}
					
					}else if(isset($_GET['search']))
					{
						$searchquery=mysql_real_escape_string($_GET['search']);
						$targetpage = "items.php?search=$searchquery";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1 
						AND ((`sub_category` LIKE '%$searchquery%') OR (`item_id` LIKE '%$searchquery%') OR (`item_description` LIKE '%$searchquery%'))";

									
					} else if(isset($_GET['highest'])) {
					
						$targetpage = "items.php?highest=set";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY ebase DESC";
						
					} else if(isset($_GET['lowest'])) {
					
						$targetpage = "items.php?lowest=set";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY ebase ASC";
						
					} else if(isset($_GET['oldest'])) {
					
						$targetpage = "items.php?oldest=set";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY donated_time ASC";
						
					} else if(isset($_GET['newest'])) {
					
						$targetpage = "items.php?newest=set";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY donated_time DESC";
						
					}
					else
					{
					
						$targetpage = "items.php?";
						$query = "SELECT COUNT(*) as num FROM $tableName WHERE claim_status=0 AND show_status=1";

					}
					
					$total_pages = mysql_fetch_array(mysql_query($query));
					$total_pages = $total_pages['num'];
					
					$stages = 3;
					$page = mysql_real_escape_string($_GET['page']);
					if($page) {
					
						$start = ($page - 1) * $limit; 
						
					} else {
					
						$start = 0;	
						
					}	
					
					// Get page data
					
					
					if(isset($_GET['cat_id'])) {
						
						$catid=mysql_real_escape_string($_GET['cat_id']);
						
						$query1 = "SELECT * FROM $tableName where category='$cat_id' and claim_status=0 AND show_status=1";
							
						
						
					} else if(isset($_GET['highest'])) {
					
						$query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY ebase DESC";
						
					} else if(isset($_GET['lowest'])) {
					
						$query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY ebase ASC";
						
					} else if(isset($_GET['oldest'])) {
					
						$query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY donated_time ASC";
						
					} else if(isset($_GET['newest'])) {
					
						$query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY donated_time DESC";
						
					} else if(isset($_GET['search'])) {
						
						 $searchquery = mysql_real_escape_string($_GET['search']);
						 $query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 
									AND ((`sub_category` LIKE '%$searchquery%') OR (`item_id` LIKE '%$searchquery%') OR (`item_description` LIKE '%$searchquery%'))";
						
					} else {
					
						$query1 = "SELECT * FROM $tableName WHERE claim_status=0 AND show_status=1 ORDER BY donated_time DESC";
					}
					
					$query1 = $query1. " LIMIT $start, $limit";
					$result = mysql_query($query1);			
					$rows=mysql_num_rows($result);
					
					// Initial page num setup
					if ($page == 0) {
						$page = 1;
					}
					
					$prev = $page - 1;	
					$next = $page + 1;							
					$lastpage = ceil($total_pages/$limit);		
					$LastPagem1 = $lastpage - 1;					
					
					$paginate = '';
					
					if($lastpage > 1) {	
					
						$paginate .= "<div class='pagination'> <ul>";
						
						// Previous
						if ($page > 1){
							$paginate.= "<li><a href='$targetpage&page=$prev'>previous</a></li>";
						}else{
							$paginate.= "<li><span class='disabled'>previous</span></li>";	
						}
							

						
						// Pages	
						if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
						{	
							for ($counter = 1; $counter <= $lastpage; $counter++)
							{
								if ($counter == $page){
									$paginate.= "<li><span class='current'>$counter</span></li>";
								}else{
									$paginate.= "<li><a href='$targetpage&page=$counter'>$counter</a></li>";}					
							}
						}
						elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
						{
							// Beginning only hide later pages
							if($page < 1 + ($stages * 2)) {
							
								for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
									if ($counter == $page){
										$paginate.= "<li><span class='current'>$counter</span></li>";
									}else{
										$paginate.= "<li><a href='$targetpage&page=$counter'>$counter</a></li>";
									}					
								}
								$paginate.= "<li>...</li>";
								$paginate.= "<li><a href='$targetpage&page=$LastPagem1'>$LastPagem1</a></li>";
								$paginate.= "<li><a href='$targetpage&page=$lastpage'>$lastpage</a></li>";		
							}
							// Middle hide some front and some back
							elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
							
								$paginate.= "<li><a href='$targetpage&page=1'>1</a></li>";
								$paginate.= "<li><a href='$targetpage&page=2'>2</a></li>";
								$paginate.= "<li>...</li>";
								for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
								{
									if ($counter == $page){
										$paginate.= "<li><span class='current'>$counter</span></li>";
									}else{
										$paginate.= "<li><a href='$targetpage&page=$counter'>$counter</a></li>";}					
								}
								$paginate.= "<li>...</li>";
								$paginate.= "<li><a href='$targetpage&page=$LastPagem1'>$LastPagem1</a></li>";
								$paginate.= "<li><a href='$targetpage&page=$lastpage'>$lastpage</a></li>";		
							}
							// End only hide early pages
							else {
								$paginate.= "<li><a href='$targetpage&page=1'>1</a></li>";
								$paginate.= "<li><a href='$targetpage&page=2'>2</a></li>";
								$paginate.= "<li>...</li>";
								for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
									if ($counter == $page){
										$paginate.= "<li><span class='current'>$counter</span></li>";
									}else{
										$paginate.= "<li><a href='$targetpage&page=$counter'>$counter</a></li>";
									}					
								}
							}
						}
									
								// Next
						if ($page < $counter - 1) { 
							$paginate.= "<li><a href='$targetpage&page=$next'>next</a></li>";
						}else {
							$paginate.= "<li><span class='disabled'>next</span></li>";
						}
							
						$paginate.= "</ul></div>";		
					
					
				}
					$total_pages.' Results';
				 // pagination

				?>
				<?php 
					if($rows>0)
					{
						
					while($fetch_donate=mysql_fetch_array($result))
					{
						
						$item_id=$fetch_donate['item_id'];
						$donated_by=$fetch_donate['donated_by'];
						$donate_type=$fetch_donate['donate_type'];
						
						
						$item_name=$fetch_donate['item_name'];
						
					
						$sql_user=mysql_query("select * from userinfo where Id='$donated_by'") or die(mysql_error());
						$fetch_user=mysql_fetch_array($sql_user);
						
						$ebase=$fetch_donate['ebase'];
						
						$condition=$fetch_donate['condition'];
						$sql_cond=mysql_query("select * from conditions where id ='$condition'");
						$fetch_cond=mysql_fetch_array($sql_cond);
						
						$condition_detail=$fetch_cond['condition'];
						
						$location=$fetch_user['Location'];
						$sql_loc = mysql_query("select * from location where id='$location'") or die(mysql_error());
						$fetch_location=mysql_fetch_array($sql_loc);
						
						
				?>
				<div class="portfolio">
				<ul id="grid">
				<li style="width:220px; ">
				<div class="post" style="height:280px;">
				<div class="row">
				<div class="span3"><a href="item.php?id=<?php echo $fetch_donate['id']; ?>" > <img class="main_pic" alt="main pic" src="drive/donations/<?php echo $fetch_donate['image'];?>" style="width:170px; height:170px; margin-left:-60px" /></a></div>
				<div class="span4 info">
				<div style="width:220px"><?php  echo $item_name;?>
				<p><?php echo $fetch_donate['ebase'];?> PP</p>
				<p><a href="item.php?id=<?php echo $fetch_donate['id']; ?>" > show </a></p></div>
				<!--<p><a href="scripts/claim.php?id=<?php echo $fetch_donate['id'];?>" class="btn">claim</a></p></div>-->
				<!--
				<p><?php echo $condition_detail;?></p>
				<p><?php echo $fetch_donate['item_description'];?></p>	
				<p class="author"><?php echo $fetch_user['Username'];?></p>
				<p class="date"><?php echo $fetch_donate['donated_date'];?></p>
				<p class="date"><?php echo $fetch_location['location'];?></p>
				-->
				</div>
				</div
				<p style="padding:30px; position:fixed">
				</p> </div></li></ul></div>

				<?php  }
					}
					else
					{	
					
					header("location:items.php");
						return;
					}?>
				<?php 
				 echo $paginate;
				?>

				<!--<script type="text/javascript"><!-- 

					google_ad_client = "ca-pub-5663904797620784"; 

					/* advert1 */ 
					google_ad_slot = "2888958757"; 
					google_ad_width = 728; 
					google_ad_height = 90; 
					
					
					</script> 
								
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">  </script> //--> 

				</div>

				

				<div class="span3 sidebar offset1">
					
					
					  <td><select onchange = "filter()" name="filter" id="filter" >
					  <option value="Sort" >Sort</option>
						<option value="Lowest price" >Lowest price </option>
                  		<option value="Highest price" >Highest price</option>
                  		<option value="Oldest" >Oldest</option>
						<option value="Newest" >Newest</option>
                </select></td>

					<h4 class="sidebar_header">Categories</h4>
					<ul class="sidebar_menu">
						<li> <a href="coupons.php">Coupons</a></li>
						<li> <a href="promocodes.php">Promo codes</a></li>
						<li> <a href="items.php">All</a></li>
							<?php $sql_cat=mysql_query("select * from category order by cat_name asc") or die(mysql_error());
									while($fetch_cat=mysql_fetch_array($sql_cat))
									{
							?>
						<li><a href="items.php?cat_id=<?php echo $fetch_cat['cat_id'];?>"><?php echo $fetch_cat['cat_name'];?></a></li>
						<?php } ?>
					</ul>
					
				</div>

				</div>
				
			</div>
			
		</div>
		
		<?php include "footer.php";?>
		
		<script>
		
		function filter() {
			
			var sel = document.getElementById('filter');
			if (sel.options[sel.selectedIndex].value == 'Sort') {
				
			} else if (sel.options[sel.selectedIndex].value == 'Lowest price') {
				window.location = "items.php?lowest=set";
			
			}else if (sel.options[sel.selectedIndex].value == 'Highest price') {
				window.location = "items.php?highest=set";
			
			}else if (sel.options[sel.selectedIndex].value == 'Oldest') {
				window.location = "items.php?oldest=set";
			
			}else if (sel.options[sel.selectedIndex].value == 'Newest') {
				window.location = "items.php?newest=set";
			
			}
		
		
		}
		
		function searchProcity() {
		
			var sel = document.getElementById('query').value;
			window.location = "items.php?search="+sel;
			
		}
		</script>

	</body>
	
</html>