<?php session_start();?>
<!DOCTYPE html>

<html>
<head>
    <title>Procity - Rewarding Those Who Do-Good</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="img/icon.ico" rel="shortcut icon">
    <link href="css/theme.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link href="css/external-pages.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic'
    rel='stylesheet' type='text/css'>
    <script language="javascript" type="text/javascript">
    
        function createlightbox() {
        
            document.getElementById('light').style.display='block';
            document.getElementById('fade').style.display='block'
            
        }
        
        function closelightbox() {
            
            document.getElementById('light').style.display='none';
            document.getElementById('fade').style.display='none'
            
        }
        
    </script>
</head>

<body>
    <!-- begins navbar -->
    <?php include"topNav.php"; ?>
	<!-- ends navbar -->

    <div id="hero">
        <div class="container">
            <!-- starts carousel -->

            <div class="row animated fadeInDown">
                <div class="span12">
                    <div class="carousel slide" id="myCarousel">
                        <!-- carousel items -->

                        <div class="carousel-inner">
                            <!-- slide -->

							<div class="item slide2">
                                <div class="row">
                                    <div class="span4 animated fadeInUpBig">
                                        <h1>Donate items for ProPoints!</h1>

                                        <p>Use those ProPoints to receive rewards!</p><a class=
                                        "btn btn-success btn-large" href=
                                        "items.php">Enter The City!</a>
                                    </div>

                                    <div class="span6 animated fadeInDownBig">
                                    <img alt="slide2" src=
                                    "img/image1.png"></div>
                                </div>
                            </div>
							
                            <div class="active item slide1">
                                <div class="row">
                                    <div class="span6"><img alt="slide1" src=
                                    "img/volunteer.jpg"></div>

                                    <div class="span4">
                                        <h1>Volunteer with organizations!</h1>

                                        <p>Earn ProPoints from doing service!</p><a class=
                                        "btn btn-success btn-large" href=
                                        "signup.php">Get started!</a>
                                    </div>
                                </div>
                            </div>
							<!-- slide -->

                            <div class="item slide2">
                                <div class="row">
                                    <div class="span4 animated fadeInUpBig">
                                        <h1>Businesses post rewards!</h1>

                                        <p>Redeem rewards from businesses!</p><a class=
                                        "btn btn-success btn-large" href=
                                        "items.php">Enter The City!</a>
                                    </div>

                                    <div class="span6 animated fadeInDownBig">
                                    <img alt="slide1" src=
                                    "img/businesses.jpg"></div>
                                </div>
                            </div>
                        </div><a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a> <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a></div>
                </div>
            </div>
        </div>
    </div>
	<!-- coming soon -->

	<center>
	<iframe width="560" height="315" src="//www.youtube.com/embed/rshVhUpv6nE" frameborder="0" allowfullscreen></iframe>
	</center>

	<center>
	<iframe width="560" height="315" src="//www.youtube.com/embed/CyTCv_cxPzc" frameborder="0" allowfullscreen></iframe>
	</center>

	<center>
	<iframe width="560" height="315" src="//www.youtube.com/embed/V5GGodn9oNw" frameborder="0" allowfullscreen></iframe>
	</center>

	
    <div >
        <div>
           
		   <div class="features_page" id="features">
                <div class="container">
                    <div class="features1">
                        <!-- header -->
                        <hr class="left visible-desktop">

                        <h2 class="section_header"><span>What is Procity?</span></h2>
                        <hr class="right visible-desktop">

                        <div class="row">
                            <div class="span12">
                                <h3 class="intro">Procity is a new University of Maryland service network! For the first time, there is great value given for doing good to society! Serve or donate to earn ProPoints. Redeem rewards and items using ProPoints! Join the network of Procitizens, all for free! </h3>
                            </div>
                        </div><!-- feature list -->

                        <div class="row">
                            <div class="span4 feature">
                                <a href="donate.php"><img alt="feature1" class="thumb" src="img/Donate.png"></a>

                                <h3>Service Organizations</h3>

                                
                                <p class="description">Our partnered organizations award their members ProPoints when they do service. If you do service and are not getting ProPoints, tell your organization to parter with us for free! Its an easy process and everyone benefits! </p>
								
                            </div>

                            <div class="span4 feature">
                                <img alt="feature2" class="thumb" src=
                                "img/VVA_logo.png">

                                <h3>Donations and the VVA</h3>

								<p class="description">You can still earn ProPoints by donating on our site! We have the Vietnam Veterans of America truck stopping by on campus every Friday to earn ProPoints! Also, if someone wants your item they claim it, you give it to them, and you earn ProPoints immediately!</p>
								
                            </div>

                            <div class="span4 feature">
                                <a href="items.php"><img alt="feature3" class="thumb" src=
                                "img/TheCity12.png"></a>

                                <h3>Business rewards and items</h3>

                                <p class="description">We work with businesses who post on our site discounts/coupons/specials that you can claim using your ProPoints! Otherwise feel free to spend your points on items others have donated!<p>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		   
		   
			<!-- features -->
			<div class="features_page" id="features">
                <div class="container">
                    <div class="features1">
                        <!-- header -->
                        <hr class="left visible-desktop">

                        <h2 class="section_header"><span>Procity News</span></h2>
                        <hr class="right visible-desktop">

                        <div class="row">
                            <div class="span12">
                                <h3 class="intro">Starting Spring '14 service organizations will have the power to award ProPoints for doing anything good for the community (volunteer service/fund-raisers/blood-drives etc). Procitizens will then have the option of claiming business rewards or donated items using their ProPoints for free! Saving you money, benefiting the community and helping businesses!
                       <br> <br> Look out for our donation competitions! We might throw one at your dorm soon!</h3>
                            </div>
                        </div><!-- feature list -->

                        <!-- Next Vietnam Veterans of America pick up: <b>October 25th, 2013</b> -->
                    </div>
                </div>
            </div>
			
 
			<div class="subscribe">
				<div class="row">
					<div class="span12">
							
						<!--<section id="" style=
						"margin-top:40px; margin-left:255px;">
							<script type="text/javascript">


							google_ad_client = "ca-pub-5663904797620784"; 

							/* advert1 */ 

							google_ad_slot = "2888958757"; 
							google_ad_width = 728; 
							google_ad_height = 90; 

							</script> <script src=
							"http://pagead2.googlesyndication.com/pagead/show_ads.js"
							type="text/javascript"></script>
						</section>-->
					</div>
				</div>
			</div>
							
        </div>
    </div>

    <div class="white_content" id="light">
        <a href="javascript:void(0)" onclick="closelightbox()" style=
        "position: absolute; z-index: 1001; margin-left: 308px; margin-top: 4px;">
        <img alt="" src=
        "http://2.bp.blogspot.com/-aXHXnOQPBJM/Tx_p1qqNI4I/AAAAAAAAAeU/bH0tlsikADQ/s1600/icon_cancel.gif"></a>
    </div><a href="javascript:void(0)" onclick="closelightbox()" style=
    "float:right">
    <div class="black_overlay" id="fade"></div></a> <script src=
    "js/jquery-1.9.1.js" type="text/javascript"></script> <script src="js/bootstrap.min.js" type="text/javascript"></script> <script src="js/theme.js" type="text/javascript"></script> 

	<?php include "footer.php";?>
	
</body>
</html>