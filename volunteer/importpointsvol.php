 <?php 
session_start();
ob_start();

date_default_timezone_set('America/New_York');

include "library/config.php";
include 'library/logincheck.php';

$getinfo=mysql_query("select * from orginfo where username='$username'") or die(mysql_error());
$info_arr=mysql_fetch_array($getinfo);
$userid=$info_arr['id'];
$orgname = $info_arr['name'];
$totalpoints = $info_arr['totalpointsawarded'];
$totalusers = $info_arr['totalusersawarded'];
$totaltimes = $info_arr['timesuploaded'];

$getinfo=mysql_query("select * from admin where username='procitystaff'") or die(mysql_error());
$fetchinfo=mysql_fetch_array($getinfo);
$orgproj = $fetchinfo["orgproject"];
$orghourly = $fetchinfo["orghourly"];


if(isset($_POST['add_prod'])) {

	$mimes = array('application/vnd.ms-excel','text/csv');
	if(!in_array($_FILES['csv']['type'],$mimes)){
	  
	  ?>
					<script> alert('Wrong format, must be csv file with first column emails, second number of ProPoints'); window.location="importpointsvol.php";</script>
					
					<?php
					
					return;
					
	} 	

	if ($_FILES['csv']['size'] > 0) {

		$bademails = array();
		$numerrors = 0;
		//get the csv file
		$file = $_FILES['csv']['tmp_name'];
		$tmpname = $_FILES['csv']['name'];
		$handle = fopen($file,"r");
		//loop through the csv file and insert into database
		$sql_item=mysql_query("select * from userinfo") or die(mysql_error());
		$getrow=mysql_num_rows($sql_item);
		if($getrow=='0') {
			
			?>
					<script> alert('Rows Empty'); window.location="importpointsvol.php";</script>
					
					<?php
					
					return;
			
		} else {
			
				
				while ($data = fgetcsv($handle,6000,",","'")) {
				
					$email = mysql_real_escape_string($data[0]);
					$givepoints = mysql_real_escape_string($data[1]);
					
					$givepoints = trim($givepoints);
					$email = trim($email);
					
					if ($email != null && $givepoints != null && is_numeric($givepoints)) {
					
						
						$sql=mysql_query("select * from userinfo where Email='$email'") or die(mysql_error());
						$rows=mysql_num_rows($sql);
						
						if($rows>0)
						{
							
							
							$userinfo = mysql_fetch_array($sql);
							$procitizen = $userinfo['Username'];
							$currpoints = $userinfo['ProPoints'];
							$currhash = $userinfo['hash'];
							$currstatus = $userinfo['status'];
							
							$email = $userinfo['Email'];
							
							$total = $currpoints + $givepoints;
							$sql2 = mysql_query("update userinfo set ProPoints='$total' where Email='$email'");
							
							$totalusers = $totalusers + 1;
							$totalpoints = $totalpoints + $givepoints;
							
							$msg= "Hi Procitizen $procitizen!\n\nWe've awarded you $givepoints ProPoints for recently doing service with $orgname! Be sure to check out The City page to redeem those points for awards! \n\nIf you're having trouble accessing your account click this link below \n\n http://www.myprocity.com/reset.php \n\n\nProcity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com"; 
							$subject = "You've earned ProPoints from service!";
							$headers = "From: procitystaff@gmail.com \r\n";
							$headers .= "Reply-To: procitystaff@gmail.com\r\n";
							$headers .= "Return-Path: procitystaff@gmail.com\r\n";

							mail($email,$subject,$msg,$headers);

							if($currstatus == "0") {
							
								// means user isn't confirmed and doesn't have a hash.
								// create a hash and update their password to match username
								// they havn't signed in yet
								if($currhash == '') {
								
									$encrypt = md5(md5(md5($procitizen)));
									$currhash = md5(md5(md5(rand(0,1000))));
									$updatehash = mysql_query("update userinfo set hash='$currhash',password='$encrypt' where Email='$email'");
								}
								
								$msg = "Hi there!\n\nYou've got ProPoints! We've already made you a Procity account from doing service with our partnered organization: $orgname. You havn't confirmed your account yet, thats why you can't log in. Click this link to activate your account. \n\n http://www.myprocity.com/scripts/confirm_email.php?username=".$procitizen."&hash=".$currhash." \n\nAccount details --\nEmail: $email\nUsername: $procitizen\nTemp Password: $procitizen\n\n Please change your temporary password (your username) immediately and enjoy using Procity! \n Thanks! \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com"; 
								$subject = "Confirm your email - Procity";
								$headers = "From: procitystaff@gmail.com \r\n";
								$headers .= "Reply-To: procitystaff@gmail.com\r\n";
								$headers .= "Return-Path: procitystaff@gmail.com\r\n";

								mail($email,$subject,$msg,$headers);	
											
								
							}
							
						} else {
						
							$at = strpos($email,"@");

							if($at <= 0) {

								$numerrors = $numerrors + 1;
								$bademails[$email] = $givepoints;
								continue;
								
							}
								
							$lastid = mysql_insert_id();
							$user = substr($email, 0, $at);
							$user = $user.$lastid;
							$total = $givepoints + 25;
								
							
							$hash = md5(md5(md5(rand(0,1000))));
							$qry = "INSERT INTO userinfo (FirstName, LastName, Username, Password, Email,ProPoints,hash,loginid,status,Location,num_donated,num_claimed,num_rewards,propicpath) VALUES 
							('','','', '', '$email','$total','$hash','','0','','0','0','0','')";
							
							mysql_query($qry);

							$lastid = mysql_insert_id();
							$user = substr($email, 0, $at);
							$user = $user.$lastid;
							$encrypt = md5(md5(md5($user)));
							mysql_query("update userinfo set username='$user',password='$encrypt' where id='$lastid'");
							
							$totalusers = $totalusers + 1;
							$totalpoints = $totalpoints + $total;
							
							$msg = "Hi there!\n\nWe've made you a Procity account from doing service with our partnered organization: $orgname. You will now earn ProPoints (we've already given you 25 PP to start!) for doing service with $orgname! Be sure to check out the City page to redeem those points for rewards, coupons, items and more!\n\nAccount details --\nEmail: $email\nUsername: $user\nTemp Password: $user\n\n First click this link to confirm your email/account so you can log in as a Procitizen. \n\n http://www.myprocity.com/scripts/confirm_email.php?username=".$user."&hash=".$hash." \n\n Please change your temporary password (your username) immediately and enjoy using Procity! \n Thanks! \n\n Procity - Rewarding Those Who Do-Good \n www.myprocity.com \n procitystaff@gmail.com"; 
							$subject = "Welcome to Procity!";
							$headers = "From: procitystaff@gmail.com \r\n";
							$headers .= "Reply-To: procitystaff@gmail.com\r\n";
							$headers .= "Return-Path: procitystaff@gmail.com\r\n";

							mail($email,$subject,$msg,$headers);	
											
					
						}
					
					} else {
					
						$bademails[$email] = $givepoints;
						//array_push($bademails, $email);
						$numerrors = $numerrors + 1;
					}
			
				}
				
				$totaltimes = $totaltimes + 1;
				mysql_query("update orginfo set totalpointsawarded='$totalpoints',totalusersawarded='$totalusers',timesuploaded='$totaltimes' where username='$username'");
		
				$currdate = date("Y-m-dH-i-s");
				$rename = $currdate.$tmpname;
				$uploadDir = 'drive/orgcsvs/'; 
				
				$uploadfile = $uploadDir.$rename;             
				move_uploaded_file($_FILES['csv']['tmp_name'], $uploadfile);

				$eventname=mysql_real_escape_string($_POST['name']);
				$desp=mysql_real_escape_string($_POST['description']);
				
				$qry = "INSERT INTO orgupload (orgid, eventname, description,time, csvpath) VALUES 
								('$userid','$eventname','$desp',NOW(), '$rename')";
								
				mysql_query($qry);
				
				if($numerrors > 0) {
							
						?>
							
								<script> alert('Some errors may have occured (rows skipped), recheck your file, read the PDF or contact procitystaff@gmail.com');</script>
								
							<?php
							echo "Number of errors: $numerrors"; echo '<br>';
							echo "Values:"; echo '<pre>'; print_r($bademails); echo  '</pre>'; echo '<br>';
							echo "Rest successful.";
		
							exit(1);
							
				} else {

							?>
							
								<script> alert('Success - no errors'); window.location="page.php";</script>
							
							<?php
							
							
				
				}
		}				
	
    }
	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/template.css" type="text/css"/>

<script src="js/jquery-1.8.2.min.js" type="text/javascript">
	</script>
	<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>
	<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#product").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>
     <script src="ji/jquery-1.7.2.js"></script> 

  <script src="ji/jquery.ui.core.js"></script> 

  <script src="ji/jquery.ui.widget.js"></script> 

     <script type="text/javascript" src="ajax1.js"></script> 

  <script type="text/javascript" src="drop_function_3.js">



</script> 


</head>

<body>

<?php include('left-nav.php');?> <!--left-menu -->

<div class="page">
    
    <div class="page-nav">
    	
        <div class="clear"></div> <!--clear div -->

    </div> <!--page nav -->
    
  <div class="box1">
    	<h1> Give volunteers points via CSV upload</h1>
        
         <tr>
	<a href="http://www.extendoffice.com/documents/excel/613-excel-export-to-csv-file.html" target="_blank"> Click here to learn how to convert xls to csv (tab delimited). Import must be in CSV only! Thank You! :)</a>
  </tr>
  
		<form action="" id="product" name="product" method="post" enctype="multipart/form-data" >
 
 <table  width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 3px #CCCCCC;" align="center">
  
   <tr>
    <td width="15%" valign="top"><strong>Instructions: </strong></td>
    <td width="85%" valign="top"><b> Column 1 should have email (any), Column 2 should have number of ProPoints earned. If you're doing by hours give <?php echo $orghourly; ?> ProPoints per hour. If you're doing by project give <?php echo $orgproj; ?> ProPoints per project </b></td>
  </tr>
  
   <tr>
    <td width="15%" valign="top"><strong>Event name</strong></td>
    <td width="85%" valign="top"><input type="text" class="validate[required] txt-feild-small" name="name" id="name" /></td>
  </tr>
  <tr>
    <td valign="top"><strong>Description</strong></td>
    <td valign="top"><input type="text" class="validate[required] txt-feild-small" name="description" id="description" /></td>
  </tr>

  <tr>
    <td valign="top"><strong>Import CSV file</strong></td>
    <td><input type="file" name="csv" id="csv" class="validate[required] txt-feild-small"  /></td>
  </tr>
  
  <tr>
    <td valign="top"><strong>Download instructions</strong></td>
    <td><a href="drive/info/ProcityServiceOrganizationUploadInstructions.pdf" download="ProcityServiceOrganizationUploadInstructions.pdf"> Click to download upload instruction pdf</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="add_prod" class="btn1" value="Add" /></td>
  </tr>
  

</table>
       




</form>

  </div><!--box1 -->
    <!--box 2 -->
<p>
        
   
    <br />
    <br />
    <br />
    <!-- sucess -->
    <!-- error -->
    <!-- warning -->
    <!-- information -->
<?php include "footer.php";?>
<!--footer -->
</div><!--page -->

<div class="clear"></div> <!--clear div -->

</body>
</html>
