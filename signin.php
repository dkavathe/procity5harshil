<?php 

$error = '';
if(isset($_GET['msg'])) {

	if($_GET['msg']=='error')
	{
		$error="Please type in valid Procity credentials";
		
	}else if($_GET['msg']=='error2')
	{
		$error="Invalid input";
		
	} else if($_GET['msg']=='reset')
	{
		$error="Successfully changed password";
		
	} else if($_GET['msg']=='confirm')
	{
		$error="Your credentials match, but you've not confirmed your account yet. Click Need help? to fix this issue";
		
	} else if($_GET['msg']=='registered')
	{
		?>
			<script> alert("Cool, you can enter The City now. Use your temporary username/email and password provided in the confirmation email."); </script>
					
		<?php
		
		$error="You're officially a Procitizen! Welcome and get ready to be rewarded for doing good!";
		
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Procity - Rewarding Those Who Do-Good</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/signin.css">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
<?php include"topNav.php"; ?>
<div id="box_login">
<div class="container">
<div class="span12 box_wrapper">
<div class="span12 box">
<div>
<div class="head">
<h4>Log in to your Procity account</h4>
</div>
<div class="form">
<div style="color:#F00;">
<?php if($error != '') {echo $error;}?></div>
<form method="post" action="scripts/signin.php"><input id="username" type="text" placeholder="Email or Username" required="" name="Username" type="text" /> 
<input id="password" placeholder="Password" required="" type="password" name="Password" type="password" />
<div class="registrationFormAlert" id="incorrectEntry"></div>
<div class="remember">
<div class="left"><input id="remember_me" type="checkbox" /> <label for="remember_me">Remember me</label></div>
<div class="right"><a href="reset.php">Need help?</a></div>
</div>
<p>&nbsp;</p>
<input class="btn" id="signinbut" onclick="" value="Sign in"  type="submit" style="width:28%;" /></form></div>
</div>
</div>
<p class="already">Don't have an account? <a href="signup.php"> Sign up</a></p> <br>
</div>
</div>
</div>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/theme.js" type="text/javascript"></script>
<?php include "footer.php";?>
</body>
</html>