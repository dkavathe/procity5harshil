<?php
$error1='';
$error2='';
$error3='';
$error4='';
$error5 = '';
$success ='';
$limit ='';

if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error1')) {
	$error1='Username taken!';
} else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error2')) {
	$error2='Email address taken!';
} else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error3')) {
	$error3='UMD email address only';
} else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error4')) {
	$error4='Entries empty';
} else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='error5')) {
	$error5='Passwords dont match';
}  else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='success')) {
	$success='Success';
}   else if(isset($_REQUEST['msg']) && ($_REQUEST['msg']=='limit')) {
	$limit='limit';
}

?>
<!DOCTYPE html>
<html>

	<head>
	
		<title>Procity - Rewarding Those Who Do-Good</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link rel="shortcut icon" href="img/icon.ico">
		<link rel="stylesheet" type="text/css" href="css/signup.css">
		<link rel="stylesheet" type="text/css" href="css/theme.css">
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<script type="text/javascript">
			var RecaptchaOptions = {
			theme : 'blackglass'};
 </script>
	</head>
	
	<body>
		
		<?php include"topNav.php"; ?>
		<div id="box_sign">
		
		  <div class="container">
		  
			<div class="span12 box_wrapper">
			
			  <div class="span12 box">
			  
				<div>
				  <div class="head">
					<h4>Create your Procity account</h4>
				  </div>
				  <div class="form">
				  <?php if($error1!='') {?> <script>alert("Username taken");</script><?php } else if($limit != '') { ?> <script> alert("Whoops, Procity Beta is currently full. Please wait until our launch date (Sep 17th) to join our network!") </script> <?php } else if($error2!='')  {?> <script>alert("Email taken");</script><?php  }else if($error5!='') { ?> <script> alert("Passwords don't match"); </script> <?php } else if($error3!='') {?> <script>alert("UMD email only");</script> <?php  } else if($error4!='') {?> <script>alert("Entries empty");</script><?php } else if($success!=''){?><script>alert("Successfully registered! We've sent you a confirmation link. Please confirm your email before joining us.");</script><?php }
				   ?>
					<form method="post" action="scripts/signup.php">
					  <input required="" id="emailadd" placeholder="Email"name="Email" type="text" />
					  <input required="" placeholder="Password"  name="Password" id="newpass" AUTOCOMPLETE="off" type="password" />
					  <input  required="" placeholder="Confirm password" name="Confirm" id="confpass" AUTOCOMPLETE="off" type="password" />
					  <!--<input  placeholder="Referred by email (they get 15 PP!)" name="Referral" id="referral" type="text" />-->
					  <div required="" class="registrationFormAlert" id="divCheckPasswordMatch"></div>
					  <div required="" class="registrationFormAlert" id="terpmaillabel">
					  
						<?php $msg; ?>
					  </div>
					  <br />
					  <input value="Get Started!" id="submitbut"type="submit"  class="btn"  style="width:35%;" />
					</form>
				  </div>
				</div>
			  </div>
			  <p class="already">Already have an account?  <a href="signin.php"> Log in</a></p>
			</div>
		  </div>
		  
		</div>
		<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script> 
		<script src="js/bootstrap.min.js" type="text/javascript"></script> 
		<script src="js/theme.js" type="text/javascript"></script> 
		
		<?php include "footer.php";?>
		
	</body>
	
</html>