<?php
session_start();

require_once 'library/functions.php';
require_once 'library/config.php';
include 'library/logincheck.php';


	if(isset($_POST['edit_reward']))
	{
		$id=mysql_real_escape_string($_POST['id']);
		$name=mysql_real_escape_string($_POST['name']);
		$desp=mysql_real_escape_string($_POST['description']);
		$propointworth=mysql_real_escape_string($_POST['propoints']);
		$salevalue=mysql_real_escape_string($_POST['sale']);
		
		$sql=mysql_query("select * from business_item where id='$id'") or die(mysql_error());
		$rows=mysql_num_rows($sql);
		if($rows<= 0)
		{
			?>
				<script> alert('Error'); window.location="page.php";</script>
				
				<?php
				
				return;
		}
		else
		{
		
			$getinfo=mysql_query("select * from admin where username='procitystaff'") or die(mysql_error());
					$info_arr=mysql_fetch_array($getinfo);
					$busmin=$info_arr['businessmin'];
					$busmax=$info_arr['businessmax'];
		
					if( $propointworth < $busmin || $propointworth > $busmax) {
					
					?>
					<script> alert('ProPoints not in range please look at stats page'); window.location="page.php";</script>
					
					<?php
						return;
					}
					
			$update="update business_item set name='$name',
					description='$desp', 
					propointworth='$propointworth',
					saleworth='$salevalue'
					 where id='$id'";
			mysql_query($update) or die(mysql_error());
			
			header("location:page.php");
			return;
		}
	} else {
	
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id=mysql_real_escape_string($_POST['id']);
		} else {
			$id=mysql_real_escape_string($_GET['id']);
		}
		
		$sql_user=mysql_query("select * from business_item where id='$id'") or die(mysql_error());
		$s_no=0;
		$fetch_user=mysql_fetch_array($sql_user);
	
	}
  
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/template.css" type="text/css"/>

<script src="js/jquery-1.8.2.min.js" type="text/javascript">
	</script>
	<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>
	<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#user_info").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
	</script>

</head>

<body>

<?php include('left-nav.php');?> <!--left-menu -->

<div class="page">
    
    <div class="page-nav">
    	 <div class="clear"></div> <!--clear div -->

    </div> <!--page nav -->
    
  <div class="box1" style="">
    	<h1> Personalized Information</h1>
     <p style="color:#F00;">   <?php if(isset($_GET['msg'])=='error') { echo 'User Name or Email Already Exists';}?></p>
        <div align="center">
		<form method="post" name="user_info" id="user_info">
		
<table width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 3px #CCCCCC;">
 
 <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
  <tr>
    <td width="15%" valign="top"><strong>Reward name</strong></td>
    <td width="85%" valign="top"><input type="text" class="validate[required] txt-feild-small" name="name" id="name" value="<?php echo $fetch_user['name'];?>" /></td>
  </tr>
  <tr>
    <td valign="top"><strong>Description</strong></td>
    <td valign="top"><input type="text" class="validate[required] txt-feild-small" name="description" id="description" value="<?php echo $fetch_user['description'];?>" /></td>
  </tr>
  <tr>
    <td valign="top"><strong>Pro Points value</strong></td>
    <td valign="top"><input type="text" class="validate[required] txt-feild-small" name="propoints" id="propoints" value="<?php echo $fetch_user['propointworth'];?>" /></td>
  </tr>
  <tr>
    <td valign="top"><strong>Sale value</strong></td>
    <td valign="top"><input type="text" class="validate[required] txt-feild-small" name="sale" id="sale" value="<?php echo $fetch_user['saleworth'];?>" /></td>
  </tr>
  
 <tr>
 <td>&nbsp;</td>
 <td>&nbsp;</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="edit_reward" class="btn1" value="Save" /></td>
  </tr>
</table>

  
  </table>
        </form>
</div>
  </div><!--box1 -->
    <!--box 2 -->
<p>
        
   
    <br />
    <br />
    <br />
    <!-- sucess -->
    <!-- error -->
    <!-- warning -->
    <!-- information -->
<?php include "footer.php";?>
<!--footer -->
</div><!--page -->

<div class="clear"></div> <!--clear div -->

</body>
</html>
